var webdriverio = require('webdriverio');
var assert = require('assert');
var save_screen_shot_path = "./tests/screen_shot/";
var mongoose = require('mongoose');
try{
    mongoose.connect('mongodb://localhost:27017/seproject');
}catch(err){
    console.log(err);
}
models = require('../app/models');

describe('User login, create project, mark completed, and logout.', function(){
    var _this = this;
    _this.timeout(10000);

    before(function(done) {
        _this.client = webdriverio
            .remote({
                desiredCapabilities: {
                    browserName: 'chrome'
                }
            })
            .init()
            .setViewportSize({
                width: 1366,
                height: 768
            }).call(done);
    });

    after(function(done) {
        _this.client.end().call(function(){
            models.member.where({username: 'test@test.com'}).findOneAndRemove(function(){
                done();
            });
        })
    });

    it('Should return correct title', function (done) { // <- 1
        _this.client.url('http://www.cozpace.com')
            .getTitle(function(err, title) {
                assert.equal(title, "CoZpace Collaborative Search Platform");
            })
            .call(done);
    });

    it('Can register', function (done) { // <- 1
        _this.client
            .click('a[href="/register"]')
            .clearElement('input')
            .addValue('input[name="username"]', "test@test.com")
            .addValue('input[name="password"]', "testtest")
            .addValue('input[name="firstname"]', "Oliver")
            .addValue('input[name="lastname"]', "Queen")
            .saveScreenshot(save_screen_shot_path + "1_register.jpg")
            .click('#regis_form button[type="submit"]')
            .pause(2000)
            .getText('#logo', function(err, text){
                assert.equal(text, 'Complete Register');
            })
            .saveScreenshot(save_screen_shot_path + "2_register_completed.jpg")
            .click('a[href="/login"]')
            .call(done);
    });

    it('Can login', function (done) { // <- 1
        _this.client
            .clearElement('input')
            .addValue('input[name="username"]', "test@test.com")
            .addValue('input[name="password"]', "testtest")
            .saveScreenshot(save_screen_shot_path + "1_login.jpg")
            .click('#site-login-btn')
            .pause(2000)
            .saveScreenshot(save_screen_shot_path + "3_dashboard.jpg")
            .call(done);
    });
    it('Can create new project', function (done) {
        _this.client
            .click('#create-newproject-btn')
            .waitForVisible('#project-name')
            .addValue('#project-name', "DotA2")
            .pause(1000)
            .saveScreenshot(save_screen_shot_path + '4_create_project_form.jpg')
            .click('#create-newproject-submit-btn')
            .pause(1000) // Wait effect
            .getText('#create-newproject-status-text', function(err, text){
                assert.equal(text, 'Create success.');
            })
            .saveScreenshot(save_screen_shot_path + '5_create_project_created.jpg')
            .click('#create-newproject-done-btn')
            .call(done);
    });

    it('Can select project', function (done) {
        _this.client
            .pause(1000)//หยุดรอให้เอฟเฟคหาย
            .click('.project-list-item:nth-of-type(2)')
            .waitForVisible('#project-review-title')
            .getText('#project-review-title', function(err, text){
                assert.equal(text, 'DotA2');
            })
            .saveScreenshot(save_screen_shot_path + '6_select_project.jpg')
            .call(done);
    });

    it('Can open project', function (done) {
        _this.client
            .click('#project-review-work-on')
            .pause(2000)
            .saveScreenshot(save_screen_shot_path + '7_project_dashboard.jpg')
            .call(done);
    });

    it('Can mark as completed project', function (done) {
        _this.client
            .click('#project-btn')
            .click('#completed-project-btn')
            .saveScreenshot(save_screen_shot_path + '8_mark_as_completed_project.jpg')
            .pause(2000) //Wait Effect
            .click('#completed-project-yes-btn')
            .pause(2000) //Wait Effect
            .call(done);
    });

    it('User can logout', function (done) {
        _this.client
            .click('#user-btn')
            .saveScreenshot(save_screen_shot_path + '9_user_logout.jpg')
            .click('a[href="/logout"]')
            .call(done);
    });

});