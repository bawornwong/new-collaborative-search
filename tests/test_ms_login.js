var webdriverio = require('webdriverio');
var assert = require('assert');
var save_screen_shot_path = "./tests/screen_shot/";
var mongoose = require('mongoose');
try{
    mongoose.connect('mongodb://localhost:27017/seproject');
}catch(err){
    console.log(err);
}
models = require('../app/models');

describe('User can login with microsoft account.', function(){
    var _this = this;
    _this.timeout(30000);

    before(function(done) {
        _this.client = webdriverio
            .remote({
                desiredCapabilities: {
                    browserName: 'chrome'
                }
            })
            .init()
            .setViewportSize({
                width: 1366,
                height: 768
            }).call(done);
    });

    after(function(done) {
        _this.client.end().call(function(){
            models.member.where({username: 'saranonuan@hotmail.com'}).findOneAndRemove(function(){
                done();
            });
        })
    });

    it('Should return correct title.', function (done) { // <- 1
        _this.client.url('http://www.cozpace.com')
            .getTitle(function(err, title) {
                assert.equal(title, "CoZpace Collaborative Search Platform");
            })
            .call(done);
    });

    it('Can login with ms account', function (done) { // <- 1
        _this.client
            .click('#ms-login-btn')
            .pause(2000)
            .addValue('input[name="login"]', 'saranonuan@hotmail.com')
            .addValue('input[name="passwd"]', 'Saran1@#$')
            .saveScreenshot(save_screen_shot_path + "10_ms_login_field.jpg")
            .click('#idSIButton9')
            .call(done);
    });


    it('Must redirect to dashboard', function (done) { // <- 1
        _this.client
            .waitForVisible('#user-btn', 5000)
            .pause(1000)
            .call(done);
    });

    it('User can logout', function (done) {
        _this.client
            .click('#user-btn')
            .click('a[href="/logout"]')
            .call(done);
    });

});