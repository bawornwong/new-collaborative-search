/*!
 * Collaborative Search App
 */
process.env.NODE_ENV = 'development'; // production , development

var port = process.env.PORT || 4000; 		// set our port

// EXPRESSJS IMPORT
// =============================================================================
var express = require('express');


// DEFINE APP
// =============================================================================
var app = module.exports = express();
var server = require('http').Server(app);


// MAIN LIB
// =============================================================================
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
var compressor = require('node-minify');
var methodOverride = require('method-override');
var cookieParser = require('cookie-parser');
var expressSession = require('express-session');
var flash = require('express-flash');
var mongoose = require('mongoose');
var morgan = require('morgan');

// GLOBAL VARIABLE
// =============================================================================
GLOBAL.settings = {
    mode: 'development',
    BING_ACCT_ID: '39Jm6aVonON9qdcnl2jzxABUU0MWLzVnewdKRIB5wH4='
};
GLOBAL.__static = __dirname + '/static';
//GLOBAL.mongodb_url = 'mongodb://'+settings.mongo.username+':'+settings.mongo.password+'@'+settings.mongo.host+':'+settings.mongo.port+'/'+settings.mongo.db;
GLOBAL.mongodb_url = 'mongodb://localhost:27017/seproject';
GLOBAL.controllers = require('./app/controllers');
GLOBAL.helpers = require('./app/helpers');
GLOBAL.models = require('./app/models');
GLOBAL.session_secret = 'OH8sa65d8a6s7tdg&^ASRD*&^ASdgasdhjabsdj';
GLOBAL.sendlog = function (text, showon) {
    if (!showon || (showon && app.settings.env == showon)) {
        console.log('+-----+');
        console.log(text);
        console.log('+-----+');
    }
};


// MONGOOSE
// =============================================================================
// =============================================================================
mongoose.connect(mongodb_url, function(){
    console.log("Mongoose connected", mongodb_url);
    GLOBAL.session_store = require('mongoose-session')(mongoose);

    // APP SETTING
    // =============================================================================
    console.log("Initial App setting");
    //app.engine('jade', require('jade').__express);
    app.set('views', __dirname + '/app/views');
    app.set('view engine', 'jade');
    app.set('view options', {
        layout: false
    });

    // APP USE
    // =============================================================================
    console.log("Initial App use");
    app.use('/static', express.static(__static));
    app.use(morgan('dev'));
    app.use(bodyParser());
    app.use(expressValidator());
    app.use(methodOverride());
    app.use(cookieParser());
    app.use(expressSession({
        key: 'express.sid',
        secret: session_secret,
        store: session_store
    }));
    app.use(controllers.member.passport.initialize());
    app.use(controllers.member.passport.session());
    app.use(flash());

    // SOCKET IO
    // =============================================================================
    console.log("Initial SocketIO");
    require('./app/socket.js')(app, server);

    // ROUTES
    // =============================================================================
    console.log("Initial Routers");
    require('./app/routes.js')(app, express, settings);

    // START THE SERVER
    // =============================================================================
    //app.listen(port);
    server.listen(port);
    console.log('Start Server on port ' + port);
});
GLOBAL.mongoose = mongoose;



// COMPRESS STATIC FILE
// =============================================================================
// Compressor Core-Javascript
var js_files = ['./static/js-core/jquery.1.9.1.js'
    , './static/js-core/jquery.migrate.1.0.0.js'
    , './static/js-core/jquery.mousewheel.js'
    , './static/js-core/jquery.jscrollpane.js'
    , './static/js-core/jquery.scroll-startstop.events.js'
    , './static/js-core/jquery.hotkey.js'
    , './static/js-core/jquery-ui.1.9.2.js'
    , './static/js-core/jquery-ui.datetimepicker.js'
    , './static/js-core/jquery.bootstrap.js'
    , './static/js-core/jquery.address.js'
    , './static/js-core/jquery.noty.js'
    , './static/js-core/underscore.js'
    , './static/js-core/moment.min.js'
    , './static/js-core/jquery.custom.js'
    , './static/js-core/jquery.Jcrop.js'
    , './static/js-core/blocksit.min.js'
    , './static/js-core/highslide-with-gallery.js'
    , './static/js-core/jquery.masonry.min.js'
    , './static/js-core/jquery.swipebox.min.js'
    , './static/js-core/jquery.pinto.js'
    , './static/js-core/clipboard.js'
    , './static/js-core/lightbox.min.js'];

new compressor.minify({
    type: 'no-compress',
    fileIn: js_files,
    fileOut: './static/js/core.min.js',
    tempPath: './temp/',
    callback: function (err) {
        if (err) console.log(err);
    }
});

// Compressor Core-Style
var css_files = ['./static/css-core/bootstrap.css'
    , './static/css-core/bootstrap.responsive.css'
    , './static/css-core/bootstrap.custom.css'
    , './static/css-core/font-face.css'
    , './static/css-core/jquery.ui.css'
    , './static/css-core/jquery.jscrollpane.css'
    , './static/css-core/font-awesome.css'
    , './static/css-core/font-awesome-ie7.min.css'
    , './static/css-core/jquery.Jcrop.css'
    , './static/css-core/swipebox.min.css'
    , './static/css-core/css_frame.css'
    , './static/css-core/lightbox.css'
    , './static/css-core/custom.css'];
new compressor.minify({
    type: 'no-compress',
    fileIn: css_files,
    fileOut: './static/css/core.min.css',
    tempPath: './temp/',
    callback: function (err) {
        if (err) console.log(err);
    }
});



