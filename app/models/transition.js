/*!
* Model: activity
*/

var mongoose = require('mongoose')
    ,crypto = require('crypto')
    ,Schema = mongoose.Schema
    ,object_id = Schema.ObjectId
    ,moment = require('moment');

var transition_schema = new Schema({
    id : object_id,
    member: {type: Schema.Types.ObjectId, ref: 'members' },
    project: {type: Schema.Types.ObjectId, ref: 'projects'},
    from : { type: String },
    to : { type: String },
    type : { type: String, default: 'SYNC'},
    created : { type: Date, default: new Date()}
}/*, { capped: { size: 1024, max: 1000000, autoIndexId: true }}*/);

module.exports = mongoose.model('transitions', transition_schema);