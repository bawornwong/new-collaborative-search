/*!
* Model: project invite
*/

var mongoose = require('mongoose')
    ,crypto = require('crypto')
    ,Schema = mongoose.Schema
    ,object_id = Schema.ObjectId
    ,moment = require('moment');

var project_invite_schema = new Schema({
    id : object_id,
    project : { type: Schema.Types.ObjectId, ref: 'projects' },
    invite_by : { type: Schema.Types.ObjectId, ref: 'members' },
    invite_to : { type: Schema.Types.ObjectId, ref: 'members' },
    created : { type: Date, default: new Date() }
});

module.exports = mongoose.model('project_invites', project_invite_schema);