/*!
* Model: activity
*/

var mongoose = require('mongoose')
    ,crypto = require('crypto')
    ,Schema = mongoose.Schema
    ,object_id = Schema.ObjectId
    ,moment = require('moment');

var project_transition_count = new Schema({
    id : object_id,
    project: {type: Schema.Types.ObjectId, ref: 'projects'},
    from : { type: String },
    to : { type: String },
    count : { type: Number, default: 0 },
    type : { type: String, default: 'SYNC'},
    created : { type: Date, default: new Date()}
}/*, { capped: { size: 1024, max: 1000000, autoIndexId: true }}*/);

module.exports = mongoose.model('project_transition_counts', project_transition_count);