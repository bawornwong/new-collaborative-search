/*!
* Model: activity
*/

var mongoose = require('mongoose')
    ,crypto = require('crypto')
    ,Schema = mongoose.Schema
    ,object_id = Schema.ObjectId
    ,moment = require('moment');

var activity_schema = new Schema({
    id : object_id,
    member: {type: Schema.Types.ObjectId, ref: 'members' },
    project: {type: Schema.Types.ObjectId, ref: 'projects'},
    message: { type: String },
    module : { type: String },
    created : { type: Date, default: new Date()}
});
    //, { capped: { size: 1000000, max: 10000000, autoIndexId: true }});

module.exports = mongoose.model('activities', activity_schema);