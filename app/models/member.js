/*!
* Model: member
*/

var mongoose = require('mongoose')
    ,crypto = require('crypto')
    ,Schema = mongoose.Schema
    ,object_id = Schema.ObjectId
    ,moment = require('moment');

var member_schema = new Schema({
    id : object_id,
    username : { type: String, required: true, unique: true},
    password : { type: String, required: true},
    firstname : { type: String, required: true},
    lastname : { type: String, required: true},
    time_zone : {
        type: {type:String},
        hours: {type:Number},
        minnutes: {type:Number}
    },
    last_login : { type: Date, default: new Date()},
    signup_date : { type: Date, default: new Date()}
});

member_schema.methods.validPassword = function (password, cb) {
    var shasum = crypto.createHash('sha1');
    shasum.update(password);
    var encrypt_password = shasum.digest('hex');
    if( this.password == encrypt_password ){
        cb(true);
    }else{
        cb(false);
    }
};

module.exports = mongoose.model('members', member_schema);