/*!
* Model: search result
*/

var mongoose = require('mongoose')
    ,crypto = require('crypto')
    ,Schema = mongoose.Schema
    ,object_id = Schema.ObjectId
    ,moment = require('moment');

var search_result_schema = new Schema({
    id : object_id,
    result_id : { type: String, required: true },
    description : { type: String },
    display_url : { type: String },
    title : { type: String },
    url : { type: String },
    project : { type: Schema.Types.ObjectId, ref: 'projects', required: true },
    
    relevant : [{
            member: {type: Schema.Types.ObjectId, ref: 'members' },
            state: { type: String, default: 'Not relevant' },
            created : { type: Date, default: new Date()},
            updated : { type: Date, default: new Date()}
        }],
    seen : [{
            member: {type: Schema.Types.ObjectId, ref: 'members' },
            timeseen: [{
                    time : { type: Date, default: new moment.utc() }
                }],
            created : { type: Date, default: new Date()},
            updated : { type: Date, default: new Date()}
        }],
    comment : [{
            member: {type: Schema.Types.ObjectId, ref: 'members' },
            message: { type: String, required: true },
            created : { type: Date, default: new Date()},
            updated : { type: Date, default: new Date() }
        }],
    snapshots : [{
            snapshot: { type: Schema.Types.ObjectId, ref: 'snapshots' }
        }],
    created : { type: Date, default: new Date()},
    updated : { type: Date, default: new Date()}
});

module.exports = mongoose.model('search_results', search_result_schema);