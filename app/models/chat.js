/*!
* Model: member
*/

var mongoose = require('mongoose')
    ,crypto = require('crypto')
    ,Schema = mongoose.Schema
    ,object_id = Schema.ObjectId
    ,moment = require('moment');

var chat_schema = new Schema({
    id : object_id,
    message : { type: String, required: true},
    member : { type: Schema.Types.ObjectId, ref: 'members' },
    project : { type: Schema.Types.ObjectId, ref: 'projects' },
    created : { type: Date, default: new Date() }
});

module.exports = mongoose.model('chats', chat_schema);