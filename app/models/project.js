/*!
* Model: project
*/

var mongoose = require('mongoose')
    ,crypto = require('crypto')
    ,Schema = mongoose.Schema
    ,object_id = Schema.ObjectId
    ,moment = require('moment');

var project_schema = new Schema({
    id : object_id,
    name : { type: String, required: true},
    status : { type: String, required: true},
    associate : [{
            invite_by: { type: Schema.Types.ObjectId, ref: 'members', default: null },
            member: {type: Schema.Types.ObjectId, ref: 'members'},
            type: { type: String, required: true},
            time_spent: { type: Date, default: 0},
            created : { type: Date, default: new Date()},
            updated : { type: Date, default: new Date()}
        }],
    _creator : { type: Schema.Types.ObjectId, ref: 'members' },
    type : { type: String, default: 'SYNC'},
    enable_snapboard : { type: Boolean, default: true},
    created : { type: Date, default: new Date()},
    updated : { type: Date, default: new Date()}
});

module.exports = mongoose.model('projects', project_schema);