/*!
* Model: Snapshot
*/

var mongoose = require('mongoose')
    ,Schema = mongoose.Schema
    ,object_id = Schema.ObjectId
    ,moment = require('moment');

var snapshot_schema = new Schema({
    id : object_id,
    project_id : {type: Schema.Types.ObjectId, ref: 'projects'},
    member_id : { type: Schema.Types.ObjectId, ref: 'members'},
    favicon: {type: String, default: null},
    showtime_created: {type: String, default: null},
    hostname: {type: String, default: null},
    result_id : {type: Schema.Types.ObjectId, ref: 'search_results'},
    image: {type:String, required:true},
    img_type: {type:String, required:true},
    search_by_keyword: {type:String, required:true, default: ''},
    created : { type: Date, default: new Date()},
    updated : { type: Date, default: new Date()}
});

module.exports = mongoose.model('snapshots', snapshot_schema);