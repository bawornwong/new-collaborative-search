/*!
* Model: keyword
*/

var mongoose = require('mongoose')
    ,crypto = require('crypto')
    ,Schema = mongoose.Schema
    ,object_id = Schema.ObjectId
    ,moment = require('moment');

var keyword_schema = new Schema({
    id : object_id,
    keyword : { type: String, required: true },
    project : { type: Schema.Types.ObjectId, ref: 'projects', required: true },
    seen : [{
            member: {type: Schema.Types.ObjectId, ref: 'members' },
            timeseen: [{
                    time : { type: Date, default: new Date()}
                }],
            created : { type: Date, default: new Date()},
            updated : { type: Date, default: new Date()}
        }],
    created : { type: Date, default: new Date()},
    updated : { type: Date, default: new Date()}
});

module.exports = mongoose.model('keywords', keyword_schema);