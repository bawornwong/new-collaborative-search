/*
* Routes setting
*/

// Init
module.exports = function(app, express)
{
    'use strict';

	///////////////////////////
	// Routes
	///////////////////////////
    var router = express.Router();

	// Login
    router.get('/login', controllers.member.login);
    //router.post('/login', controllers.member.login_post);
    router.post('/login', controllers.member.passport.authenticate('local', { failureRedirect: '/login', failureFlash: true }), controllers.member.login_callback);
    router.get('/auth/live', controllers.member.passport.authenticate('windowslive', { scope: ['wl.signin', 'wl.basic', 'wl.emails'] }));
    router.get('/auth/live/callback', controllers.member.passport.authenticate('windowslive', { failureRedirect: '/login', failureFlash: true }), controllers.member.login_callback );
    router.all('/logout', controllers.member.logout);
    router.get('/register', controllers.member.register_get);
    router.post('/register', controllers.member.register_post);

	// Member
    router.post('/member/search', controllers.member.search_post);

	// Dashboard
    router.all('/dashboard', helpers.authen.ensureAuthenticated, controllers.dashboard.home);
    //router.all('/set', controllers.dashboard.setsession);

	// Project
    router.post('/project/get', helpers.authen.ensureAuthenticated, controllers.project.get_list);
    router.post('/project/new', helpers.authen.ensureAuthenticated, controllers.project.new_project);
    router.post('/project/getinfo', helpers.authen.ensureAuthenticated, controllers.project.get_project);
    router.post('/project/user/admin', helpers.authen.ensureAuthenticated, controllers.project.post_admin_project_user);
    router.post('/project/user/remove', helpers.authen.ensureAuthenticated, controllers.project.post_remove_project_user);
    router.post('/project/invite/remove', helpers.authen.ensureAuthenticated, controllers.project.post_remove_invite_user);
    router.post('/project/invite', helpers.authen.ensureAuthenticated, controllers.project.post_invite_user);
    router.post('/project/invite/reject', helpers.authen.ensureAuthenticated, controllers.project.post_reject_invite);
    router.post('/project/invite/accept', helpers.authen.ensureAuthenticated, controllers.project.post_accept_invite);
    router.post('/project/status/completed', helpers.authen.ensureAuthenticated, controllers.project.post_set_completed);
    router.get('/project/:project_key', helpers.authen.ensureAuthenticated, controllers.project.home);

	// Search
    router.post('/search/get', helpers.authen.ensureAuthenticated, controllers.search.post_search);
    router.post('/search/relevant', helpers.authen.ensureAuthenticated, controllers.search.post_relevant);
    router.post('/search/comment', helpers.authen.ensureAuthenticated, controllers.search.post_comment);
    router.post('/search/result/get', helpers.authen.ensureAuthenticated, controllers.search.post_get_result);
    router.post('/search/result/seen', helpers.authen.ensureAuthenticated, controllers.search.post_result_seen);
    router.post('/search/keyword/seen', helpers.authen.ensureAuthenticated, controllers.search.post_keyword_seen);
    router.post('/search/keyword/get', helpers.authen.ensureAuthenticated, controllers.search.post_get_keyword);
    router.post('/search/getobjectid' ,helpers.authen.ensureAuthenticated, controllers.search.post_get_object_id);
    router.post('/search/getresult' ,helpers.authen.ensureAuthenticated, controllers.search.post_get_result_data);
    router.get('/search/query_suggestion' ,helpers.authen.ensureAuthenticated, controllers.search.get_query_suggestion);

	// Chat
    router.post('/chat/get', helpers.authen.ensureAuthenticated, controllers.chat.get_message);

	// Activity
    router.post('/activity/get', helpers.authen.ensureAuthenticated, controllers.activity.get_activity);

	// Snapshot
    router.post('/snapshot/create', helpers.authen.ensureAuthenticated, controllers.snapshot.post_create);
    router.post('/snapshot/crop', helpers.authen.ensureAuthenticated, controllers.snapshot.post_crop);
    router.post('/snapshot/no_crop', helpers.authen.ensureAuthenticated, controllers.snapshot.post_no_crop);
    router.post('/snapshot/get', helpers.authen.ensureAuthenticated, controllers.snapshot.get_snapshot);
    router.post('/snapshot/get_delete', helpers.authen.ensureAuthenticated, controllers.snapshot.get_snapshot_delete);
    router.post('/snapshot/get_relevance', helpers.authen.ensureAuthenticated, controllers.snapshot.get_snapshot_relevance);


    // Log
    router.get('/log/project/:project_key', controllers.logs.view_project_log);
    router.get('/log', controllers.logs.viewlog);

    //Ocr
    router.get('/ocr', controllers.ocrs.viewocr);
    router.get('/ocr/project/:project_key', controllers.ocrs.view_project_ocr);
	// Check authen
    router.all('/', function(req, res){
		res.redirect('/dashboard');
	});

    // OCR
    router.get('/test_process_ocr', controllers.ocrs.test_process_ocr);

    // PDF
    router.post('/export_pdf', controllers.pdf.get_pdf);

    // Apply router
    app.use('/', router);
};

