exports.viewocr = function(req, res){
    var errors = {};
    var messages = {};
    res.render('ocr/ocr.jade', { messages: messages, errors: errors});
};

exports.view_project_ocr = function(req, res){
    var errors = {};
    var messages = {};
    var project_key = req.params.project_key;
    messages.project_key = project_key;

    models.project
        .findOne({"_id": project_key})
        .exec(function(err, project){
            if(!err){
                console.log(project);
                messages.project = project;
                res.render('ocr/ocr.jade', { messages: messages, errors: errors});
            }
        });
};

exports.test_process_ocr = function(req, res){
    var tesseract  = require('node-tesseract');
    var _ = require('underscore');
    var path = require('path');
    var async = require('async');
    var stopwords_english = require('stopwords').english;
    var natural = require('natural');
    var NGrams = natural.NGrams;
    var fs = require('fs')
        , gm = require('gm').subClass({ imageMagick: true });
    var snapshot_store = path.join(__static, '/snapshot');

    var tasks = [];
    //console.log(stopwords_english);

    models.snapshot.find().exec(function(err, snapshots){
        _.each(snapshots, function(snapshot){
            var input_image_path = path.join(snapshot_store, 'crop-' + snapshot.image);
            var ocr_image_path = path.join(snapshot_store, 'ocr-' + snapshot.image);
            tasks.push(function(callback){
                gm(input_image_path).size(function(err, size){
                    gm(input_image_path)
                        .contrast(-1000)
                        .resize(size.width * 3, size.height * 3)
                        .write(ocr_image_path, function (err) {
                            if (err) console.log(err);
                            else{
                                tesseract.process(ocr_image_path , function(err, text) {
                                    var texts = text.replace(/(\r\n|\n|\r)/gm," ").replace(/[\.,-\/#!$%\^&\*\[\]\'"‘;:{}=\-_`~()><1234567890]/g,"").replace(/\s+/g, ' ').trim();
                                    var output = [];

                                    _.each(texts.split(" "), function(text){

                                        //callback(null, text.split(" "));
                                        if(text && !_.contains(stopwords_english, text.toLowerCase())){

                                            output.push(text);
                                            console.log(text.replace(/\n/gm," "));

                                            var textss = text;
                                            //console.log(text.split(" "));
                                            //console.log(NGrams.bigrams(text.split(" ")));
                                        }
                                    });
                                    console.log("james");
                                    console.log(texts);
                                    console.log(texts.split(" "));
                                    console.log(NGrams.bigrams(texts.split(" ")));
                                    callback(null, output);

                                });
                            }
                        });
                });
            })
        });

        async.series(tasks, function(err, texts) {
            res.json(texts);
        });
    });

};