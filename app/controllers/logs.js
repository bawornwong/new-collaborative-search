exports.viewlog = function(req, res){
	var errors = {};
	var messages = {};
    res.render('log/log.jade', { messages: messages, errors: errors});
};

exports.view_project_log = function(req, res){
	var errors = {};
	var messages = {};
    var project_key = req.params.project_key;
    messages.project_key = project_key;

    models.project
        .findOne({"_id": project_key})
        .exec(function(err, project){
            if(!err){
                console.log(project);
                messages.project = project;
                res.render('log/log.jade', { messages: messages, errors: errors});
            }
        });
};

