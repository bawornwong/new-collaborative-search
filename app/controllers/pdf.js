/**
 * Created by bawor_000 on 2/20/2016.
 */
exports.get_pdf = function(req, res){
    var htmlToPdf = require('html-to-pdf');
    htmlToPdf.convertHTMLFile('app/test.html', "app/test.pdf",
        function (error, success) {
            if (error) {
                console.log('Oh noes! Errorz!');
                console.log(error);
            } else {
                console.log('Success!');
                console.log(">>", success);
                var opener = require('opener');
                opener('app/test.pdf');
            }
        }
    );
}