// --------------------------------------------------------------------
/**
 * Method: GET Create Snapshot
 **/

exports.post_create = function (req, res) {

    var webshot = require('webshot');
    var uuid = require('node-uuid').v4();
    var path = require('path');

    var MetaInspector = require('node-metainspector');
    var easyimg = require('easyimage');
    var Promise = require('bluebird');
    var _ = require('lodash');

    //console.log(Promise.defer())

    var file_name = uuid + '.png';
    var snapshot_store = path.join(__static, '/snapshot');
    var url = req.body.url;

    var promiseWhile = function(condition, action) {
        var resolver = Promise.defer();

        var loop = function() {
            if (!condition()) return resolver.resolve();
            return Promise.cast(action())
                .then(loop)
                .catch(resolver.reject);
        };

        process.nextTick(loop);

        return resolver.promise;
    };

    function word_breaking(str) {
        var text_data = [];
        var text = ""
        for(var i = 0; i < str.length; i++){
            //console.log(str[i])
            if(str[i] == "-" || str[i] == "_" || str[i] == "." || str[i] == "=" || str[i] == "?"){
                text_data.push(text)
                text = ""
            }else{
                text += str[i]
            }
        }

        return text_data
    }

    function setpoint(data) {
        var total_point = 0
        var data_img = []
        var data_list = []

        _.forEach(data, function(img_data) {

            data_img.push({alt: img_data.toLowerCase(), point: 1/data.length})
        })


        return data_img
    }
    var client = new MetaInspector(url, {});


        //////////////////////////// send value to Create Snapshot ///////////////////////
        client.on("fetch", function () {
            var img_list =[]
            _.forEach(client.images, function(img_url) {

                if (img_url.substring(0,4) != 'http') {
                    img_list.push({src: "https:"+img_url, alt: "", score : 0, x_point: 0, y_point: 0, z_point: 0, size: 0, data_img: ""});
                }else{
                    img_list.push({src: img_url, alt: "", score : 0, x_point: 0, y_point: 0, z_point: 0, size: 0, data_img: ""});
                }
            });

            for(i in img_list){
                img_list[i].z_point = (client.images.length - i)/client.images.length
                //console.log(client.images.length - i)
            }

            var list_title = client.title.split(" ")
            var list_titles = []
            var list_titles_0 = []

            for(var i = 0; i < list_title.length; i++){
                if(list_title[i] == "-" || list_title[i] == "_"){
                    list_title.splice(i, 1)
                }
            }
            _.forEach(list_title, function(img_data) {
                list_titles.push({alt: img_data.toLowerCase(), point: 1/list_title.length})
                list_titles_0.push({alt: img_data.toLowerCase(), point: 0})
            })

            console.log(list_titles)

            //////////////////////////////// score ////////////////////////////////////
            var count = 0;

            promiseWhile(function() {
                return count < img_list.length;
            }, function() {
                return new Promise(function(resolve, reject){
                    easyimg.info(img_list[count].src).then(function (file) {
                        //console.log(file);
                        img_list[count].size = file.width * file.height;
                        img_list[count].data_img = setpoint(word_breaking(file.name))
                        //img_list[count].data_img = {alt:word_breaking(file.name), point: 1/word_breaking(file.name).length}
                        count++;
                        resolve();
                    })
                })
            }).then(function() {

                _.forEach(img_list, function(datas_img) {


                    if(datas_img.size > 345600){
                        datas_img.x_point = 5
                    }else if(datas_img.size > 172800){
                        datas_img.x_point = 4
                    }else if(datas_img.size > 84480){
                        datas_img.x_point = 3
                    }else if(datas_img.size > 36864){
                        datas_img.x_point = 2
                    }else{
                        datas_img.x_point = 1
                    }

                    //console.log("-------")

                    //////// set B
                    var b = []
                    _.forEach(datas_img.data_img, function(B) {
                        b.push({alt: B.alt, point: 0})
                    })

                    var A_value = []
                    var B_value = []
                    A_value = _.sortBy(_.unionBy(datas_img.data_img, list_titles_0, 'alt'), function(a) {return a.alt;});
                    B_value = _.sortBy(_.unionBy(list_titles, b, 'alt'), function(b) {return b.alt;});

                    datas_img.data_img = _.unionBy(list_titles, b, 'alt')
                    //console.log(A_value)
                    //console.log(B_value)

                    var num_step_AB = 0
                    var num_step_A = Math.sqrt(_.sumBy(A_value, function(o) { return o.point^2; }))
                    var num_step_B = Math.sqrt(_.sumBy(B_value, function(o) { return o.point^2; }))
                    _.forEach(A_value, function(a) {
                        _.forEach(B_value, function(b) {
                            //console.log(a.alt, b.alt)
                            if(a.alt == b.alt){
                                //console.log(a.alt, b.alt)
                                //console.log(a.point, b.point)
                                num_step_AB += a.point * b.point
                            }
                        })
                    })
                    //console.log("num_step_AB: ", num_step_AB)
                    //console.log("num_step_A: ", num_step_A)
                    //console.log("num_step_B: ", num_step_B)

                    datas_img.y_point = num_step_AB/(num_step_A * num_step_B)


                    datas_img.score = (datas_img.x_point/5) + (datas_img.y_point) + (datas_img.z_point)
                })

                //console.log(img_list)
                var list = _.reverse(_.sortBy(img_list, function(img) {
                    return img.score;
                }))
                //console.log(_.reverse(_.sortBy(img_list, function(img) {
                //    return img.score;
                //})))

                //for(i = 0; i < 3; i++){
                //    "img_" + i =  list[i].src
                //}
                webshot(url, path.join(snapshot_store, file_name), {
                    shotSize: {
                        height: 'all'
                    },
                    streamType: 'png'
                }, function (err) {

                console.log(url, file_name);
                res.send({file: file_name, s_img1: list[0].src, s_img2: list[1].src, s_img3: list[2].src, s_img4: list[3].src, s_img5: list[4].src});
                });
            });
            //////////////////////////////// X point ////////////////////////////////////
        });
        client.on("error", function (err) {
            console.log(error);
        });
        client.fetch();

        /////////////////////////////////////////////////////////////////////////////////


        //////////////////////////// send value to Create Snapshot ///////////////////////


};

var download = function(uri, filename, callback){
    request.head(uri, function(err, res, body){
        console.log('content-type:', res.headers['content-type']);
        console.log('content-length:', res.headers['content-length']);
        request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
    });
};
// --------------------------------------------------------------------

/**
 * Method: POST Crop Image
 **/
exports.post_crop = function (req, res) {
    var async = require('async');
    var easyimg = require('easyimage');
    var path = require('path');

    var send_back = {};
    var snapshot = null;

    var img_type = req.body.type;
    var project_id = req.body.project_id;
    var member_id = req.session.member.member_id;
    var result_id = req.body.result_id;

    var file_name = req.body.file_name;
    var snapshop_store = path.join(__static, '/snapshot');
    var img_src_path = path.join(snapshop_store, file_name); //***
    var img_dst_path = path.join(snapshop_store, 'crop-' + file_name); //***

    async.series({
            crop_image: function (callback) { // CropImage
                var crop = easyimg.crop({
                    src: img_src_path,
                    dst: img_dst_path,
                    cropwidth: req.body.width,
                    cropheight: req.body.height,
                    gravity: 'NorthWest',
                    x: req.body.left,
                    y: req.body.top
                });

                crop.then(function (file) {
                    console.log("jameskung!!!!! +++++ : ", file)
                    snapshot = new models.snapshot();
                    snapshot.project_id = project_id;
                    snapshot.member_id = member_id;
                    snapshot.result_id = result_id;
                    snapshot.image = file_name;
                    snapshot.favicon = null;
                    snapshot.hostname = null;
                    snapshot.img_type = img_type;
                    snapshot.showtime_created = null;
                    snapshot.search_by_keyword = req.body.search_by_keyword;
                    snapshot.save(function (err) {

                        if (!err) {
                            console.log(snapshot);
                            console.log("Add image in database already jameskung");
                            callback(null, err);
                        }
                        console.log(snapshot);
                    });
                }, function (err) {
                    console.log('Snapshot Error', err);
                    callback(err);
                });
            },

            add_on_search_result: function (callback) { // AddImage to Search Result
                console.log("Add image in database already test22222");
                if (snapshot) {
                    models.search_result
                        .findOne({'_id': result_id})
                        .populate('project', 'name status created updated associate')
                        .populate('comment.member', 'firstname lastname username type')
                        .populate('relevant.member', 'firstname lastname username type')
                        .populate('seen.member', 'firstname lastname username type')
                        .populate('')
                        .populate('snapshots.snapshot', 'image')
                        .exec(function (err, result) {
                            result.snapshots.push({snapshot: snapshot._id});
                            //result.updated = time;

                            result.save(function (err) {
                                if (err) {
                                    send_back['error'] = err;
                                }
                                else {
                                    send_back['search_result'] = result;
                                    send_back['error'] = false;
                                }

                                callback(null, err);
                            });

                        });
                } else {
                    send_back['error'] = false;
                    callback(null, null);
                }
            }
        },
        function (err, results) { // Callback
            console.log(send_back);
            send_back['user_id'] = req.session.member.member_id;
            res.json(send_back);
        });

};


// --------------------------------------------------------------------


/**
 * Method: POST No Crop Image
 **/
exports.post_no_crop = function (req, res) {


    var async = require('async');
    var easyimg = require('easyimage');
    var path = require('path');

    var send_back = {};
    var snapshot = null;

    var img_type = req.body.type;
    var project_id = req.body.project_id;
    var member_id = req.session.member.member_id;
    var result_id = req.body.result_id;

    var file_name = req.body.file_name;
    var snapshop_store = path.join(__static, '/snapshot');
    var img_src_path = path.join(file_name); //***
    var img_dst_path = path.join(file_name); //***
    //
    console.log("asdasdasdasdasd", req.body)
    async.series({
            set_image: function (callback) { // CropImage

                var savetp_snapshot = easyimg.info(file_name).then(function (file) {
                    console.log(file)
                    snapshot = new models.snapshot();
                    snapshot.project_id = project_id;
                    snapshot.member_id = member_id;
                    snapshot.result_id = result_id;
                    snapshot.image = file_name;
                    snapshot.favicon = null;
                    snapshot.hostname = null;
                    snapshot.showtime_created = null;
                    snapshot.img_type = img_type;
                    snapshot.search_by_keyword = req.body.search_by_keyword;
                    snapshot.save(function (err) {

                        if (!err) {
                            console.log(snapshot);
                            console.log("Add image in database already jameskung");
                            callback(null, err);
                        }
                        console.log(snapshot);
                    });
                }, function (err) {
                    console.log('Snapshot Error', err);
                    callback(err);
                });
            },

            add_on_search_result: function (callback) { // AddImage to Search Result
                console.log("Add image in database already test22222");
                if (snapshot) {
                    models.search_result
                        .findOne({'_id': result_id})
                        .populate('project', 'name status created updated associate')
                        .populate('comment.member', 'firstname lastname username type')
                        .populate('relevant.member', 'firstname lastname username type')
                        .populate('seen.member', 'firstname lastname username type')
                        .populate('')
                        .populate('snapshots.snapshot', 'image')
                        .exec(function (err, result) {
                            result.snapshots.push({snapshot: snapshot._id});
                            //result.updated = time;

                            result.save(function (err) {
                                if (err) {
                                    send_back['error'] = err;
                                }
                                else {
                                    send_back['search_result'] = result;
                                    send_back['error'] = false;
                                }

                                callback(null, err);
                            });

                        });
                } else {
                    send_back['error'] = false;
                    callback(null, null);
                }
            }
        },
        function (err, results) { // Callback
            console.log(send_back);
            send_back['user_id'] = req.session.member.member_id;
            res.json(send_back);
        });
};


// --------------------------------------------------------------------
/**
 * Method: GET Snapshot
 **/
exports.get_snapshot = function (req, res) {

    var favicon = require('favicon');
    var _ = require('underscore');
    var urls = require('url');
    models.snapshot
        .find({"project_id": req.body.project_id})
        .select("id image member_id result_id favicon search_by_keyword created hostname showtime_created img_type relevance")
        .populate('member_id', 'firstname')
        .populate('result_id', 'url title description')
        .exec(function (err, result) {
            for (var key in result) {

                var page_url = result[key].result_id.url;
                var client = urls.parse(page_url)
                var host_url = client.protocol + "//" + client.hostname;
                //console.log(client.hostname)
                if (client.hostname.substring(0,3) == "www"){
                    result[key].hostname = client.hostname.substring(4,client.hostname.length-4);
                }
                else{
                    result[key].hostname = client.hostname.substring(0,client.hostname.length-4);
                }
                result[key].favicon = host_url + "/favicon.ico";
                //favicon(urls.parse(page_url), function (err, favicon_url) {
                //    console.log(key, favicon_url);
                //    result[key].favicon = favicon_url;
                //    //console.log(key + result[key].search_by_keyword + "  " +result[key].favicon)
                //    return result[key].favicon;
                //});
                var month = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
                result[key].showtime_created = month[result[key].created.getMonth()] +" " + result[key].created.getDate() + ", " +result[key].created.getFullYear() + "  "
                                                +result[key].created.getHours() + ":"+result[key].created.getMinutes() + ":"+result[key].created.getSeconds()
            }
             res.send(result);
        });
};


/**
 * Method: Deleted Snapshot
 **/
//--------------------------------------------------------------------
exports.get_snapshot_delete = function (req, res) {
    var favicon = require('favicon');
    var _ = require('underscore');
    var urls = require('url');
    models.snapshot
        .find({"project_id": req.body.project_id})
        .select("id")
        .exec(function (err, result) {
            console.log(req.body.ids)

            for (var key in result) {
                if(result[key]._id == req.body.ids){
                    //console.log("*-------*"+req.body.ids)
                    models.snapshot.findByIdAndRemove(req.body.ids, function(err){
                        if (err) throw err;
                        {
                            console.log('VS deleted!');
                        }
                    })
                }
            }
            res.send(result);
        });
};

exports.get_snapshot_relevance = function (req, res) {

    models.snapshot
        .find({"project_id": req.body.project_id})
        .select("id relevance")
        .exec(function (err, result) {

            for (var key in result) {

                if(result[key]._id == req.body.id){
                    if(req.body.relevance == "relevance"){
                        models.snapshot().relevance = "relevance"
                        models.snapshot().save(function (err) {

                            if (!err) {
                                console.log("Add relevance");
                                callback(null, err);
                            }
                        });


                        result[key].relevance = "relevance"

                    }else{
                        models.snapshot().relevance = "irrelevant"
                        models.snapshot().save(function (err) {

                            if (!err) {
                                console.log("Add irrelevant");
                                callback(null, err);
                            }
                        });
                        result[key].relevance = "irrelevant"
                    }
                }
                console.log(key, result[key], req.body.id)
            }
            res.send(result);
        });
};

// --------------------------------------------------------------------
/**
 * Method: GET Snapshot (not work)
 **/
exports.post_snapboard = function (req, res) {
    console.log("add image in database already1");
    if (req.xhr) {
        console.log("add image in database already2");
        var errors = {};
        var messages = {};
        var async = require('async');
        var easyimg = require('easyimage');
        var path = require('path');
        var folder = path.join(__dirname, 'snapshot');
        var img = req.body.img;
        var url = (req.body.Url) ? req.body.Url : req.body.url;
        var project_key = req.body.key;
        var send_back = {};
        var found = false;
        var moment = require('moment');
        var time = new Date();
        pathimg = path.join(folder, "../../../static/static", img);
        async.series({
                check_exist_result: function (callback) { // Get project info


                    console.log("add image in database already3");
                    models.search_result
                        .findOne({"url": url, "project": project_key})
                        .exec(function (err, result) {
                            if (result) {
                                console.log("add image in database already4");
                                found = true;
                                callback(null, result);
                            }
                            else {
                                var data = {
                                    result_id: req.body.ID,
                                    description: req.body.Description,
                                    keyword: req.body.search_by_keyword,
                                    display_url: req.body.DisplayUrl,
                                    title: req.body.Title,
                                    url: req.body.Url,
                                    project: project_key
                                };
                                console.log("add image in database already5");
                                new_result(data, function () {
                                    found = true;
                                    callback(null, result);
                                });
                            }
                            console.log("add image in database already6");
                        });
                },
                save_snapshot: function (callback) {
                    console.log("add image in database already7");
                    if (found) {
                        console.log("add image in database already8");
                        easyimg.crop(
                            {
                                src: pathimg, dst: pathimg,
                                cropwidth: req.body.width, cropheight: req.body.height,
                                gravity: 'NorthWest',
                                x: req.body.left, y: req.body.top
                            },
                            function (err, image) {
                                if (err) throw err;
                                console.log("add image in database already10");
                                models.search_result
                                    .findOne({"url": url, "project": project_key})
                                    .populate('project', 'name status created updated associate')
                                    .populate('comment.member', 'firstname lastname username type')
                                    .populate('relevant.member', 'firstname lastname username type')
                                    .populate('seen.member', 'firstname lastname username type')
                                    .populate('snapshot.member', 'firstname lastname username type')
                                    .populate('snapshots.snapshot', 'image')


                                    .exec(function (err, result) {
                                        console.log("add image in database already012");
                                        result.snapshot.push({
                                            member: req.session.member.member_id,
                                            image_name: path.basename(img),
                                            created: time,
                                            updated: time
                                        });


                                        result.updated = time;

                                        result.save(function (err) {
                                            if (err) {
                                                send_back['error'] = err;
                                            }
                                            else {
                                                send_back['error'] = false;
                                            }
                                            callback(null, err);
                                        });
                                    });

                            }
                        )
                    }
                    else {
                        console.log("add image in database already18");
                        callback(null, null);
                    }
                },
                get_back: function (callback) {
                    if (found) {
                        models.search_result
                            .findOne({"url": url, "project": project_key})
                            .populate('project', 'name status created updated associate')
                            .populate('comment.member', 'firstname lastname username type')
                            .populate('relevant.member', 'firstname lastname username type')
                            .populate('seen.member', 'firstname lastname username type')
                            .populate('snapshot.member', 'firstname lastname username type')
                            .populate('snapshots.snapshot', 'image')

                            .exec(function (err, result) {
                                if (result) {
                                    send_back['search_result'] = result;
                                    callback(null, result);
                                }
                            });
                    }
                    else {
                        callback(null, null);
                    }
                }
            },
            function (err, results) { // Callback
                res.json(send_back);
            });
    }
    else {
        res.send(404, 'Sorry, we cannot find that!');
    }
};