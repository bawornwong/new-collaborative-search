/*
* Controls Index
*/

// Init
exports.member = require('./member.js');
exports.dashboard = require('./dashboard.js');
exports.project = require('./project.js');
exports.search = require('./search.js');
exports.chat = require('./chat.js');
exports.activity = require('./activity.js');
exports.snapshot = require('./snapshot.js');
exports.logs = require('./logs.js');
exports.ocrs = require('./ocrs.js');
exports.pdf = require('./pdf.js');