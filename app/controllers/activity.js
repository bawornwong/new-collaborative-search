/*
*  Chat
*/

// --------------------------------------------------------------------
/**
* Get activity
**/
exports.get_activity = function(req, res){
	var moment = require('moment');
    var _ = require('underscore');

	var limit = 10;
	var project_key = req.body.pkey; 
	var send_back = {};
	var update = (req.body.update != null && typeof req.body.update !== 'undefined') ? moment(req.body.update).utc() : moment.utc();
	
	models.activity
		.find({"project_id": project_key})
		.where('created').lt(update)
		.sort('-created')
		.limit(100)
		.populate('member', 'firstname lastname username')
		.select('message created member module')
		.exec(function(err, result){
			send_back['last'] = _.last(result) ;
			send_back['total'] = _.size(result);
			send_back['results'] = (result) ? result : [];
			send_back['next'] = false;


			res.json(send_back);
		});
};