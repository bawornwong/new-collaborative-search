/*
*  Socket control
*/
var passport = require('passport')
    , WindowsLiveStrategy = require('passport-windowslive').Strategy
    , LocalStrategy = require('passport-local').Strategy
    , crypto = require('crypto');

passport.serializeUser(function(user, done) {
    done(null, user);
});
passport.deserializeUser(function(obj, done) {
    done(null, obj);
});
passport.use(new LocalStrategy(
    function(username, password, done) {
        models.member.findOne({ username: username }, function(err, user) {
            if (err) { return done(err); }
            if (!user) {
                return done(null, false, { message: 'Incorrect username.' });
            }

            user.validPassword(password, function(valid){
                if (!valid) {
                    return done(null, false, { message: 'Incorrect password.' });
                }
                return done(null, {
                    'username': user.username,
                    'member_id': user._id
                });
            });
        });
    }
));
passport.use(new WindowsLiveStrategy({
        clientID: '000000004C14E6CB',
        clientSecret: 'Y3YiJiZ8aP64ec6Fyoh1oY1ma-0SHYLO',
        callbackURL: 'http://www.cozpace.com:3000/auth/live/callback'
    },
    function(accessToken, refreshToken, profile, done) {
        profile = profile._json;

        models.member.findOne({username: profile.emails.account}).exec(function(err, user){
            if(err){
                return done(err);
            }else if(user){
                return done(null, {
                    'username': user.username,
                    'member_id': user._id
                });
            } else {
                user = new models.member();
                user.username = profile.emails.account;
                user.firstname = profile.first_name;
                user.lastname = profile.last_name;
                user.password = crypto.randomBytes(20).toString('hex');
                user.time_zone = {type: 'add',hours: 7};
                user.signup_date = new Date();
                user.last_login = new Date();
                user.save(function (err, user) {
                    if(err) return done(err);
                    return done(null, {
                        'username': user.username,
                        'member_id': user._id
                    });
                })
            }
        });
    }
));
exports.passport = passport;

// --------------------------------------------------------------------
/**
* Get login page
**/
exports.login = function(req, res){

	var errors = '';
	var messages = {};

    var flash_erros = req.flash();
    if(flash_erros.error){
        errors = flash_erros.error[0];
    }

	res.render('member/login.jade', { messages: messages, errors: errors});
};

// --------------------------------------------------------------------
/**
 * Call back from passport.js login success.
 **/

exports.login_callback = function(req, res) {
    // Successful authentication, redirect dashboard.
    req.session.member = req.user;
    res.redirect('/dashboard');
};

// --------------------------------------------------------------------
/**
* Post Search Page
**/
exports.search_post = function(req, res){
	if ( req.body.q )
	{
		var find_regex = new RegExp(req.body.q, 'i');
		models.member
			.find()
			.or([{ 'firstname': find_regex }, { 'username': find_regex}])
			.select('_id member_key username firstname lastname')
			.exec(function(err, results){
				res.json(results);
			});
	}
	
		
};

// --------------------------------------------------------------------
/**
* All Logout page
**/
exports.logout = function(req, res){
	req.session.member = false;
    req.logout();
	res.redirect('/login');
};

// --------------------------------------------------------------------
/**
* Get Register Page
**/
exports.register_get = function(req, res){
	var errors = {};
	var messages = {};

	// Init value
	errors.password = {value:''};
	errors.username = {value:''};
	errors.firstname = {value:''};
	errors.lastname = {value:''};

	res.render('member/register.jade', { title: 'My Registration App', messages: messages, errors: errors });
};

// --------------------------------------------------------------------
/**
* Post Register Page
**/
exports.register_post = function(req, res){
	var errors = {};
	var messages = {};

	// Set output
	var output = function(error){
		// If error : show form
		if (error)
		{
			res.render('member/register.jade', { messages: messages, errors: errors});
		}
		else
		{
			res.render('member/welcome.jade', { messages: messages, errors: errors});
		}
	};

	// Init value
	errors.password = {value:req.body.password};
	errors.username = {value:req.body.username};
	errors.firstname = {value:req.body.firstname};
	errors.lastname = {value:req.body.lastname};

	// Set validation
	req.assert('username', 'required').notEmpty();
	req.assert('username', 'valid email required').isEmail();
	req.assert('password', 'required').notEmpty();
	req.assert('password', '6 to 20 characters required').len(6, 20);
	req.assert('firstname', 'required').notEmpty();
	req.assert('lastname', 'required').notEmpty();

	// Init Valadate
	var validate = req.validationErrors(true);

	// If not validate : set error true
	if(validate)
	{
		// Username validate
		if (validate.username)
			errors.username = validate.username;

		// Password validate
		if (validate.password)
			errors.password = validate.password;

		// Firstname validate
		if (validate.firstname)
			errors.firstname = validate.firstname;

		// Lastname validate
		if (validate.lastname)
			errors.lastname = validate.lastname;

		output(true);
	}
	else
	{
		var async = require('async');
		var usercount = 0;

		async.series({
			check_username: function(callback){ // Check invite

				models.member.count({username:req.body.username},function(err, count){
					usercount = count;
					callback(null, count);
				});
			},
			save_user : function(callback){ // Get project list3

				if ( usercount == 0 )
				{
					var crypto = require('crypto');

					var pwd_shasum = crypto.createHash('sha1');
					pwd_shasum.update(req.body.password);
					var password = pwd_shasum.digest('hex');

					var mkey_shasum = crypto.createHash('sha1');
					mkey_shasum.update(password+req.body.username);
					var member_key = mkey_shasum.digest('hex');

					member = new models.member();
					member.username = req.body.username;
					member.firstname = req.body.firstname;
					member.lastname = req.body.lastname;
					member.password = password;
					member.time_zone = {type: 'add',hours: 7};
					member.signup_date = new Date();
					member.last_login = new Date();

					member.save(function (err) {
						if (!err){
							messages.firstname = req.body.firstname;
							messages.lastname = req.body.lastname;
						}

						callback(null, err);
					});
					
				}
				else
				{
					callback(null, true);
				}

			}
		},
		function(err, results) { // Callback
			if ( results.check_username == 0 && !results.save_user )
			{
				output();
			}
			else
			{
				if ( results.check_username != 0 ) 
				{
					errors.username.msg = 'This email already used!';
				}

				output(true);
			}
		});
	}

};

