/*
*  Project control
*/

var _ = require('underscore');
// --------------------------------------------------------------------
/**
* Get home page project dashboard
**/
exports.home = function(req, res){
	var errors = {};
	var messages = {};

	messages.project_key = req.params.project_key;
	project_key = req.params.project_key;

	models.project
		.findOne({"_id": project_key, "associate.member" : req.session.member.member_id})
		.populate('associate.member', 'firstname lastname username type')
		.select('name status created updated associate')
		.exec(function(err, result){

			if ( result )
			{
				if ( result.status == 'current' )
				{
					res.render('dashboard/home.jade', {member: req.session.member, messages: messages, errors: errors});
				}
				else
				{
					res.redirect('/dashboard');
				}
			}
			else
			{
				res.redirect('/dashboard');
			}

		});
	
};

// --------------------------------------------------------------------
/**
* Ajax new project save
**/
exports.new_project = function(req, res){
	var time = new Date();

	if (req.xhr)//req.xhr == true
	{
		var project_name = req.body.project_name;

		if (project_name != '' && project_name)
		{

			project = new models.project();
		    project.name = project_name;
		    project.status = 'current';
		    project._creator = req.session.member.member_id;
		    project.created = time;
		    project.updated = time;

		    project.associate.push({member:req.session.member.member_id, type:'Master', created:time ,updated:time});
		    
		    project.save(function (err) {
		    	if (err)
		    	{
					res.json({ ack: false, message: 'Cannot create new project!' });
		    	}
		    	else
		    	{
					res.json({ ack: true, message: 'Created...' });
		    	}
		        
		    });
		}
		else
		{
			res.json({ ack: false, message: 'Blank project name!' });
		}
		
	}
	else
	{
		res.send(404, 'Sorry, we cannot find that!');
	}
};

// --------------------------------------------------------------------
/**
* Ajax project list
**/
exports.get_list = function(req, res){
	if (req.xhr)
	{

		var async = require('async');

		var status = req.body.status;
		var page = (req.body.page) ? req.body.page : 0;
		var all_status = ['current', 'completed'];
		var send_back = {
			'invite':{},
			'current':{},
			'completed':{}
		};

		async.series({
			check_invite : function(callback){ // Check invite

				models.project_invite.count({invite_to:req.session.member.member_id},function(err, count){
					//console.log("check invite...");
					//console.log(count);
					callback(null, count);
				});
			},
			project_list : function(callback){ // Get project list3

				if ( _.indexOf(all_status, status) != -1 )
				{
					models.project
						.find({"status": status,"associate.member" : req.session.member.member_id})
						.count()
						.exec(function(err, count){
							send_back[status]['total'] = count;

							models.project
								.find({"status": status,"associate.member" : req.session.member.member_id})
								.limit(10)
								.skip(page * 10)
								.sort('-created')
								.populate('associate.member', 'firstname lastname username')
								.select('name status created updated associate')
								.exec(function(err, result){
									
									send_back[status]['this_total'] = _.size(result);
									send_back[status]['results'] = result;

									callback(null, result);
								});
						});
					
				}
				else if ( status == 'invite' )
				{
					models.project_invite
						.find({invite_to : req.session.member.member_id})
						.populate('project', 'name project_key status created updated associate')
						.populate('invite_by', 'firstname lastname username')
						.limit(10)
						.skip(page * 10)
						.select('created project invite_by')
						.exec(function(err, result){
							send_back[status]['this_total'] = _.size(result);
							send_back[status]['results'] = result;
							callback(null, result);
						});
				}

			}
		},
		function(err, results) { // Callback
			var send_me = [];

			for ( var x in send_back[status]['results'] )
			{
				var me = _.filter(send_back[status]['results'][x].associate, function(associate){ return associate.member._id.toString() == req.session.member.member_id.toString(); });
				send_me[x] = me[0];
			}

			send_back[status]['me'] = send_me;			
			send_back['invite']['total']  = results.check_invite;

			res.json(send_back);
		});

	}
	else
	{
		res.send(404, 'Sorry, we cannot find that!');
	}
};

// --------------------------------------------------------------------
/**
* Get project project only ajax request
**/
exports.get_project = function(req, res){
	if (req.xhr)
	{
		var async = require('async');

		var project_key = req.body.key;
		var send_back = {};
		var permit = false;

		async.series({
			project : function(callback){ // Get project info
				models.project
					.find({"_id":project_key, "status":"current" ,"associate.member" : req.session.member.member_id})
					.count()
					.exec(function(err, count){

						if (count == 1)
						{
							permit = true;

							models.project
								.findOne({"_id": project_key})
								.populate('associate.member', 'firstname lastname username type')
								.select('name status created updated associate enable_snapboard')
								.exec(function(err, result){
									send_back['project'] = result;

									if ( result )
									{
										var me = _.filter(result.associate, function(associate){ return associate.member._id.toString() == req.session.member.member_id.toString(); });
										send_back['me'] = me[0];
									}

									callback(null, result);
								});
						}
					});
			},
			get_invite : function(callback){
				models.project_invite
					.find({"project":project_key})
					.populate('invite_by', 'firstname lastname username type')
					.populate('invite_to', 'firstname lastname username type')
					.exec(function(err, results){
						send_back['invite'] = results;
						callback(null, results);
					});
			}
		},
		function(err, results) { // Callback
			send_back['user_id'] = req.session.member.member_id;
			res.json(send_back);
		});
	}
	else
	{
		res.send(404, 'Sorry, we cannot find that!');
	}
};

// --------------------------------------------------------------------
/**
* Post set doing
**/
exports.post_set_completed = function(req, res){
	if (req.xhr)
	{
		var async = require('async');

		var project_key = req.body.key;
		var send_back = {};
		var permit = false;
		var time = new Date();

		async.series({
			check_permission : function(callback){ // Get project info
				models.project
					.find({
						$and: [
							{ "_id": project_key },
							{ "associate.member" : req.session.member.member_id },
							{
								$or : [
									{"associate.type": "Master"},
									{"associate.type": "Admin"}
								]
							}
						]
					})
					.count()
					.exec(function(err, count){
						if (count == 1)
						{
							permit = true;
							callback(null, count);					
						}
						else
						{
							callback(null, null);
						}
					});
			},
			set_completed : function(callback){
				models.project
					.findOne({"_id": project_key})
					.select('name status created updated associate')
					.exec(function(err, result){
						if (permit)
						{
							if (result.status != 'completed')
							{
								result.status = 'completed';
								result.save(function(err){
									callback(null, result);
								});
							}
							else
							{
								callback(null, result);
							}
						}
						else
						{
							callback(null, null);
						}
						
					});
			}
		},
		function(err, results) { // Callback
			send_back['permit'] = permit;
			send_back['user_id'] = req.session.member.member_id;
			res.json(send_back);
		});
	}
	else
	{
		res.send(404, 'Sorry, we cannot find that!');
	}
};

// --------------------------------------------------------------------
/**
* Post invite user
**/
exports.post_invite_user = function(req, res){
	if (req.xhr)
	{
		var async = require('async');

		var project_key = req.body.key;
		var send_back = {};
		var permit = false;
		var time = new Date();

		async.series({
			check_permission : function(callback){ // Get project info
					models.project
					.findOne({"_id":project_key, "associate.member" : req.session.member.member_id})
					.exec(function(err, result){
						if ( result )
						{
							var me = _.filter(result.associate, function(associate){
                                return associate.member.toString() == req.session.member.member_id.toString();
                            });
							if(me.length && (me[0].type == 'Master' || me[0].type == 'Admin')){
								permit = true;
								send_back['permit'] = true;
							}
							else
							{
								send_back['permit'] = false;
							}
						}

						callback(null, result);
					});
			},
			save_invite : function(callback){
				if ( permit )
				{
					var invite_to = req.body.id;

					project_invite = new models.project_invite();
				    project_invite.project = project_key;
				    project_invite.invite_by = req.session.member.member_id;
				    project_invite.invite_to = invite_to;
				    project_invite.created = time;

				    project_invite.save(function (err) {

				    	if ( err )
				    	{
				    		send_back['error'] = err;
				    	}
				    	else
				    	{
				    		send_back['error'] = false;
				    		send_back['project_invite'] = project_invite;
				    		callback(null, project_invite);
				    	}
				    	
				    });

				}
				else
				{
					callback(null, false);
				}
			}
		},
		function(err, results) { // Callback
			res.json(send_back);
		});

		
	}
};

// --------------------------------------------------------------------
/**
* Post remove invite user
**/
exports.post_remove_invite_user = function(req, res){
	if (req.xhr)
	{
		var async = require('async');

		var project_key = req.body.key;
		var invite_id = req.body.invite_id;
		var send_back = {};
		var permit = false;
		var time = new Date();

		async.series({
			check_permission : function(callback){ // Get project info
				models.project
					.findOne({"_id":project_key, "associate.member" : req.session.member.member_id})
					.exec(function(err, result){
						if ( result )
						{
							var me = _.filter(result.associate, function(associate){ return associate.member.toString() == req.session.member.member_id.toString(); });
							if ( me && ( me[0].type == 'Master' || me[0].type == 'Admin' ) )
							{
								permit = true;
								send_back['permit'] = true;
							}
							else
							{
								send_back['permit'] = false;
							}
								
						}
						else
						{
							send_back['permit'] = false;
						}

						callback(null, result);
					});
			},
			remove_invite : function(callback){
				if ( permit )
				{

				    models.project_invite
						.remove({"_id": invite_id})
						.exec(function(err){

							if ( err )
					    	{
					    		send_back['error'] = err;
					    	}
					    	else
					    	{
					    		send_back['error'] = false;
					    	}

					    	callback(null, err);
						});

				}
				else
				{
					callback(null, false);
				}
			}
		},
		function(err, results) { // Callback
			res.json(send_back);
		});

		
	}
};

// --------------------------------------------------------------------
/**
* Post remove project user
**/
exports.post_admin_project_user = function(req, res){
	if (req.xhr)
	{
		var async = require('async');

		var project_key = req.body.key;
		var associate_id = req.body.associate_id;
		var member_id = req.body.member_id;
		var admin_action = req.body.admin_action;
		var send_back = {};
		var permit = false;
		var time = new Date();

		async.series({
			check_permission : function(callback){ // Get project info
				models.project
					.findOne({"_id":project_key, "associate.member" : req.session.member.member_id})
					.exec(function(err, result){
						if ( result )
						{
							var me = _.filter(result.associate, function(associate){ return associate.member.toString() == req.session.member.member_id.toString(); });
							if ( me && (req.session.member.member_id != member_id && ( me[0].type == 'Master' || me[0].type == 'Admin' )) )
							{
								permit = true;
								send_back['permit'] = true;
							}
							else
							{
								send_back['permit'] = false;
							}
								
						}
						else
						{
							send_back['permit'] = false;
						}

						callback(null, result);
					});
			},
			admin_user : function(callback){
				if ( permit )
				{

				    models.project
						.findOne({"_id": project_key})
						.exec(function(err, result){
							if ( result )
					    	{

					    		// Get and Check admin action
					    		var associate = result.associate.id(associate_id);
					    		if ( admin_action == 'grant' )
					    		{
					    			associate.type = 'Admin';
					    		}
					    		else
					    		{
					    			associate.type = 'User';
					    		}
					    		
					    		associate.updated = time;
					    		result.updated = time;
					    		// Save
					    		result.save(function (err) {
					    			if ( err )
					    			{
					    				send_back['error'] = err;
					    			}
					    			else
					    			{
					    				send_back['error'] = false;
					    			}
					    		});
					    		

					    	}
					    	else
					    	{
					    		send_back['error'] = true;
					    		
					    	}

					    	callback(null, result);
						});

				}
				else
				{
					callback(null, false);
				}
			}
		},
		function(err, results) { // Callback
			res.json(send_back);
		});

		
	}
};

// --------------------------------------------------------------------
/**
* Post remove project user
**/
exports.post_remove_project_user = function(req, res){
	if (req.xhr)
	{
		var async = require('async');

		var project_key = req.body.key;
		var associate_id = req.body.associate_id;
		var member_id = req.body.member_id;
		var send_back = {};
		var permit = false;
		var time = Date();

		async.series({
			check_permission : function(callback){ // Get project info
				models.project
					.findOne({"_id":project_key, "associate.member" : req.session.member.member_id})
					.exec(function(err, result){
						if ( result )
						{
							var me = _.filter(result.associate, function(associate){ return associate.member.toString() == req.session.member.member_id.toString(); });
							if ( me && (req.session.member.member_id != member_id && ( me[0].type == 'Master' || me[0].type == 'Admin' )) )
							{
								permit = true;
								send_back['permit'] = true;
							}
							else
							{
								send_back['permit'] = false;
							}
								
						}
						else
						{
							send_back['permit'] = false;
						}

						callback(null, result);
					});
			},
			remove_user : function(callback){
				if ( permit )
				{

				    models.project
						.findOne({"_id": project_key})
						.exec(function(err, result){
							if ( result )
					    	{
					    		result.associate.id(associate_id).remove();
					    		result.updated = time;
					    		result.save(function (err) {

					    			if ( err )
					    			{
					    				send_back['error'] = err;
					    			}
					    			else
					    			{
					    				send_back['error'] = false;
					    			}
					    			
					    			callback(null, err);
					    		});
					    	}
					    	else
					    	{
					    		send_back['error'] = true;
					    		callback(null, result);
					    	}
						});

				}
				else
				{
					callback(null, false);
				}
			}
		},
		function(err, results) { // Callback
			res.json(send_back);
		});

		
	}
};

// --------------------------------------------------------------------
/**
* Post accept invite
**/
exports.post_accept_invite = function(req, res){
	if (req.xhr)
	{
		var async = require('async');

		var project_key = req.body.key;
		var invite_id = req.body.invite_id;
		var invite_by = null;
		var send_back = {};
		var permit = false;
		var complete = false;
		var time = new Date();

		async.series({
			check_permission : function(callback){ // Get project info
				models.project_invite
					.findOne({"project":project_key, "invite_to" : req.session.member.member_id})
					.exec(function(err, result){
						if ( result )
						{
							invite_by = result.invite_by;
							permit = true;
							send_back['permit'] = true;
						}
						else
						{
							permit = false;
							send_back['permit'] = false;
						}

						callback(null, result);
					});
			},
			save_in_project : function(callback){
				models.project
					.findOne({"_id":project_key})
					.exec(function(err,result){
						//console.log(result);
						if ( result && permit )
						{
							result.associate.push({member:req.session.member.member_id, type:'User', invite_by: invite_by, created: time, updated: time});

							result.save(function (err) {
						    	if (err)
						    	{
									send_back['error'] = err;
						    	}
						    	else
						    	{
									send_back['error'] = false;
									send_back['project'] = result;
									complete = true;
						    	}

						    	callback(null, err);
						        
						    });
						}
						else
						{
							callback(null, null);
						}
					});
			},
			delete_invite : function(callback){
				if ( complete )
				{
					models.project_invite
						.remove({"project":project_key, "invite_to" : req.session.member.member_id})
						.exec(function(err){
							if ( err )
							{
								send_back['delerror'] = err;
							}
							else
							{
								send_back['delerror'] = false;
							}

							callback(null, err);
						});
				}
				else
				{
					callback(null, null);
				}
				
			}
		},
		function(err, results) { // Callback
			res.json(send_back);
		});
	}
}

// --------------------------------------------------------------------
/**
* Post reject invite
**/
exports.post_reject_invite = function(req, res){
	if (req.xhr)
	{
		var async = require('async');

		var project_key = req.body.key;
		var invite_id = req.body.invite_id;
		var send_back = {};
		var permit = false;

		async.series({
			check_permission : function(callback){ // Get project info
				models.project_invite
					.remove({"project":project_key, "invite_to" : req.session.member.member_id})
					.exec(function(err){
						if ( err )
						{
							send_back['error'] = err;
						}
						else
						{
							send_back['error'] = false;
						}

						callback(null, err);
					});
			}
		},
		function(err, results) { // Callback
			res.json(send_back);
		});
	}
}