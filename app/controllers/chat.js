/*
*  Chat
*/

// --------------------------------------------------------------------
/**
* Get chat message
**/

exports.get_message = function(req, res){
	var moment = require('moment');
    var _ = require('underscore');

	var limit = 10;
	var project_key = req.body.pkey; 
	var send_back = {};
	var update = (req.body.update != null && typeof req.body.update !== 'undefined') ? moment(req.body.update).utc() : moment.utc();
	
	models.chat
		.find({"project": project_key})
		.where('created').lt(update)
		.limit(limit)
		.sort('-created')
		.populate('member', 'firstname lastname username')
		.select('message created member')
		.exec(function(err, result){
			send_back['last'] = _.last(result) ;
			send_back['total'] = _.size(result);
			send_back['results'] = (result) ? result : [];

			if (send_back.total < limit)
			{
				send_back['next'] = false;
			}
			else
			{
				send_back['next'] = true;
			}

			res.json(send_back);
		});
};