/*
*  Search
*/

var _ = require('underscore');

// --------------------------------------------------------------------
/**
* Method: New result
**/
var new_result = function(data, callback){
	search_result = new models.search_result();
    search_result.result_id = data.result_id;
    search_result.description = data.description;
    search_result.display_url = data.display_url;
	search_result.x = data.x;
    search_result.url = data.url;
    search_result.title = data.title;
	search_result.host = data.host;
    search_result.project = data.project;
    search_result.created = new Date();
    search_result.updated = new Date();

    if (data.project)
    {
    	search_result.save(function (err) {
            console.log('Save', search_result.result_id, search_result.project, err);
			callback();
	    });
    }
	
};

// --------------------------------------------------------------------
/**
* Method: New keyword
**/
var new_keyword = function(data, callback){
	keyword = new models.keyword();
    keyword.keyword = data.keyword;
    keyword.project = data.project;
    keyword.created = new Date();
    keyword.updated = new Date();

    if (data.project)
    {
    	keyword.save(function (err) {
			callback();
	    });
    }
	
};

// --------------------------------------------------------------------
/**
* Method: Post relevant
**/
exports.post_relevant = function(req, res){
	if (req.xhr)
	{
		var async = require('async');

		var project_key = req.body.key;
		var relevant = req.body.relevant;
		var irrelevant = req.body.irrelevant;
		var url = (req.body.Url) ? req.body.Url:req.body.url;
		var send_back = {};
		var found = false;
		var time = new Date();

		var state = 'Not relevant';

		if ( relevant == 'true' && irrelevant == 'true' )
		{
			state = 'Maybe';
		}
		else if ( relevant == 'true' && irrelevant == 'false' )
		{
			state = 'Relevant';
		}
		else if ( relevant == 'false' && irrelevant == 'true' )
		{
			state = 'Irrelevant';
		}

		async.series({
			check_exist_result : function(callback){ // Get project info
				models.search_result
					.findOne({"url":url, "project": project_key})
					.exec(function(err, result){

						if ( result )
						{
							found = true;
							callback(null, result);
						}
						else
						{
							var data = {
									result_id : req.body.ID,
									description : req.body.Description,
									display_url : req.body.DisplayUrl,
									host : req.body.hostname,
									title : req.body.Title,
									url : req.body.Url,
									project : project_key
								};

							new_result(data, function(){ found = true; callback(null, result); });
						}
					});
			},
			save_relevent : function(callback){
				if ( found )
				{
					models.search_result
						.findOne({"url":url, "project": project_key})
						.populate('project', 'name status created updated associate')
						.populate('comment.member', 'firstname lastname username type')
						.populate('relevant.member', 'firstname lastname username type')
						.populate('seen.member', 'firstname lastname username type')
                        .populate('snapshots.snapshot', 'image')
						.exec(function(err, result){

                            if (result)
                            {
                                var me = _.filter(result.relevant, function(relevant){ if (relevant) return relevant.member._id.toString() == req.session.member.member_id.toString(); });

								if ( me[0] )
                                {
                                    me[0].state = state;
                                    me[0].updated = time;

									if (me.length > 1) {
										_.each(me, function(element, index, list){
											if(index != 0){
												element.remove();
											}
										});
									}
                                }
                                else
                                {
                                    result.relevant.push({member:req.session.member.member_id, state:state, created:time, updated: time});
                                }

                                result.updated = time;

                                result.save(function(err){
                                    if ( err )
                                    {
                                        send_back['error'] = err;
                                    }
                                    else
                                    {
                                        send_back['error'] = false;
                                    }

                                    callback(null, err);
                                });
                            }
                            else
                            {
                                send_back['error'] = true;
                                callback(null, true);
                            }

							
						});
				}
				else
				{
					callback(null, null);
				}
				
			},
			get_back : function(callback){
				if ( found )
				{
					models.search_result
						.findOne({"url":url, "project": project_key})
						.populate('project', 'name status created updated associate')
						.populate('comment.member', 'firstname lastname username type')
						.populate('relevant.member', 'firstname lastname username type')
						.populate('seen.member', 'firstname lastname username type')
						.populate('snapshots.snapshot', 'image')
						.exec(function(err, result){
							if ( result )
							{
								send_back['search_result'] = result;
								callback(null, result);
							}
						});
				}
				else
				{
					callback(null, null);
				}
			}
		},
		function(err, results) { // Callback
			send_back['state'] = state;
			res.json(send_back);
		});
	}
	else
	{
		res.send(404, 'Sorry, we cannot find that!');
	}
};

// --------------------------------------------------------------------
/**
* Method: Post comment
**/
exports.post_comment = function(req, res){
	if (req.xhr)
	{
		var async = require('async');

		var message = req.body.message;
		var url = (req.body.Url) ? req.body.Url:req.body.url;
		var project_key = req.body.key;
		var send_back = {};
		var found = false;
		var moment = require('moment');
		var time = new Date();
		console.log("comment1");
		async.series({
			check_exist_result : function(callback){ // Get project info
				models.search_result
					.findOne({"url":url, "project": project_key})
					.exec(function(err, result){
						console.log("comment2");
						if ( result )
						{
							found = true;
							callback(null, result);
						}
						else
						{

							var data = {
									result_id : req.body.ID,
									description : req.body.Description,
									display_url : req.body.DisplayUrl,
									title : req.body.Title,
									url : req.body.Url,
									project : project_key
								};

							new_result(data, function(){ found = true; callback(null, result); });
						}
					});
			},
			save_comment : function(callback){
				console.log("comment3");
				if ( found )
				{
					models.search_result
						.findOne({"url":url, "project": project_key})
						.populate('project', 'name status created updated associate')
						.populate('comment.member', 'firstname lastname username type')
						.populate('relevant.member', 'firstname lastname username type')
						.populate('seen.member', 'firstname lastname username type')
						.populate('snapshots.snapshot', 'image')

						.exec(function(err, result){

							result.comment.push({member:req.session.member.member_id, message:message, created: time, updated: time});
							result.updated = time;

							result.save(function(err){
								if ( err )
								{
									send_back['error'] = err;
								}
								else
								{
									send_back['error'] = false;
								}

								callback(null, err);
							});
							
						});
				}
				else
				{
					callback(null, null);
				}
				
			},
			get_back : function(callback){
				console.log("comment4");
				if ( found )
				{
					models.search_result
						.findOne({"url":url, "project": project_key})
						.populate('project', 'name status created updated associate')
						.populate('comment.member', 'firstname lastname username type')
						.populate('relevant.member', 'firstname lastname username type')
						.populate('seen.member', 'firstname lastname username type')
						.populate('snapshots.snapshot', 'image')
						.exec(function(err, result){
							if ( result )
							{
								send_back['search_result'] = result;
								callback(null, result);
							}
						});
				}
				else
				{
					callback(null, null);
				}
			}
		},
		function(err, results) { // Callback
            console.log(send_back);
			res.json(send_back);
			console.log("comment5");
		});
	}
	else
	{
		res.send(404, 'Sorry, we cannot find that!');
	}
};


// --------------------------------------------------------------------
/**
* Method: Post result seen
**/
exports.post_result_seen = function(req, res){
	if (req.xhr)
	{
		var async = require('async');

		var message = req.body.message;
		var url = (req.body.Url) ? req.body.Url:req.body.url;

		var project_key = req.body.key;
		var send_back = {};
		var found = false;
		var moment = require('moment');
		var time = new Date();

		console.log("post_result_seen: xxxxxx", req.body)

		async.series({
			check_exist_result : function(callback){ // Get project info
				models.search_result
					.findOne({"url": url, "project": project_key})
					.exec(function(err, result){

						if ( result )
						{
							found = true;
							callback(null, result);
						}
						else
						{
							var data = {
									result_id : req.body.ID,
									description : req.body.Description,
									display_url : req.body.DisplayUrl,
									x : req.body.Description,
									title : req.body.Title,
									url : req.body.Url,
									project : project_key
								};
							new_result(data, function(){ found = true; callback(null, result); });
						}
					});
			},
			save_seen : function(callback){
				if ( found )
				{
					models.search_result
						.findOne({"url": url, "project": project_key})
						.populate('project', 'name status created updated associate')
						.populate('comment.member', 'firstname lastname username type')
						.populate('relevant.member', 'firstname lastname username type')
						.populate('seen.member', 'firstname lastname username type')
						.populate('snapshots.snapshot','image img_type')
						.exec(function(err, result){
                            console.log(result);

                            if (result)
                            {
                                // Check Prev Bug
                                var checkDuplicate = _.filter(result.seen, function(seen){ if (seen) return seen.member._id.toString() == req.session.member.member_id.toString(); });

                                if (checkDuplicate.length > 1) {
                                    _.each(checkDuplicate, function(element, index, list){
                                        if(index != 0){
                                            element.remove();
                                        }
                                    });
                                }

                                // Check Exist Me
                                var me = _.find(result.seen, function(seen){ return seen.member._id.toString() == req.session.member.member_id.toString(); });
                                if ( me )
                                {
                                    me.timeseen.push({time:time});
                                    me.updated = time;
                                }
                                else
                                {
                                    result.seen.push({
                                        member:req.session.member.member_id,
                                        updated: time,
                                        created: time,
                                        timeseen:[{time: time}]
                                    });
                                }

                                result.updated = time;
                                result.save(function(err){
                                    if ( err )
                                    {
                                        send_back['error'] = err;
                                    }
                                    else
                                    {
                                        send_back['error'] = false;
                                    }

                                    callback(null, err);
                                });
                            }
                            else
                            {
                                callback(null, null);
                            }
						});
				}
				else
				{
					callback(null, null);
				}
				
			},
			get_back : function(callback){
				if ( found )
				{
					models.search_result
						.findOne({"url":url, "project": project_key})
						.populate('project', 'name status created updated associate')
						.populate('comment.member', 'firstname lastname username type')
						.populate('relevant.member', 'firstname lastname username type')
						.populate('seen.member', 'firstname lastname username type')
						.populate('snapshots.snapshot','image img_type')
						.exec(function(err, result){
							if ( result )
							{
								console.log("post_result_seen: ", result)
								send_back['search_result'] = result;
								callback(null, result);
								//console.log('seen');
							}
						});
				}
				else
				{
					callback(null, null);
				}
			}
		},
		function(err, results) { // Callback
			res.json(send_back);
		});
	}
	else
	{
		res.send(404, 'Sorry, we cannot find that!');
	}
};

// --------------------------------------------------------------------
/**
* Method: Keyword seen
**/
exports.post_keyword_seen = function(req, res){
	if (req.xhr)
	{
		var async = require('async');
		var keyword = req.body.keyword;
		var project_key = req.body.key;
		var send_back = {};
		var found = false;
		var moment = require('moment');
		var time = new Date();

		async.series({
			check_exist_keyword : function(callback){ // Get project info
				models.keyword
					.findOne({"keyword":keyword, "project": project_key})
					.exec(function(err, result){
						if ( result )
						{
							found = true;
							callback(null, result);
						}
						else
						{
							var data = {
									keyword : keyword,
									project : project_key
								};
							new_keyword(data, function(){ found = true; callback(null, result); });
						}
					});
			},
			save_seen : function(callback){
				if ( found )
				{
					models.keyword
						.findOne({"keyword":keyword, "project": project_key})
						.populate('project', 'name status created updated associate')
						.populate('seen.member', 'firstname lastname username type')
						.exec(function(err, result){

                            // Check Prev Bug
                            var checkDuplicate = _.filter(result.seen, function(seen){ if (seen) return seen.member._id.toString() == req.session.member.member_id.toString(); });

                            if (checkDuplicate.length > 1) {
                                _.each(checkDuplicate, function(element, index, list){
                                    if(index != 0){
                                        element.remove();
                                    }
                                });
                            }

                            // Check Exist Me
							var me = _.find(result.seen, function(seen){ return seen.member._id.toString() == req.session.member.member_id.toString(); });
							if ( me )
							{
								me.timeseen.push({timeseen:time});
								me.updated = time;
							}
							else
							{
								result.seen.push({
									member:req.session.member.member_id,
									updated: time,
									created: time,
									timeseen:[{time: time}]
								});
							}

							result.updated = time;
							result.save(function(err){
								if ( err )
								{
									send_back['error'] = err;
								}
								else
								{
									send_back['error'] = false;
								}

								callback(null, err);
							});
							
						});
				}
				else
				{
					callback(null, null);
				}
				
			},
			get_back : function(callback){
				if ( found )
				{
					models.keyword
						.findOne({"keyword":keyword, "project": project_key})
						.populate('project', 'name status created updated associate')
						.populate('seen.member', 'firstname lastname username type')
						.exec(function(err, result){
							if ( result )
							{
								send_back['keyword'] = result;
								callback(null, result);
							}
						});
				}
				else
				{
					callback(null, null);
				}
			}
		},
		function(err, results) { // Callback
			res.json(send_back);
		});
	}
	else
	{
		res.send(404, 'Sorry, we cannot find that!');
	}
};

// --------------------------------------------------------------------
/**
* Get search
**/
exports.post_search = function(req, res){

	var query = req.body.query;
	var project_key = req.body.pkey;
	var send_back = {};

	var client = helpers.bing_search_api({
        acctKey: settings.BING_ACCT_ID
    });

	client.search(query, function(error, response, data) {
		if (req.xhr)
		{
			if (!error && response.statusCode >= 200 && response.statusCode < 300)
			{
				send_back = JSON.parse(data);

                models.search_result
                    .find({"project":project_key})
                    .populate('project', 'name status created updated associate')
                    .populate('comment.member', 'firstname lastname username type')
                    .populate('relevant.member', 'firstname lastname username type')
                    .populate('seen.member', 'firstname lastname username type')
					.populate('snapshots.snapshot','image img_type')
                    .exec(function(err, results){

                        send_back['search_result'] = results;
                        send_back['user_id'] = req.session.member.member_id;
//                        send_back['suggests'] = suggests;
                        //console.log(send_back);
                        res.json(send_back);
                    });
					
			}
			else
			{
				res.json({});
			}
		}
		else
		{
			res.send(404, 'Sorry, we cannot find that!');
		}
	});
};

// --------------------------------------------------------------------
/**
* Method: Post get result
**/
exports.post_get_result = function(req, res){

	if (req.xhr)
	{
		var async = require('async');

		var project_key = req.body.key;
		var send_back = {};
		var found = false;


		async.series({
			get_result : function(callback){ // Get project info
				models.search_result
					.find({"project": project_key})
					.populate('project', 'name status created updated associate')
					.populate('comment.member', 'firstname lastname username type')
					.populate('relevant.member', 'firstname lastname username type')
					.populate('seen.member', 'firstname lastname username type')
                    .populate('snapshots.snapshot','image img_type')
					.exec(function(err, result){
						found = true;
						console.log("post_get_result: ", result)
						send_back['search_result'] = result;
						callback(null, result);

					});
			}
		},
		function(err, results) { // Callback
			send_back['user_id'] = req.session.member.member_id;
			res.json(send_back);
		});
	}
	else
	{
		res.send(404, 'Sorry, we cannot find that!');
	}
};


// --------------------------------------------------------------------
/**
* Method: Post get keyword
**/
exports.post_get_keyword = function(req, res){
	if (req.xhr)
	{
		var async = require('async');

		var project_key = req.body.key;
		var send_back = {};
		var found = false;

		async.series({
			get_keyword : function(callback){ // Get project info
				models.keyword
					.find({"project": project_key})
					.populate('seen.member', 'firstname lastname username type')
					.exec(function(err, result){
						found = true;
						send_back['keyword'] = result;
						callback(null, result);

					});
			}
		},
		function(err, results) { // Callback
			send_back['user_id'] = req.session.member.member_id;
			res.json(send_back);
		});
	}
	else
	{
		res.send(404, 'Sorry, we cannot find that!');
	}
};

///find id

exports.post_get_object_id = function(req, res){
    var project_id = req.body.project_id;
    var url = req.body.url;
    models.search_result
        .findOne({"project":project_id,"url":url})
        .exec(function(err,result){
            res.json(result);
        });
};


exports.post_get_result_data = function(req, res){
    var project_id = req.body.project_id;
    var url = req.body.url;
    models.search_result
        .findOne({"project":project_id,"url":url})
        .populate('project', 'name status created updated associate')
        .populate('comment.member', 'firstname lastname username type')
        .populate('relevant.member', 'firstname lastname username type')
        .populate('seen.member', 'firstname lastname username type')
		.populate('snapshots.snapshot','image img_type')
        .exec(function(err, result){
            res.json(result);
        });

};



exports.get_query_suggestion = function(req, res){
    var query = req.query.term.trim();
//    models.search_result
//        .findOne({"project":project_id,"url":url})
//        .populate('project', 'name status created updated associate')
//        .populate('comment.member', 'firstname lastname username type')
//        .populate('relevant.member', 'firstname lastname username type')
//        .populate('seen.member', 'firstname lastname username type')
//        .populate('snapshots.snapshot','image')
//        .exec(function(err, result){
//            res.json(result);
//        });

    var client = helpers.bing_search_api({
        acctKey: settings.BING_ACCT_ID
    });

    client.relate(query, function(error, response, data){
        var suggests = [];

        if( typeof data !== "undefined" ){
            data = JSON.parse(data);
            data.d.results.forEach(function(d){
                suggests.push(d.Title);
            });
        }

        res.json(suggests);
    });
};

