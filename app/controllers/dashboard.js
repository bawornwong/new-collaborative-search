/*
*  Dashboard
*/

// Get home page dashboard
exports.home = function(req, res){

	var errors = {};
	var messages = {};
	res.render('dashboard/home.jade', {member: req.session.member, messages: messages, errors: errors});
};