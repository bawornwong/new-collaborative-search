/*!
 * Bing Search Client for Node.js
 * Copyright(c) 2011 Sujal Shah <sujal@variolabs.com>
 * Source covered by the MIT License
 *
 * Portions from or inspired by the twitter-node library by
 * Jeff Waugh (https://github.com/jdub/node-twitter)
 * including the constructor/options storage
 *
 */

var request = require('request')
    , url = require('url')
    , querystring = require('querystring')
    , _ = require('underscore')
    , crypto = require('crypto');;

exports = module.exports;

var SearchClient = function(options)
{
    if (!(this instanceof SearchClient)) return new SearchClient(options);

    var defaults = {
        /*
         * Should be a space delimited list of valid bing sources:
         * Image, News, PhoneBook, RelatedSearch, Spell, Translation,
         * Video, Web
         */
        sourceList: "Web",
        relatedList: "RelatedSearch"

        /*
         * Access key
         */
        , acctKey: null

        /*
         * Base URL for API, includes the question mark
         */
        , baseUrl: "https://api.datamarket.azure.com/Bing/Search/v1/"

        /*
         * API Version
         */
        , bingApiVersion: "2.2"

        /*
         * Bing API Market
         */
        , bingApiMarket: "en-US"

        /*
         * Limit responses to this amount
         */
        , limit: 10

        /*
         * User Agent
         *
         */
        , userAgent: "Bing Search Client for Node.js ("+SearchClient.version+")"

    };

    if (options === null || typeof options !== 'object')
        options = {};

    // merge options passed in with defaults
    this.options = _.extend(defaults, options);

};

SearchClient.version = "0.0.1";
module.exports = SearchClient;

/*
 * search(query, options, callback)
 *
 * callback gets 3 arguments back, basically the responses from
 * the underlying request object, except that body is turned into objects
 */
SearchClient.prototype.search = function(query, options, callback) {

    if (typeof options === 'function') {
        callback = options;
        options = null;
    }

    if ( typeof callback !== 'function' ) {
        throw "ERROR: Callback function required, was not present or of type function";
        return this;
    }

    var finalOptions = this.options;

    if (options !== null)
    {
        finalOptions = _.extend(this.options, options);
    }

    var authKey = Buffer(finalOptions.acctKey+":"+finalOptions.acctKey).toString('base64');
    var queryStr = querystring.stringify({
        "$format": "json",
        "Query": "'"+query+"'"
    });
    var queryUrl = url.parse(finalOptions.baseUrl+finalOptions.sourceList+'?'+queryStr);

    request({
        uri: queryUrl.href,
        method: finalOptions.method || "GET",
        headers: {
            "User-Agent": finalOptions.userAgent,
            "Authorization": "Basic "+authKey,
            "request_fulluri": true,
            "ignore_errors": true
        }
    }, function(error, response, body){
        callback(error, response, body);
    });

};

/*
 * suggest(query, options, callback)
 *
 */
SearchClient.prototype.relate = function(query, options, callback) {

    if (typeof options === 'function') {
        callback = options;
        options = null;
    }

    if ( typeof callback !== 'function' ) {
        throw "ERROR: Callback function required, was not present or of type function";
        return this;
    }

    var finalOptions = this.options;

    if (options !== null)
    {
        finalOptions = _.extend(this.options, options);
    }

    var authKey = Buffer(finalOptions.acctKey+":"+finalOptions.acctKey).toString('base64');
    var queryStr = querystring.stringify({
        "$format": "json",
        "Query": "'"+query+"'"
    });
    var queryUrl = url.parse(finalOptions.baseUrl+finalOptions.relatedList+'?'+queryStr);

    request({
        uri: queryUrl.href,
        method: finalOptions.method || "GET",
        headers: {
            "User-Agent": finalOptions.userAgent,
            "Authorization": "Basic "+authKey,
            "request_fulluri": true,
            "ignore_errors": true
        }
    }, function(error, response, body){
        callback(error, response, body);
    });

};
