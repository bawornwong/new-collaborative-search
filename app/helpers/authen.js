/*
* Authen helper
*/

// Init
var async = require('async');

// user authen redirect
exports.user_authen = function(req, res, next){
	var logedin = false;
	var session = req.session.member;

	async.series({
		check_session : function(callback){ // Check session
			//console.log('Checking....');

			if (session != null && session != false && session != {} && typeof session !== "undefined")
			{
				//console.log('Loged in....');
				logedin = true
			}

			callback(null, logedin);
		},
		member_count : function(callback){ // Check in DB
			//console.log('Checking db');
			if (logedin == true)
			{
				models.member.count({'username':session.username,'_id':session.member_id},function(err, count){
					//console.log('querying db');


					if(count != 1)
					{
						logedin = false;
						session = false;
						//console.log('no login');
					}

					callback(null, count);
				});
			}
			else
			{
				//console.log('no check db');
				callback(null);
			}
		}
	},
	function(err, results) { // Callback
		//console.log("Running...");
		//console.log(err);
		//console.log(results);
		//console.log(logedin);

		if (!logedin)
	    {
	    	res.redirect('/login');
	    }
	    else
	    {
	    	next();
	    }
	});
	
};

exports.ensureAuthenticated = function ensureAuthenticated(req, res, next) {
	if (req.isAuthenticated()) { return next(); }
    var auth = req.isAuthenticated();
	res.redirect('/login')
};