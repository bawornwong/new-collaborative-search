/*
* Socket setting
*/

// Init
module.exports = function(app, server)
{
    var io = require('socket.io')(server);
	var cookie = require('cookie');
	var moment = require('moment');
    var _ = require('underscore');
	var chat_room = {};
    var mongoose = require('mongoose');

	// Authorization
	io.set('authorization', function (data, accept) {
        var signature = require( "cookie-signature" );
        var prefix = "s:";

	    if (data.headers.cookie) {
	        data.cookie = cookie.parse(data.headers.cookie);
	        data.sessionID = data.cookie['express.sid'];

            if (data.sessionID) {
                data.sessionID = data.sessionID.replace( prefix, "" );
                data.sessionID = signature.unsign( data.sessionID, session_secret );
            } else {
                return accept('No cookie transmitted.', false);
            }

	    } else {
	    	return accept('No cookie transmitted.', false);
	    }

	    session_store.get(data.sessionID, function(error, session){
	    	if (session && session.member)
	    	{
	    		data.member = session.member;
                //console.log(data);
	    	}

	    	accept(null, true);
	    });
	});

	// Configure
    //io.set('transports', ['websocket','xhr-polling']);
    //io.set("polling duration", 10);
    //io.set('log level', 2);

	// Socketserver
	io.of('/socketserver')
	.on('connection', function (socket) {


		// Check login
		if(!socket.request.member)
		{
            console.log('Socket disconnect');
			socket.disconnect();
			return;
		}

        // Define socketlog funtion for send log to client.
        var socketlog = function(text ,showon){

            if( !showon || (showon && app.settings.env == showon) )
            {
                socket.emit('log', text);
            }

        };

        // Member Data
        var member = socket.request.member;

		// Setting to socket
		socket.member_id = member.member_id;
		socket.username = member.username;
		socket.myroom = [];

		// Send server log
		sendlog(socket.username+' connect!', 'development');

		// Send client log
		socketlog({message:'Connecting...'}, 'development');
		socketlog({message:'Connect by '+socket.username},'development');

		// --------------------------------------------------------------------
		/**
		* Disconnect
		**/
		socket.on('disconnect', function () {
			
			if(socket.myroom.length>0)
			{
				sendlog(socket.myroom.length, 'development');

				for (var room_no in socket.myroom) {
					var room = socket.myroom[room_no];

					sendlog(room, 'development');
					// Calculate time
					if (room.connect_time)
					{

						var disconnect_time = moment(new Date()).utc();

						// Save to database
						models.project
							.findOne({_id:room.name})
							.select('associate')
							.exec(function(err, result){

								associate = _.filter(result.associate, function(associate){ return associate.member.toString() == socket.member_id; });
								sendlog('Associate '+associate, 'development');

								if (associate[0])
								{
									associate[0].time_spent = moment(associate[0].time_spent) + moment(disconnect_time-room.connect_time);
								}
								
								sendlog(associate, 'development');
								result.save(function (err) {
									if (err) return handleError(err);
									sendlog('Save time spent... in main', 'development');
								});
							});
					}
				}
				
			}
			
			sendlog(socket.username+' disconnect!');
		});

		// --------------------------------------------------------------------
		/**
		* Room
		**/

		// Connect to project room
		socket.on('connect_project', function (data) {
			// Join room
			socket.join(data.pkey);

			// Check room
			if (!chat_room[data.pkey])
			{
				chat_room[data.pkey] = [];
			}

			chat_room[data.pkey].push(socket.username);

			// Set room to socket
			var thisroom = _.findWhere(socket.myroom, {name:data.pkey});
			if (!thisroom)
			{
				socket.myroom.push({name:data.pkey, connect_time:new Date()});
			}

			// Send client log
			socketlog({message:'Join room: '+data.pkey}, 'development');
			sendlog(socket.username+' join '+socket.myroom, 'development');
		});

		// Disconnect to project room
		socket.on('disconnect_project', function (data) {
			// Leave room
			socket.leave(data.pkey);

			// Set room to socket
			var thisroom = _.findWhere(socket.myroom, {name:data.pkey}) ;
			if (thisroom)
			{
				// Calculate time
				if (thisroom.connect_time)
				{
					var disconnect_time = moment(new Date()).utc();

					// Save to database
					models.project
						.findOne({_id:data.pkey})
						.select('associate')
						.exec(function(err, result){

							associate = _.filter(result.associate, function(associate){ return associate.member.toString() == socket.member_id; });
                            sendlog('Associate '+associate, 'development');

							if (associate[0])
							{
								associate[0].time_spent = moment(associate[0].time_spent) + moment(disconnect_time-thisroom.connect_time);
							}

                            sendlog('Associate '+associate, 'development');
							result.save(function (err) {
								if (err) return handleError(err);
								sendlog('Save time spent... in main', 'development');
							});
						});
				}
			}

			// splice
			if(chat_room[data.pkey])
			{
				chat_room[data.pkey].splice(chat_room[data.pkey].indexOf(socket.username), 1);

				if(chat_room[data.pkey].length==0)
				{
					delete chat_room[data.pkey];
				}
			}

			// Check and remove room to socket
			var thisroom = _.findWhere(socket.myroom, {name:data.pkey}) ;
			if (_.indexOf(socket.myroom, thisroom) != -1)
			{
				socket.myroom.splice(socket.myroom.indexOf(thisroom),1);
			}

			// Send client log
			sendlog(socket.username+' leave room '+socket.myroom, 'development');
			socketlog({message:'Leave room: '+data.pkey}, 'development');
		});

		// --------------------------------------------------------------------
		/**
		* Chat
		**/ 

		// Chat server recieve
		socket.on('chat_send', function (data) {
			sendlog(socket.username+' chat in '+socket.myroom, 'development');

			if (data.message && data.pkey)
			{

				var moment = require('moment');

				var chat = new models.chat();
			    chat.message = data.message;
			    chat.created = moment(new Date()).utc();
			    chat.member = member.member_id;
			    chat.project = data.pkey;

			    chat.save(function (err, chat) {
			    	if (err)
			    	{
			    	}
			    	else
			    	{
			    		models.chat
							.findOne({_id:chat._id})
							.populate('member', 'firstname lastname username')
							.select('message created member')
							.exec(function(err, result){
								socket.broadcast.to(data.pkey).emit('chat_receive', result);
								socket.emit('chat_receive', result);
							});
			    	}
			        
			    });
			}
		});

		// --------------------------------------------------------------------
		/**
		* Activity
		**/ 

		// Chat server recieve
		socket.on('activity_send', function (data) {
			sendlog(socket.username+' make an activity in '+socket.myroom, 'development');
            console.log("++++++++++++++++++++++++++++++");
            console.log(data);
			if (data.message && data.pkey)
			{

				var moment = require('moment');

				var activity = new models.activity();
				activity.module = data.module;
			    activity.message = data.message;
			    activity.created = moment(new Date()).utc();
			    activity.member = member.member_id;
			    activity.project = data.pkey;

			    activity.save(function (err, activity) {
			    	if (err)
			    	{
			    	}
			    	else
			    	{
			    		models.activity
							.findOne({_id:activity._id})
							.populate('member', 'firstname lastname username')
							.populate('project', 'name status created updated associate')
							.select('message created member project module')
							.exec(function(err, result){
                                console.log("eieieieieei========");
                                console.log(data);
								socket.broadcast.to(data.pkey).emit('activity_receive', result);

								socket.emit('activity_receive',result);
							});
			    	}
			        
			    });
			}
		});

	});

    var Project = models.project;
    var Activity = models.activity;
    var Transition = models.transition;
    var ProjectTransitionCount = models.project_transition_count;
    var subscribe_activity = function(next, limit) {
        var filter = {};
        Activity.find(filter).sort({$natural: -1}).limit(limit || 10).exec(function (err, latest) {
            if (latest && latest.length >= 1) {
                latest = latest[latest.length - 1];
                filter._id = { $gt: latest._id }
            }
            var activity_stream = Activity.find(filter)
                .populate('member', 'firstname lastname')
                .populate('project', 'name type')
                //.tailable()
                .stream();
            // call the callback
            activity_stream.on('data', next);
        });
    };

    var find_prev_doc = function(new_document, callback){
        var filter = {};
        filter['member'] = new_document.member;
        filter['project'] = {_id: new_document.project._id};
        filter['_id'] = {$lt : new_document._id};
        Activity.find(filter)
            .sort({$natural: -1})
            .limit(1)
            .populate('member', 'firstname lastname username')
            .populate('project', 'name')
            .exec(function(err, last_document){
                if(last_document.length){
                    last_document = last_document[0];
                    last_document.project.type = new_document.project.type;
                    if(callback)return callback(last_document);
                }
            })
    };
    var save_transition = function(last_doc, current_doc, callback){
        var moment = require('moment');
        var transition = new Transition();
        transition.member = current_doc.member;
        transition.project = current_doc.project;
        transition.from = last_doc.module;
        transition.to = current_doc.module;
        transition.type = last_doc.project.type;
        transition.created = moment(new Date()).utc();
        transition.save(function (err, transition) {
            if (!err){
                if(callback) return callback(transition);
            }
        });
    };

    var update_server_transition_count = function(transition, callback){
        var filter = {project: transition.project, from: transition.from, to: transition.to};
        ProjectTransitionCount
            .findOne(filter, function(err, obj){
                if(!obj){
                    ProjectTransitionCount.create(filter, function(err, obj){
                        update_server_transition_value(obj);
                    });
                }else{
                    update_server_transition_value(obj);
                }
            });

        var update_server_transition_value = function(obj){
            obj.count += 1;
            obj.save(function(err){
                if (err) return handleError(err);
                if (callback) return callback(obj);
            });
        }
    };

    var push_log = function(new_document){
        log_server_nsp.to('log_all').emit('log:streaming', new_document);
        log_server_nsp.to('log_project_'+new_document.project._id).emit('log:streaming', new_document);
    };

    var push_transition = function(new_transition){
        log_server_nsp.to('log_all').emit('new:transition', new_transition);
        log_server_nsp.to('log_project_'+new_transition.project._id).emit('new:transition', new_transition);
    };

    var push_project_transition = function(project_transition){
        log_server_nsp.to('log_all').emit('update:project_transition',
            [project_transition.type, project_transition.from+'->'+project_transition.to, project_transition.count]
        );
        log_server_nsp.to('log_project_'+project_transition.project).emit('update:project_transition',
            [project_transition.type, project_transition.from+'->'+project_transition.to, project_transition.count]
        );
    };

    var push_refresh_project_transition_percent = function(project_transition){
        log_server_nsp.to('log_all').emit('refresh:project_transition_percent');
        log_server_nsp.to('log_project_'+project_transition.project).emit('refresh:project_transition_percent');
    };

    // Monitor new activity and process it!
    subscribe_activity(function(new_document) {
        push_log(new_document);
        find_prev_doc(new_document, function(prev_doc){
            save_transition(prev_doc, new_document, function(transition){
                Transition
                    .findOne({_id: transition._id})
                    .populate('member', 'firstname lastname username')
                    .populate('project', 'name')
                    .exec(function(err, new_transition){
                        push_transition(new_transition);
                    });
                update_server_transition_count(transition, function(project_transition){
                    push_project_transition(project_transition);
                    push_refresh_project_transition_percent(project_transition);
                });
            });
        })
    }, 1);

    // Socketserver
    var log_server_nsp = io.of('/logserver')
        .on('connection', function (socket) {
            socket.project_id = null;
            socket.on('subscribe', function(data){
                if(data.project_id){
                    socket.project_id = data.project_id;
                    socket.join('log_project_'+socket.project_id);
                    socket.emit('subscribeSuccess');
                }else{
                    socket.join('log_all');
                    socket.emit('subscribeSuccess');
                }
            });

            socket.on('get:project_list', function(){
                var filter = {};
                if(socket.project_id){
                    filter._id = socket.project_id;
                }
                Project
                    .find(filter)
                    .sort("name")
                    .exec(function(err, projects){
                        socket.emit('get:project_list', projects)
                    });
            });

            socket.on('update:project_type_setting', function(change){
                _.each(change, function(new_val){
                    var filter = {_id: new_val[0]};
                    Project
                        .findOne(filter, function(err, project){
                            var new_type = new_val[1];
                            if(project && project.type != new_val[1]){
                                project.type = new_type;
                                project.save(function(err){
                                    if (err) return handleError(err);
                                    Transition.find({project: project._id}).exec(function(err, results){
                                        if (err) return handleError(err);
                                        _.each(results, function(transition){
                                            transition.type = new_type;
                                            transition.save();
                                        });
                                        log_server_nsp.emit('refresh:transition');
                                    });
                                    ProjectTransitionCount.find({project: project._id}).exec(function(err, results){
                                        if (err) return handleError(err);
                                        _.each(results, function(transition){
                                            transition.type = new_type;
                                            transition.save();
                                        });
                                        log_server_nsp.emit('refresh:project_transition');
                                        log_server_nsp.emit('refresh:project_transition_percent');
                                    });
                                    socket.broadcast.emit('update:project_type_setting', project);
                                });
                            }
                        });
                });
            });

            socket.on('update:project_snapboard_setting', function(setting){
                var filter = {_id: setting['project_id']};
                Project
                    .findOne(filter, function(err, project){
                        if(project && project.enable_snapboard != setting['enable']){
                            project.enable_snapboard = setting['enable'];
                            project.save();
                        }
                    });
            });

            var groups = [
                'SYNC',
                'ASYNC',
                'INDIVIDUAL'
            ];
            //Generate possible transition keys.
            var keys = [
                'project',
                'snapshot',
                'seen',
                'query',
                'chat',
                'relevant',
                'comment',
            ];
            socket.on('get:project_transition', function(){
                var transition_summary = {};

                _.each(groups, function(group){
                    if(!transition_summary[group]){
                        transition_summary[group] = {};
                    }
                    _.each(keys, function(key1){
                        _.each(keys, function(key2){

                                transition_summary[group][key1+'->'+key2] = 0;

                        });
                    });
                });

                var filter = [];
                if(socket.project_id){
                    filter.push({
                        $match: {
                            project: mongoose.Types.ObjectId(socket.project_id)
                        }
                    });
                    filter.push({
                        $group: {
                            _id : { from: '$from', to: '$to', type: '$type'},
                            count : { $sum : "$count" }
                        }
                    });
                }else{
                    filter.push({"$group" : {
                        _id : { from: '$from', to: '$to', type: '$type'},
                        count : { $sum : "$count" }
                    }});
                }
                ProjectTransitionCount.aggregate(filter, function (err, results) {
                    _.each(results, function(result){
                        var _id = result._id;
                        if(_id.from != _id.to){
                            transition_summary[_id.type][_id.from+'->'+_id.to] = result.count;
                        }
                    });
                    send_result();
                });

                var send_result = function(){
                    socket.emit('get:project_transition', transition_summary);
                };
            });

            socket.on('get:project_transition_percent', function(){
                var transition_types_count = {};
                var transition_percents = {};

                // Find summation count each type.
                _.each(groups, function(group){
                    if(!transition_percents[group]){
                        transition_percents[group] = {};
                    }
                    _.each(keys, function(key1){
                        _.each(keys, function(key2){
                            transition_percents[group][key1+'->'+key2] = 0;
                        });
                    });
                });

                var filter = [];
                if(socket.project_id){
                    filter.push({
                        $match: {
                            project: mongoose.Types.ObjectId(socket.project_id)
                        }
                    });
                    filter.push({
                        $group: {
                            _id : { from: '$from', to: '$to', type: '$type'},
                            count : { $sum : "$count" }
                        }
                    });
                }else{
                    filter.push({"$group" : {
                        _id : { from: '$from', to: '$to', type: '$type'},
                        count : { $sum : "$count" }
                    }});
                }

                //Calc
                ProjectTransitionCount.aggregate(filter, function (err, results) {
                    _.each(results, function(result){
                        var _id = result._id;
                        if(_id.from != _id.to && _id.from != "project" && _id.to != "project"){
                            if(!transition_types_count[_id.type]){
                                transition_types_count[_id.type] = 0;
                            }
                            transition_types_count[_id.type] += result.count;
                        }
                    });
//                    console.log(transition_types_count);
                    _.each(results, function(result){
                        var _id = result._id;
                        if(_id.from != _id.to && _id.from != "project" && _id.to != "project"){
                            transition_percents[_id.type][_id.from+'->'+_id.to] = parseFloat(result.count) / transition_types_count[_id.type] * 100;
                        }
                    });
                    send_result();
                });

                var send_result = function(){
                    socket.emit('get:project_transition_percent', transition_percents);
                };
            });

        });

	return io;
};

