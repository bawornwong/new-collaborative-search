
$(document).ready(function() {

	//////////////////	
	// Form validations
	//////////////////
	$('span.help-inline:empty').each(function() {
		$(this).hide();
	});
	
	$('span.help-inline:not(:empty)').each(function() {
		$(this).closest('.control-group').addClass('error');
	});

	//////////////////
	// Diable form on submit
	//////////////////	
	$('form').submit(function(){
		$('.hide_on_submit').hide();
		$('form:not(.submit-not-disable), input:not(.submit-not-disable), select:not(.submit-not-disable), textarea:not(.submit-not-disable)')
			.removeAttr('disabled','disabled')
			.attr('readonly','readonly');
		//$('select').combobox('disabled');
		$('input[type="submit"], button[type="submit"]')
			.attr('disabled','disabled')
			.html('Please wait...');
	});

	//////////////////
	// Auto change to datetime picker
	//////////////////	
	$('.datetimepicker').datetimepicker({
		dateFormat: 'yy-mm-dd',
		timeFormat: 'hh:mm:ss',
		showSecond: true,
		minDate: "now-1"
	});

});

///////////////////////////////////////////////////////////////////////////////////////////////////////////////// My Plugin
(function( $ ) {

	// --------------------------------------------------------------------
	//////////////////	
	// Function: random tring
	//////////////////
	$.random_string = function(digit) {
		var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
		var string_length = digit;
		var randomstring = '';
		for (var i=0; i<string_length; i++) {
			var rnum = Math.floor(Math.random() * chars.length);
			randomstring += chars.substring(rnum,rnum+1);
		}

		return randomstring;
	}
	
	// --------------------------------------------------------------------
	//////////////////	
	// Function: jScroll extend
	//////////////////
	// the extension functions and options
	$.jsp_extention = function($el){
		return {
			
			extPluginOpts	: {
				// speed for the fadeOut animation
				mouseLeaveFadeSpeed	: 500,
				// scrollbar fades out after hovertimeout_t milliseconds
				hovertimeout_t		: 5000,
				// if set to false, the scrollbar will be shown on mouseenter and hidden on mouseleave
				// if set to true, the same will happen, but the scrollbar will be also hidden on mouseenter after "hovertimeout_t" ms
				// also, it will be shown when we start to scroll and hidden when stopping
				useTimeout			: true,
				// the extension only applies for devices with width > deviceWidth
				deviceWidth			: 980
			},
			hovertimeout	: null, // timeout to hide the scrollbar
			isScrollbarHover: false,// true if the mouse is over the scrollbar
			elementtimeout	: null,	// avoids showing the scrollbar when moving from inside the element to outside, passing over the scrollbar
			isScrolling		: false,// true if scrolling
			addHoverFunc	: function() {
				
				// run only if the window has a width bigger than deviceWidth
				if( $(window).width() <= this.extPluginOpts.deviceWidth ) return false;
				
				// Init instance
				var instance		= this;
				
				// functions to show / hide the scrollbar
				$.fn.jspmouseenter 	= $.fn.fadeIn;
				$.fn.jspmouseleave 	= $.fn.fadeOut;
				
				// hide the jScrollPane vertical bar
				var $vBar = this.getContentPane().siblings('.jspVerticalBar').css('opacity',0);
				
				/*
				 * mouseenter / mouseleave events on the main element
				 * also scrollstart / scrollstop - @James Padolsey : http://james.padolsey.com/javascript/special-scroll-events-for-jquery/
				 */
				$el.bind('mouseenter.jsp',function() {
					if(instance.getIsScrollableV())
					{
						// show the scrollbar
						$vBar.stop( true, true ).jspmouseenter();
						
						if( !instance.extPluginOpts.useTimeout ) return false;
						
						// hide the scrollbar after hovertimeout_t ms
						clearTimeout( instance.hovertimeout );
						$vBar.css('opacity',1);

						instance.hovertimeout 	= setTimeout(function() {
							// if scrolling at the moment don't hide it
							if( !instance.isScrolling )
							{
								$vBar.stop( true, true ).jspmouseleave( instance.extPluginOpts.mouseLeaveFadeSpeed || 0 ,function(){
									$vBar.css('opacity',0).show();
								});
							}
								
						}, instance.extPluginOpts.hovertimeout_t );
					
					}
				}).bind('mouseleave.jsp',function() {
					// hide the scrollbar
					if( !instance.extPluginOpts.useTimeout )
					{
						$vBar.stop( true, true ).jspmouseleave( instance.extPluginOpts.mouseLeaveFadeSpeed || 0 ,function(){
									$vBar.css('opacity',0).show();
								});
					}	
					else
					{
						clearTimeout( instance.elementtimeout );
						if( !instance.isScrolling )
						{
							$vBar.stop( true, true ).jspmouseleave( instance.extPluginOpts.mouseLeaveFadeSpeed || 0 ,function(){
									$vBar.css('opacity',0).show();
								});
						}
					}
					
				});
				
				if( this.extPluginOpts.useTimeout ) {
					
					$el.bind('scrollstart.jsp', function() {
						if(instance.getIsScrollableV())
						{
							// when scrolling show the scrollbar
							clearTimeout( instance.hovertimeout );
							$vBar.css('opacity',1);

							instance.isScrolling	= true;
							$vBar.stop( true, true ).jspmouseenter();
						}
					
					}).bind('scrollstop.jsp', function() {
						if(instance.getIsScrollableV())
						{
							// when stop scrolling hide the scrollbar (if not hovering it at the moment)
							clearTimeout( instance.hovertimeout );
							instance.isScrolling	= false;
							instance.hovertimeout 	= setTimeout(function() {
								if( !instance.isScrollbarHover )
										$vBar.stop( true, true ).jspmouseleave( instance.extPluginOpts.mouseLeaveFadeSpeed || 0 , function(){
										$vBar.css('opacity',0).show();
									});
								}, instance.extPluginOpts.hovertimeout_t );
						}
					
					});
				
					// wrap the scrollbar
					// we need this to be able to add the mouseenter / mouseleave events to the scrollbar
					var $vBarWrapper	= $('<div/>').css({
						position	: 'absolute',
						left		: $vBar.css('left'),
						top			: $vBar.css('top'),
						right		: $vBar.css('right'),
						bottom		: $vBar.css('bottom'),
						width		: $vBar.width(),
						height		: $vBar.height()
					})
					$vBar.bind('mouseenter.jsp',function() {
						if(instance.getIsScrollableV())
						{
							
							clearTimeout( instance.hovertimeout );
							clearTimeout( instance.elementtimeout );
							$vBar.css('opacity',1);

							instance.isScrollbarHover	= true;
							
								// show the scrollbar after 100 ms.
								// avoids showing the scrollbar when moving from inside the element to outside, passing over the scrollbar								
							instance.elementtimeout	= setTimeout(function() {
								$vBar.stop( true, true ).jspmouseenter();
							}, 100 );	
						}

					}).bind('mouseleave.jsp',function() {
						if(instance.getIsScrollableV())
						{
							// hide the scrollbar after hovertimeout_t
							clearTimeout( instance.hovertimeout );
							instance.isScrollbarHover	= false;
							instance.hovertimeout = setTimeout(function() {
								// if scrolling at the moment don't hide it
							if( !instance.isScrolling )
									$vBar.stop( true, true ).jspmouseleave( instance.extPluginOpts.mouseLeaveFadeSpeed || 0 ,function(){
									$vBar.css('opacity',0).show();
								});
							}, instance.extPluginOpts.hovertimeout_t );
						}
					});
					
					//$vBar.wrap( $vBarWrapper );
				
				}
			
			}
			
		};
	}

})( jQuery );

///////////////////////////////////////////////////////////////////////////////////////////////////////////////// Slide to current height
(function( $ ) {
	var methods  = {
		init : function(options) {
        },
        expand : function(){
        	$(this).animate({
        			height: $(this)[0].scrollHeight
        		}, "fast");
        },
        to : function(height){

        	$(this).animate({
        			height: height
        		}, "fast");
        },
        zero : function(){

        	$(this).animate({
        			height: 0
        		}, "fast");
        }
	};

    $.fn.slideToHeight = function( method ) {
    
        if ( methods[method] ) {
            return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' does not exist on jQuery.notiBox' );
        }    
  
    };

})( jQuery );

