
// ============================================================================================================================================
// Global value
// ============================================================================================================================================
var socket = null;
var keyword = '';
var found_result = false;
var num_result = 0;
var userlist = {
    invite: [],
    project: [],
    project_id: null,
    project_info: null
};
var search_result_store = null;
var keyword_store = null;
var activity_result_store = null;
var user_id = null;
var loadqueue = {
    'page':['pageload'],
    'projectlist':[]
};
var project_status = ['current','completed','invite'];
var project_list = {
    'current_tab':null,
    'current_project':null,
    'invite':{
        'description':'List of project invited.',
        'title':'Project invited',
        'not_found':'Result not found.',
        'page':0
    },
    'current':{
        'description':'List of current projects.',
        'title':'Current project',
        'not_found':'Result not found.',
        'page':0
    },
    'completed':{
        'description':'List of completed projects.',
        'title':'Completed project',
        'not_found':'Result not found.',
        'page':0
    }
};
var result_tab = {
    current: 'all',
    current_data: {},
    tab: []
};

var count_time_store = {
    'start':null,
    'current':null,
    'time_in':null
};

var current_descritipn = '';

// For Snapshot
var jcrop_api;

// J Scroll Pane
var jsp = {
    'project_list':{
        'content':null,
        'api':null,
        'reinitialise':null
    },
    'invite_userlist':{
        'content':null,
        'api':null,
        'reinitialise':null
    },
    'project_userlist':{
        'content':null,
        'api':null,
        'reinitialise':null
    }
};

var templates = {};

// ============================================================================================================================================
// Run Function
// ============================================================================================================================================
$(document).ready(function() {

    // Noti Setting
    $.noty.defaults = {
        layout: 'bottomLeft',
        theme: 'defaultTheme',
        type: 'alert',
        text: '', // can be html or string
        dismissQueue: true, // If you want to use queue feature set this true
        template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
        animation: {
            open: {height: 'toggle'},
            close: {height: 'toggle'},
            easing: 'swing',
            speed: 500 // opening & closing animation speed
        },
        timeout: 15000, // delay for closing event. Set false for sticky notifications
        force: false, // adds notification to the beginning of queue when set to true
        modal: false,
        maxVisible: 10, // you can set max visible notification for dismissQueue true option,
        killer: false, // for close all notifications before show
        closeWith: ['click'], // ['click', 'button', 'hover']
        callback: {
            onShow: function() {},
            afterShow: function() {},
            onClose: function() {},
            afterClose: function() {}
        },
        buttons: false // an array of buttons
    };

    // ============================================================================================================================================
    //////////////////
    // Init Run
    //////////////////
    // ============================================================================================================================================
    $.run_init = function(){
        // ---------------------------------
        // Initial
        // Init project button
        $.each(project_status, function(key, value){
            $('#'+value+'-btn')
                .click(function(){
                    if ( project_list.current_tab != value ){
                        $('.project-list-item').remove();
                        $('#project-list-item-empty, #project-list-seemore').hide();
                        $.load_project_list(value);
                    }
                })
                .tooltip({
                    title: project_list[value]['description'],
                    placement:'bottom'
                });
        });


        // ---------------------------------
        // Run
        if( $.PKEY == null )
        {
            $.load_dashboard();
        }
        else
        {
            $.load_project($.PKEY);
        }

        // ---------------------------------
        // Hot key
        $(document).bind('keydown', 'Alt+n',$.newproject_window);

        // ---------------------------------
        // Hide Load
        $.page_loader('hide');
    };

    // ============================================================================================================================================
    //////////////////
    // Load JS Part
    //////////////////
    // ============================================================================================================================================
    $.load_js = function() {
        $.when(
            $.getScript("/static/js/dashboard/activity.js"),
            $.getScript("/static/js/dashboard/button_control.js"),
            $.getScript("/static/js/dashboard/chat.js"),
            $.getScript("/static/js/dashboard/dashboard.js"),
            $.getScript("/static/js/dashboard/jscrollpane.js"),
            $.getScript("/static/js/dashboard/keyword.js"),
            $.getScript("/static/js/dashboard/project.js"),
            $.getScript("/static/js/dashboard/relevant.js"),
            $.getScript("/static/js/dashboard/result.js"),
            $.getScript("/static/js/dashboard/search.js"),
            $.getScript("/static/js/dashboard/seen.js"),
            $.getScript("/static/js/dashboard/snapshot.js"),
            $.getScript("/static/js/dashboard/url_control.js"),
            $.getScript("/static/js/dashboard/user_management.js"),
            $.getScript("/static/js/dashboard/utilities.js"),
            $.Deferred(function (deferred) {
                $(deferred.resolve);
            })
        ).done(function () {
            // Next To Run Init
            $.run_init();
        });
    };

    // ============================================================================================================================================
    //////////////////
    // Get Template First
    //////////////////
    // ============================================================================================================================================
    $.get('/static/js/dashboard/templates.tpl', function(doc) {
        // Template Setting For use {{ var }}
        _.templateSettings = {
            evaluate    : /\{%([\s\S]+?)%\}/g,
            interpolate: /\{\{(.+?)\}\}/g,
            escape      : /<%-([\s\S]+?)%>/g
        };

        // Filter Template Tag
        var include_template = $(doc).filter('template');

        // Store Template For Use
        include_template.each(function() {
            templates[this.id] = _.template($(this).html());
        });

        // Next to Get JS
        $.load_js();
    });
});

