var application = angular.module('ocrApp', ['ui.bootstrap', 'ui.slimscroll', 'highcharts-ng']);

application.controller("AllOcrController", function($scope, $window){
    $scope.project = false;
    $scope.project_id = $window.project_id;
    $scope.project_type = $window.project_type;
});
