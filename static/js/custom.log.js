var socket = null;

var application = angular.module('logApp', ['ui.bootstrap', 'ui.slimscroll', 'highcharts-ng']);

application.factory('socket', function ($rootScope) {
    var socket = io.connect('/logserver');
    return {
        on: function (eventName, callback) {
            socket.on(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply(socket, args);
                });
            });
        },
        emit: function (eventName, data, callback) {
            socket.emit(eventName, data, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    if (callback) {
                        callback.apply(socket, args);
                    }
                });
            })
        }
    };
});

application
    .controller("AllLogController", function($scope, $window, socket){
        $scope.project = false;
        $scope.project_id = $window.project_id;
        $scope.project_type = $window.project_type;

        console.log($scope);
        console.log($scope.project_id);
        console.log($scope.project_type);
        console.log($window);
        console.log($window.project_id);
        console.log($window.project_type);

        socket.on('connect', function (data) {
            if($scope.project_id){
                socket.emit('subscribe', {project_id: $scope.project_id});
                $scope.project = true;
            }else{
                socket.emit('subscribe', {project_id: null});
            }
        });

        socket.on('subscribeSuccess', function(){
            // Call if display all project.
            socket.emit('get:project_list');
            socket.emit('get:project_transition');
            socket.emit('get:project_transition_percent');
            socket.on('refresh:project_transition', function(){
                socket.emit('get:project_transition');
            });
            socket.on('refresh:project_transition_percent', function(){
                socket.emit('get:project_transition_percent');
            });
        });

        $scope.logs = {all: []};
        socket.on('log:streaming',function(log){
            $scope.logs.all.splice(0, 0, log);
            if($scope.logs[log.module]){
                $scope.logs[log.module].splice(0, 0, log);
            }else{
                $scope.logs[log.module] = [log];
            }
        });

        $scope.transition_streaming = [];
        socket.on('new:transition', function(transition){
            $scope.transition_streaming.splice(0, 0, transition);
        });

        $scope.transition_summary = {};
        $scope.transition_summary_keys = [];
        $scope.$watch('transition_summary', function(types){
            if($scope.project_id){
                var key = $scope.project_type;
                var type = types[key];
                var keys = [];
                _.each(type, function( val, key ) {
                    var split_key = key.split('->');
                    if(split_key[0] != "project" &&  split_key[1] != "project"){
                        keys.push([key, val]);
                    }
                });
                keys = _.sortBy(keys, function(o) { return o[0]; });

                //Render to graph
                if($scope.project_type == 'SYNC'){
                    $scope.chartConfig.series[0].data = keys;
                    $scope.chartConfig.series[1].data = [];
                    $scope.chartConfig.series[2].data = [];
                }else if($scope.project_type == 'ASYNC'){
                    $scope.chartConfig.series[0].data = [];
                    $scope.chartConfig.series[1].data = keys;
                    $scope.chartConfig.series[2].data = [];
                }else{
                    $scope.chartConfig.series[0].data = [];
                    $scope.chartConfig.series[1].data = [];
                    $scope.chartConfig.series[2].data = keys;
                }

                //Group for display in table.
                keys = _.groupBy(keys, function(o) { return o[0].split('->')[0] });
                var results = [];
                _.each(keys, function( val, key ) {
                    results.push([key, val]);
                });
                $scope.transition_summary_keys =  results;
            }else{
                _.each(types, function(type, key){
                    var keys = [];
                    _.each(type, function( val, key ) {
                        var split_key = key.split('->');
                        if(split_key[0] != "project" &&  split_key[1] != "project"){
                            keys.push([key, val]);
                        }
                    });
                    keys = _.sortBy(keys, function(o) { return o[0]; });

                    if(key == 'SYNC'){
                        $scope.chartConfig.series[0].data = keys;
                    }else if(key == 'ASYNC'){
                        $scope.chartConfig.series[1].data = keys;
                    }else{
                        $scope.chartConfig.series[2].data = keys;
                    }

                    //Group for display in table.
                    keys = _.groupBy(keys, function(o) { return o[0].split('->')[0] });
                    var results = [];
                    _.each(keys, function( val, key ) {
                        results.push([key, val]);
                    });

                    //Render to graph
                    $scope.transition_summary_keys[key] =  results;
                });
            }
        }, true);

        //Project List
        socket.on('get:project_list', function(projects){
            $scope.projects = projects;
        });

        // Setting
        $scope.save_project_type_setting = function(){
            var change = [];
            _.each($scope.projects, function( obj ) {
                change.push([
                    obj._id,
                    obj.type
                ]);
                if($scope.project_id && $scope.project_id == obj._id){
                    $scope.project_type = obj.type;
                }
            });
            socket.emit('update:project_type_setting', change);
        };
        socket.on('update:project_type_setting', function(project){
            _.each($scope.projects, function( obj ) {
                if(obj._id == project._id){
                    obj.type = project.type;
                }
                if($scope.project_id && $scope.project_id == obj._id){
                    $scope.project_type = obj.type;
                }
            });
        });

        //Transition
        socket.on('get:project_transition', function(transition_summary){
            $scope.transition_summary = transition_summary;
        });
        socket.on('update:project_transition', function(transition_summary){
            $scope.transition_summary[transition_summary[0]][transition_summary[1]] = transition_summary[2];
        });

        $scope.chartConfig = {
            options: {
                chart: {
                    type: 'line',
                    zoomType: 'x'
                }
            },
            series: [{
                name: 'SYNC',
                data: [],
                dataLabels: {
                    enabled: true,
                    formatter:function(){
                        if(this.y)
                            return this.y;
                        return ''
                    }
                }
            },{
                name: 'ASYNC',
                data: [],
                dataLabels: {
                    enabled: true,
                    formatter:function(){
                        if(this.y)
                            return this.y;
                        return ''
                    }
                }
            },{
                name: 'INDIVIDUAL',
                data: [],
                dataLabels: {
                    enabled: true,
                    formatter:function(){
                        if(this.y)
                            return this.y;
                        return ''
                    }
                }
            }],
            title: {
                text: 'Summary'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                title: {
                    text: 'Count'
                }
            },
            loading: false
        };

        //Transition Percents
        $scope.transition_percents = {};
        $scope.transition_percents_keys = [];
        socket.on('get:project_transition_percent', function(transition_percents){
            $scope.transition_percents = transition_percents;
        });
        $scope.$watch('transition_percents', function(types){
            if($scope.project_id){
                var key = $scope.project_type;
                var type = types[key];
                var keys = [];
                _.each(type, function( val, key ) {
                    var split_key = key.split('->');
                    if(split_key[0] != "project" &&  split_key[1] != "project"){
                        keys.push([key, val]);
                    }
                });
                keys = _.sortBy(keys, function(o) { return o[0]; });

                //Render to graph
                if($scope.project_type == 'SYNC'){
                    $scope.chartConfigPercent.series[0].data = keys;
                    $scope.chartConfigPercent.series[1].data = [];
                    $scope.chartConfigPercent.series[2].data = [];
                }else if($scope.project_type == 'ASYNC'){
                    $scope.chartConfigPercent.series[0].data = [];
                    $scope.chartConfigPercent.series[1].data = keys;
                    $scope.chartConfigPercent.series[2].data = [];
                }else{
                    $scope.chartConfigPercent.series[0].data = [];
                    $scope.chartConfigPercent.series[1].data = [];
                    $scope.chartConfigPercent.series[2].data = keys;
                }

                //Group for display in table.
                keys = _.groupBy(keys, function(o) { return o[0].split('->')[0] });
                var results = [];
                _.each(keys, function( val, key ) {
                    results.push([key, val]);
                });
                $scope.transition_percents_keys =  results;
            }else{
                _.each(types, function(type, key){
                    var keys = [];
                    _.each(type, function( val, key ) {
                        var split_key = key.split('->');
                        if(split_key[0] != "project" &&  split_key[1] != "project"){
                            keys.push([key, val]);
                        }
                    });
                    keys = _.sortBy(keys, function(o) { return o[0]; });

                    if(key == 'SYNC'){
                        $scope.chartConfigPercent.series[0].data = keys;
                    }else if(key == 'ASYNC'){
                        $scope.chartConfigPercent.series[1].data = keys;
                    }else{
                        $scope.chartConfigPercent.series[2].data = keys;
                    }

                    //Group for display in table.
                    keys = _.groupBy(keys, function(o) { return o[0].split('->')[0] });
                    var results = [];
                    _.each(keys, function( val, key ) {
                        results.push([key, val]);
                    });

                    //Render to graph
                    $scope.transition_percents_keys[key] =  results;
                });
            }
        }, true);

        $scope.chartConfigPercent = {
            options: {
                chart: {
                    type: 'line',
                    zoomType: 'x'
                }
            },
            series: [{
                name: 'SYNC',
                data: [],
                dataLabels: {
                    enabled: true,
                    formatter:function(){
                        if(this.y)
                            return ''+Highcharts.numberFormat(this.y, 2) + '%';
                        return ''
                    }
                }
            },{
                name: 'ASYNC',
                data: [],
                dataLabels: {
                    enabled: true,
                    formatter:function(){
                        if(this.y)
                            return ''+Highcharts.numberFormat(this.y, 2) + '%';
                        return ''
                    }
                }
            },{
                name: 'INDIVIDUAL',
                data: [],
                dataLabels: {
                    enabled: true,
                    formatter:function(){
                        if(this.y)
                            return ''+Highcharts.numberFormat(this.y, 2) + '%';
                        return ''
                    }
                }
            }],
            title: {
                text: 'Percents'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                title: {
                    text: 'Percents (%)'
                }
            },
            loading: false
        };

        $scope.enable_snapshot = function(project){
            socket.emit('update:project_snapboard_setting', {project_id: project._id, enable: true});
            project.enable_snapboard = true;
        };
        $scope.disable_snapshot = function(project){
            socket.emit('update:project_snapboard_setting', {project_id: project._id, enable: false});
            project.enable_snapboard = false;
        };

    });

