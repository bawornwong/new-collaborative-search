

$(document).ready(function() {

	//////////////////	
	// Form validations
	//////////////////
	$('.alert:empty').hide();
	
	$('.alert:not(:empty)').show();

	$('#username').focus();
	$('#loginbox, #regisbox').css('margin-top', ($(window).height()/2 - $('#loginbox, #regisbox').outerHeight()/2) - 35 );

});

$(window).resize(function(){
	$('#loginbox, #regisbox').css('margin-top', ($(window).height()/2 - $('#loginbox, #regisbox').outerHeight()/2) - 35 );
});