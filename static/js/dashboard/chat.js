// --------------------------------------------------------------------
//////////////////
// Function: Update chat
//////////////////
$.update_chat = function(chat_updated){
    // Update message
    var data = 'pkey='+$.PKEY;
    var new_update = false;
    var nonscroll = false;

    // Check chat updatd
    if ( chat_updated == null )
    {
        data = 'pkey='+$.PKEY;
        new_update = true;
    }
    else
    {
        data = 'update='+chat_updated+'&pkey='+$.PKEY;
        nonscroll = true;
    }

    var update_message = $.ajax({
        type:"POST",
        url:"/chat/get",
        cache: false,
        data: data
    });

    update_message.done(function(msg){
        var receive = msg;

        // Hide chat loader
        $('#chat-loader')
            .animate(
            {'height': 0,
                'margin-bottom': 0},
            'fast',
            function(){
                $(this).hide();
            }
        );

        // Chat scroll event
        if(receive.next)
        {
            $('#chat-show').scroll(function () {
                if ( $(this).scrollTop()==0 )
                {
                    // Remove event scroll
                    $('#chat-show').unbind('scroll');

                    // Show chat loader
                    $('#chat-loader')
                        .show()
                        .animate(
                        {'height': 20,
                            'margin-bottom': 10}
                    );

                    // Get pre chat from server
                    $.update_chat(receive.last.created);
                }
            });
        }

        $.each(receive.results, function(key, value){
            $.insert_chat(value, true, nonscroll);
            $('#chat-empty').slideUp('fast');
        });
    });

    update_message.fail(function(msg){

    });

}

// --------------------------------------------------------------------
//////////////////
// Function: Insert chat
//////////////////
$.insert_chat = function(value, update, nonscroll){
    $('#chat-empty:visible').slideUp();
    // insert message
    var created = new moment(value.created);

    var insert_to = 'chat-show-'+created.format('YYYY-MM-DD');
    var insert = $('<div>')
        .attr('id', value._id+'-chat-message')
        .attr('class','chat-messages')
        .html( '<span class="chat-info"><span class="chat-info-username">'+value.member.firstname+' '+value.member.lastname+'</span><span class="chat-info-date">'+created.format('HH:mm:ss')+'</span></span>'+
            '<span class="chat-message">'+value.message+'</span>');

    // Check dategroup
    if( $('#chat-show > #'+insert_to).length == 0 ){
        var dategroup = $('<div>')
            .attr('id', insert_to)
            .attr('class','chat-message-dategroup')
            .html( '<div class="chat-message-dategroup-title label">'+created.format('YYYY-MM-DD')+'</div><div class="chat-message-dategroup-message"></div>');
        // Insert date group
        if( $('#chat-show > :Last-child').length == 0 )
        {
            $('#chat-show').append(dategroup);
        }
        else
        {
            if(update)
            {
                dategroup.insertAfter('#chat-show > #chat-loader');
            }
            else
            {
                dategroup.insertAfter('#chat-show > :Last-child');
            }
        }

    }

    // Insert to date group
    if( $('#chat-show > #'+insert_to+' > .chat-message-dategroup-message > :Last-child').length == 0 )
    {
        $('#chat-show > #'+insert_to+'> .chat-message-dategroup-message').append(insert);
    }
    else
    {
        if (update)
        {
            insert.insertBefore('#chat-show > #'+insert_to+' > .chat-message-dategroup-message > :First-child');
        }
        else
        {
            insert.insertAfter('#chat-show > #'+insert_to+' > .chat-message-dategroup-message > :Last-child');
        }
    }

    // If not scroll value
    if (!nonscroll)
    {
        $("#chat-show").scrollTop($("#chat-show")[0].scrollHeight);
    }
    else
    {
        $("#chat-show").scrollTop(40);
    }
}

// --------------------------------------------------------------------
//////////////////
// Chat
//////////////////
$('#chat_form')
    .submit(function(){

        // Send chat message
        socket.emit("chat_send", {pkey:$.PKEY,message: $('#chat-message').val()});
        socket.emit("activity_send", {pkey:$.PKEY,message:' chat: '+$('#chat-message').val(),module:'chat'});

        // Init next summission
        $('#chat-message').val('');
        return false;
    });