
// --------------------------------------------------------------------
//////////////////
// Function: show/hide snapboard panel
//////////////////
$.snapboard_panel = function(action){
    if( action == 'show' ){
        $('#snapboard-panel')
            .slideDown('slow');
        $('#snapboard-btn')
            .addClass('active');
        console.log(window.innerWidth,'snapshot_panel_active');

    }else{
        $('#snapboard-panel')
            .slideUp('slow');
        $('#snapboard-btn')
            .removeClass('active');
        console.log('snapshot_panel_not active');
    }
};
// --------------------------------------------------------------------



// --------------------------------------------------------------------
//////////////////
// Function: Snapshot Crop and Set to Result ����������� DB �ѧ�
//////////////////

$.snapshot_crop = function(data){
    $.page_loader('show');
    console.log(data.type)


    console.log("jameskung!!!!!!!!", data)

    if(data.type == "suggest"){
        data['file_name'] = data.src;
        data['search_by_keyword'] = result_tab.current_data.search_by_keyword;
        console.log("suggest: ", data)

        var snapshot = $.ajax({
            type: 'POST',
            url: '/snapshot/no_crop',
            cache: false,
            data: data
        });

    }else{
        data['file_name'] = $('#snapshot-image').attr('data-name');
        data['search_by_keyword'] = result_tab.current_data.search_by_keyword;
        console.log("crop: ", data)
        //_.find(search_result_store, function(search_result){ return search_result.url == result_tab.current; });

        /////////////////////// send data to crop img /////////////
        // Init AJAX
        var snapshot = $.ajax({
            type: 'POST',
            url: '/snapshot/crop',
            cache: false,
            data: data
        });
        /////////////////////// send data to crop img /////////////

    }

    snapshot.done(function(msg){

        $('#web-snapshot-container').modal('hide');
        //////// notification //////////
        noty({text: 'Snapshot for "'+result_tab.current+'" created. You can see it on the result list or snapboard.', type: 'success'});
        //////// Activity //////////
        socket.emit("activity_send", {pkey: $.PKEY, message:'snapshot:123456789'+result_tab.current, module:'snapshot'});

    });

    snapshot.fail(function(){
        noty({text: 'Can not create snapshot!!!', type: 'error'});
    });

    snapshot.always(function(msg){
        $.page_loader('hide');
    });
};

// ============================================================================================================================================
//////////////////
// Snapshot crop
//////////////////

var url_stack = {};
window.addEventListener('message', function(message) {
    var data = message.data;
    console.log(data);
    if(data.ID){
        if(!url_stack[data.ID]) url_stack[data.ID] = [];
        url_stack[data.ID].push(data.url);
        console.log(url_stack);
    }
});


var data_img_url = ""
$('#web-snapshot-btn').click(function(){
    if(!project_list.current_project.project.enable_snapboard){
        alert("Snapboard function is Disable");
        return;
    }

    var snapshot_image = $('#snapshot-image');

    var snapshot_done_btn = $('#web-snapshot-done-btn');
    var snapshot_container = $('#web-snapshot-container');

    if (!result_tab.current || result_tab.current == 'all') {
        noty({text: 'Can not create snapshot!!!', type: 'error'});
        return false;
    }

    snapshot_container
        .modal('show')
        .on('hidden', function () {
            jcrop_api = null;
            $(".jcrop-holder").remove();
            snapshot_image.replaceWith('<img id="snapshot-image" src="/static/img/loader-2.gif" >');
            $('#suggest-image1').replaceWith('<img id="suggest-image1" src="/static/img/loader-2.gif" >');
            $('#suggest-image2').replaceWith('<img id="suggest-image2" src="/static/img/loader-2.gif" >');
            $('#suggest-image3').replaceWith('<img id="suggest-image3" src="/static/img/loader-2.gif" >');
            $('#suggest-image4').replaceWith('<img id="suggest-image4" src="/static/img/loader-2.gif" >');
            $('#suggest-image5').replaceWith('<img id="suggest-image5" src="/static/img/loader-2.gif" >');
        }) ;
    snapshot_done_btn.attr('disabled', 'disabled');

    var url_to_render = result_tab.current;
    var ID = result_tab.current_data.ID;
    if(url_stack[ID]){
        var stack_length = url_stack[ID].length;
        url_to_render = url_stack[ID][stack_length-1];
    }

    var create_snapshot = $.ajax({
        type: 'POST',
        url: '/snapshot/create',
        cache: false,
        data: {
            url: url_to_render
        }
    }) ;

    create_snapshot.done(function(msg){
        // Reset JCropApi
        jcrop_api = null;
        $("#snapshot-image")
            .attr('src','/static/snapshot/'+msg.file)
            .attr('data-name',msg.file);


        $("#suggest-image1")
            .attr('src', msg.s_img1)
            .attr('data-name', msg.s_img1)

        $("#suggest-image2")
            .attr('src', msg.s_img2)
            .attr('data-name', msg.s_img2);
        $("#suggest-image3")
            .attr('src', msg.s_img3)
            .attr('data-name', msg.s_img3);
        $("#suggest-image4")
            .attr('src', msg.s_img4)
            .attr('data-name', msg.s_img4);
        $("#suggest-image5")
            .attr('src', msg.s_img5)
            .attr('data-name', msg.s_img5);

        $('#snapshot-image').Jcrop({
            onSelect: function(c){
            }
        }, function(){
            jcrop_api = this;
        });
        //console.log(snapshot_done_btn.removeAttr("disabled"))
        snapshot_done_btn.removeAttr("disabled");



        $('#suggest-image1').click(function(){
            data_img_url = msg.s_img1
            console.log(data_img_url)
            //console.log($('#suggest-image1').attr('src'))
        })
        $('#suggest-image2').click(function(){
            data_img_url = msg.s_img2
            console.log(data_img_url)
            //console.log($('#suggest-image2').attr('src'))
        })
        $('#suggest-image3').click(function(){
            data_img_url = msg.s_img3
            console.log(data_img_url)
            //console.log($('#suggest-image3').attr('src'))
        })
        $('#suggest-image4').click(function(){
            data_img_url = msg.s_img4
            console.log(data_img_url)
            //console.log($('#suggest-image3').attr('src'))
        })
        $('#suggest-image5').click(function(){
            data_img_url = msg.s_img5
            console.log(data_img_url)
            //console.log($('#suggest-image3').attr('src'))
        })
    });

    create_snapshot.fail(function(){
        noty({text: 'Can not create snapshot!!!', type: 'error'});
    });

    return false;
});


// Snapshot Done Event
$('#web-snapshot-done-btn').click(function(){
    //console.log(data_img_url)

    var dimension = jcrop_api.tellSelect();
    var this_result = _.find(search_result_store, function(search_result){ return search_result.url == result_tab.current; });

    if(dimension.h == 0 && dimension.w == 0 ){
        console.log($('#suggest-image1').attr('src'))
        $.snapshot_crop({type: "suggest", project_id: $.PKEY, member_id: user_id, result_id: this_result._id, src: data_img_url});
    }else{
        var width = dimension.w+20;
        var height = dimension.h+32;
        var left = dimension.x+5;
        var top = dimension.y+8;

        $.snapshot_crop({type: "crop", width: width, height: height, top: top, left: left, project_id: $.PKEY, member_id: user_id, result_id: this_result._id});
    }
});


$('#web-snapshot-close-btn').click(function(){
    $('#web-snapshot-container').modal('hide');
});
