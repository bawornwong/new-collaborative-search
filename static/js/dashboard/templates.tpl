<template id="search_result">
    <span class="search-result-link">
        <a id="search-result-open-{{ ID }}" href="#" oncontextmenu="return false;">
            {{ Title }}
        </a>
    </span>
    {% if (typeof(Thumbs_control) != "undefined") { %}
        <div class="search-result-action pull-right btn-group">
            <div id="search-result-relevant-{{ ID }}" class="btn search-result-relevant" >
                <i class="icon-thumbs-up"></i>
            </div>
            <div id="search-result-irrelevant-{{ ID }}" class="btn search-result-irrelevant" >
                <i class="icon-thumbs-down"></i>
            </div>
        </div>
    {% } %}
    <br>
    <span class="search-result-url">
        {{ DisplayUrl }}
    </span>
    <span class="search-result-description">
        {{ Description }}
    </span>
    <div class="search-result-minicomponent">
        <span class="label">
            <i class="icon-thumbs-up"></i>
            <span id="search-result-minicomponent-count-relevant-{{ ID }}">0</span>
        </span>
         <span class="label">
            <i class="icon-thumbs-down"></i>
            <span id="search-result-minicomponent-count-irrelevant-{{ ID }}">0</span>
        </span>
        <span class="label">
            <i class="icon-comments"></i>
            <span id="search-result-minicomponent-comment-{{ ID }}">0</span>
        </span>

        <span class="label">
            <i class="icon-picture"></i>
            <span id="search-result-minicomponent-snapshot-{{ ID }}">0</span>
        </span>

        <span class="label">
            <i class="icon-thumbs-up"></i>
            <i class="icon-thumbs-down"></i>
            <span id="search-result-minicomponent-relevant-{{ ID }}">0</span>
        </span>
        <span class="label">
            <i class="icon-eye-open"></i>
            <span id="search-result-minicomponent-seen-{{ ID }}">0</span>
        </span>
    </div>
</template>

<template id="search_component">
    <div class="tabbable tabs-below">
        <div class="tab-content">

           <div class="tab-pane active search-result-component-comment" id="search-result-component-comment-{{ ID }}">
                <ul>
                    <li class="comment-form">
                        <form id="search-result-component-comment-form-{{ ID }}" action="" method="post" accept-charset="utf-8" readonly="readonly">
                             <textarea id="search-result-component-comment-input-{{ ID }}" type="text" name="search-result-component-comment-input-{{ ID }}" placeholder="Comment here!" autocomplete="off" class="submit-not-disable"></textarea>
                             <br>
                             <button class="btn btn-success">submit</button>
                        </form>
                    </li>
                    <li class="no-comment">No comment.</li>
                </ul>
           </div>
           <div class="tab-pane search-result-component-snapshot" id="search-result-component-snapshot-{{ ID }}">
                <div class="snapshot_boxes" >
                      <div class="no-snapshot">
                            No snapshot.
                      </div>
                </div>
            </div>

            <div class="tab-pane search-result-component-relevant" id="search-result-component-relevant-{{ ID }}">
                <div class="wrap"><p class="no-relevant">No relevant.</p></div>
            </div>
            <div class="tab-pane search-result-component-seen" id="search-result-component-seen-{{ ID }}">
                <div class="wrap"><p>No seen.</p></div>
            </div>
        </div>
        <ul class="nav nav-tabs">
            <li class="active"><a href="#search-result-component-comment-{{ ID }}" data-toggle="tab">Comment</a></li>
            <li><a href="#search-result-component-snapshot-{{ ID }}" data-toggle="tab">Snapboard</a></li>
            <li><a href="#search-result-component-relevant-{{ ID }}" data-toggle="tab">Relevance</a></li>
            <li><a href="#search-result-component-seen-{{ ID }}" data-toggle="tab">Viewed</a></li>
        </ul>
    </div>
</template>

<template id="search_result_tab">
    <a href="#" id="search-result-tabmenu-{{ ID }}" class="search-result-tabmenu-link">
        <span class="search-result-tabmenu-title">{{ Title }}</span>
        <div id="search-result-tabmenu-{{ ID }}-remove" class="search-result-tabmenu-remove">
            <div class="icon-remove"></div>
        </div>
    </a>
</template>

<template id="search_result_export_to_pdf">
            <h1> Title: {{ Title }} </h1>
            Description: {{ Description }}
            <br>
            Link: <a href="{{ DisplayUrl }}" >
                        {{ DisplayUrl }}
                    </a>
            <br>
            Created: {{ created }}
            Snapshots: 0 image
</template>