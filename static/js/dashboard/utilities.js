// ============================================================================================================================================
//////////////////
// Function: Page loader
//////////////////
$.page_loader = function(action){
    if( action == 'show' ){

        if (loadqueue.page.length == 0){
            $('#coverpage').fadeIn('fast');

            $('#loadpage')
                .show()
                .css('opacity',0)
                .css('padding',0)
                .animate({
                    opacity: 1,
                    height: 'auto',
                    padding: '15px 10px 10px 10px'
                }, 'fast');
        }

        loadqueue.page.push($.random_string(10));

    }else{

        loadqueue.page.pop();

        if (loadqueue.page.length == 0){
            $('#coverpage').fadeOut('fast');

            $('#loadpage')
                .animate({
                    opacity: 0,
                    height: 'auto',
                    padding: '0px'
                }, 'fast')
                .hide();
        }
    }
};

// --------------------------------------------------------------------
//////////////////
// Function: show/hide loader
//////////////////
$.loader = function(id ,action){
    if( action == 'show' ){
        //alert($('#'+id+'-loader').length );
        if( $('#'+id+'-loader').length == 0 ){
            $('<div>')
                .attr('id', id+'-loader')
                .attr('class','loader')
                .html('Loading...')
                .insertBefore('#search-bar');
        }

        $('#'+id+'-loader')
            .show()
            .css('opacity',0)
            .css('padding',0)
            .animate({
                opacity: 1,
                height: 'auto',
                padding: '10px'
            }, 'fast');

        if ( $('#search-bar:hidden').length == 1 )
        {

            $('#'+id+'-loader').css('top','50px');
        }

    }else{
        $('#'+id+'-loader')
            .animate({
                opacity: 0,
                height: 'auto',
                padding: '0px'
            }, 'fast', function(){
                $(this).remove();
            });
    }
};