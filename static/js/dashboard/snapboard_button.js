/**
 * Created by bawor_000 on 12/4/2015.
 */
$.snapboard_button = function(action){
    if( action == 'show' ){
        $('#snapboard_button')
            .slideDown('slow');

        // Animate Main Section
        $('#main')
            .animate({
                top: 100
            }, 'fast');

        // Animate Search Bar
        $('#snapboard-bar')
            .show()
            .animate({
                top: 50,
                opacity: 1
            }, 'fast');

    }else{
        $('#snapboard_button')
            .slideUp('slow');

        // Animate Search Bar
        $('#main')
            .animate({
                top: 50
            }, 'fast');

        // Animate Main Section
        $('#snapboard-bar')
            .animate({
                top: 0,
                opacity: 0
            }, 'fast', function() {
                $('#snapboard-bar').show();
            });
    }
};