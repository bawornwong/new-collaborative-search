
// ============================================================================================================================================
//////////////////
// JScrollpane
//////////////////
// ============================================================================================================================================

// Projectlist
jsp.project_list.content = $('#project-list-content').jScrollPane({
    verticalGutter 	: -16,
    hideFocus: true
});

jsp.project_list.api = jsp.project_list.content.data('jsp');

// extend the jScollPane by merging
$.extend( true, jsp.project_list.api, $.jsp_extention(jsp.project_list.content) );
jsp.project_list.api.addHoverFunc();

jsp.project_list.reinitialise = function(){
    jsp.project_list.api.reinitialise();
    jsp.project_list.api.addHoverFunc();
};

// invite userlist
jsp.invite_userlist.content = $('#user-management-invite-userlist').jScrollPane({
    verticalGutter 	: -16,
    hideFocus: true
});

jsp.invite_userlist.api = jsp.invite_userlist.content.data('jsp');

// extend the jScollPane by merging
$.extend( true, jsp.invite_userlist.api, $.jsp_extention(jsp.invite_userlist.content) );
jsp.invite_userlist.api.addHoverFunc();

jsp.invite_userlist.reinitialise = function(){
    jsp.invite_userlist.api.reinitialise();
    jsp.invite_userlist.api.addHoverFunc();
};

// project userlist
jsp.project_userlist.content = $('#user-management-project-userlist').jScrollPane({
    verticalGutter 	: -16,
    hideFocus: true
});

jsp.project_userlist.api = jsp.project_userlist.content.data('jsp');

// extend the jScollPane by merging
$.extend( true, jsp.project_userlist.api, $.jsp_extention(jsp.project_userlist.content) );
jsp.project_userlist.api.addHoverFunc();

jsp.project_userlist.reinitialise = function(){
    jsp.project_userlist.api.reinitialise();
    jsp.project_userlist.api.addHoverFunc();
};