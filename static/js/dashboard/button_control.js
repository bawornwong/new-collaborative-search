// --------------------------------------------------------------------
//////////////////
// Relevant
//////////////////
$('#relevant-result-btn')
    .click(function () {
        $.loader('relevant', 'show');

        $.load_search_result(null, function () {
            $.search_panel('hide');
            $.keyword_panel('hide');
            $.seen_panel('hide');
            $.snapboard_panel('hide');
            $.activity_panel('time_hide');
            $.relevant_panel('show');
            $.relevant_result_list();
            $.loader('relevant', 'hide');
            $('.result-component').hide();
        });


    })
    .tooltip({
        title: 'Relevant search result',
        placement: 'bottom'
    });
//////////////////
// browser
//////////////////
$('#browser-result-btn')
    .click(function () {

        $.load_search_result(null, function () {
            $.search_panel('hide');
            $.keyword_panel('hide');
            $.activity_panel('time_hide');
            $.seen_panel('hide');
            $.snapboard_panel('hide');
            $.relevant_panel('hide');
            $.search_panel('show');
            $('.result-component').hide();

        });

    })
    .tooltip({
        title: 'browser search result',
        placement: 'bottom'
    });
// --------------------------------------------------------------------
//////////////////
// Keyword
//////////////////
$('#keyword-btn')
    .click(function () {
        $.loader('keyword', 'show');

        $.load_keyword(null, function () {

            $.search_panel('hide');
            $.relevant_panel('hide');
            $.activity_panel('time_hide');
            $.seen_panel('hide');
            $.keyword_panel('show');
            $.keyword_list();
            $.snapboard_panel('hide');
            $.loader('keyword', 'hide');
            $('.result-component').hide();

        });
    })
    .tooltip({
        title: 'Keywords were used',
        placement: 'bottom'
    });

// --------------------------------------------------------------------
//////////////////
// Activity
//////////////////
$('#activity-btn')
    .click(function () {
        //$.loader('activity', 'show');
        $.search_panel('hide');
        $.relevant_panel('hide');
        $.keyword_panel('hide');
        $.snapboard_panel('hide');
        $.seen_panel('hide');

        //$.update_activity(null, function () {
        //
        //    $.activity_panel('show');
        //    $.loader('activity', 'hide');
        //});
        $.load_activity_result(null, function () {

            $.activity_panel('shown');
            $('.result-component').hide();

            $.activity_list();
            $.loader('activity-loader', 'hide');
        });
    })
    .tooltip({
        title: 'Activity',
        placement: 'bottom'
    });

// --------------------------------------------------------------------



//////////////////
// Seen
//////////////////
$('#seen-btn')
    .click(function () {
        $.loader('seen', 'show');
        $.search_panel('hide');
        $.relevant_panel('hide');
        $.keyword_panel('hide');
        $.activity_panel('time_hide');
        $.snapboard_panel('hide');

        $.load_search_result(null, function () {

            $.seen_panel('show');
            $('.result-component').hide();
            //$.seen_result_list(); $.seen_result_list();
            $.seen_result_list();
            $.loader('seen', 'hide');
        });

    })
    .tooltip({
        title: 'Results were seen',
        placement: 'bottom'
    });

function smartTrim(str, length, delim, appendix) {
    if (str.length <= length) return str;

    var trimmedStr = str.substr(0, length + delim.length);

    var lastDelimIndex = trimmedStr.lastIndexOf(delim);
    if (lastDelimIndex >= 0) trimmedStr = trimmedStr.substr(0, lastDelimIndex);

    if (trimmedStr) trimmedStr += appendix;
    return trimmedStr;
}
////////-------------------------------------------------------------------------------///////////////////////////////

function show_snappet_sort(key, value) {
    console.log(value)

    if(value.img_type == "suggest"){
        html =
            "<div class='grid'>" +

                //"<div class=\"actions\" grid_id=\"2\">" +
                //    "<a href=\"#\" class=\"button\">Pin</a>" +
                //    "<a href=\"#\" class=\"button\">Like</a>" +
                //    "<a href=\"#\" class=\"button disabled comment_tr\">Comment</a>" +
                //"</div>" +

            "<div class='imgholder'>" +
            "<a data-title=" + value.member_id.firstname +
            " data-lightbox='" + value._id + "'href='"+ value.image + "'>" +
            "<img src=" + value.image + "></a>" +
            "</div>" +

                ////////Gridtext///////////
            "<div class='gridtext'>" +

                //Url
            "<div>" +
            "<img src='" + value.favicon + "' height='16' width='16' onerror=" + '"this.style.display=' + "\'none\'" + ';"' + " >" + "     " +
            "<a data-value='" + value + "' data-id='" + value.ID + "' data-url='" + value.result_id.url + "' class='metahostname snapshot-open' href='#'>" + "from : " + value.hostname + "     " + "</a>" +
            "<button type=\"button\" data-id='" + value.ID + "' data-url='" + value.result_id.url + "' class='btn btn-info btn-xs btn_url'>" + "Copy URL" + "</button>" +
            "<button type=\"button\" data-id='" + value.ID + "' data-url='" + value.result_id.url + "' class='btn btn-danger btn-xs snapshot-delete'>" + "Delete" + "</button>" +
            "</div>" +

                //Title
            "<div><strong><u>" +
            "<a data-value='" + value + "' data-id='" + value.ID + "' data-url='" + value.result_id.url + "' class='color_darkblue snapshot-open' href='#'>" + smartTrim(value.result_id.title, 30, ' ', ' ...') +
            "</a></u></strong></div>" +

                //Description
            "<div>" + value.result_id.description + "</div>" + "<hr>" +
            "</div>" +

                //Created by
            "<div class='metauser' data-size=\"2\">" +
            "<table class=\"nobreak\" style=\"width:100%\" bgcolor=\"#00FF00\">" +
            "<tr>" +
            "<td align='left' rowspan=\"2\" width=\"28px\">" + "<img class='img-circle' src='" + "/static/img/user_bt.png" + "' height='24' width='24' onerror=" + '"this.style.display=' + "\'none\'" + ';"' + " >" + "</td>" +
            "<td >" + "Created by " + value.member_id.firstname + "</td>" +
            "</tr>" +
            "<tr>" +
            "<td>" + value.showtime_created + "</td>" +
            "</tr>" +
            "</table>" +
            "</div>" +
            "<div class=\"search-result-action pull-right btn-group\">"+
                //"<div data-id='" + value.ID + "' id=\"search-result-relevant\" class=\"btn search-result-relevant\">"+
            "<i data-id='" + value.ID + "' id=\"search-result-relevant\" class=\"btn search-result-relevant icon-thumbs-up\">"+"</i>"+
                //"</div>"+
                //"<div data-id='" + value.ID + "' id=\"search-result-irrelevant\" class=\"btn search-result-irrelevant\">"+
            "<i data-id='" + value.ID + "' id=\"search-result-irrelevant\" class=\"btn search-result-irrelevant icon-thumbs-down\">"+"</i>"+
                //"</div>"+
            "</div>"+"</div>"

    }else{
        html =
            "<div class='grid'>" +
                "<div class='imgholder'>" + "<a data-title=" + value.member_id.firstname + " data-lightbox=" + value._id + " href='/static/snapshot/crop-" + value.image + "'>" +
                    "<img src='/static/snapshot/crop-" + value.image + "'>" + "</a>" +
                "</div>" +

                ////////Gridtext///////////
                "<div class='gridtext'>" +

                    //Url
                    "<div>" +
                        "<img src='" + value.favicon + "' height='16' width='16' onerror=" + '"this.style.display=' + "\'none\'" + ';"' + " >" + "     " +
                        "<a data-value='" + value + "' data-id='" + value.ID + "' data-url='" + value.result_id.url + "' class='metahostname snapshot-open' href='#'>" + "from : " + value.hostname + "     " + "</a>" +
                        "<button type=\"button\" data-id='" + value.ID + "' data-url='" + value.result_id.url + "' class='btn btn-info btn-xs btn_url'>" + "Copy URL" + "</button>" +
                        "<button type=\"button\" data-id='" + value.ID + "' data-url='" + value.result_id.url + "' class='btn btn-danger btn-xs snapshot-delete'>" + "Delete" + "</button>" +
                    "</div>" +

                    //Title
                    "<div><strong><u>" +
                    "<a data-value='" + value + "' data-id='" + value.ID + "' data-url='" + value.result_id.url + "' class='color_darkblue snapshot-open' href='#'>" + smartTrim(value.result_id.title, 30, ' ', ' ...') +
                    "</a></u></strong></div>" +

                    //Description
                        "<div>" + value.result_id.description + "</div>" + "<hr>" +
                "</div>" +

                //Created by
                "<div class='metauser' data-size=\"2\">" +
                    "<table class=\"nobreak\" style=\"width:100%\" bgcolor=\"#00FF00\">" +
                        "<tr>" +
                            "<td align='left' rowspan=\"2\" width=\"28px\">" + "<img class='img-circle' src='" + "/static/img/user_bt.png" + "' height='24' width='24' onerror=" + '"this.style.display=' + "\'none\'" + ';"' + " >" + "</td>" +
                            "<td >" + "Created by " + value.member_id.firstname + "</td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td>" + value.showtime_created + "</td>" +
                        "</tr>" +
                    "</table>" +
                "</div>" +
            "<div class=\"search-result-action pull-right btn-group\">"+
                //"<div data-id='" + value.ID + "' id=\"search-result-relevant\" class=\"btn search-result-relevant\">"+
            "<i data-id='" + value.ID + "' id=\"search-result-relevant\" class=\"btn search-result-relevant icon-thumbs-up\">"+"</i>"+
                //"</div>"+
                //"<div data-id='" + value.ID + "' id=\"search-result-irrelevant\" class=\"btn search-result-irrelevant\">"+
            "<i data-id='" + value.ID + "' id=\"search-result-irrelevant\" class=\"btn search-result-irrelevant icon-thumbs-down\">"+"</i>"+
                //"</div>"+
            "</div>"+"</div>"
    }

    return html
}


//var result_relevant_click = function(id){
//    // Push button state
//    if( $('#search-result-relevant').hasClass('push') ) {
//        $('#search-result-relevant').removeClass('push active');
//    }
//    else {
//        $('#search-result-relevant').addClass('push active');
//        $('#search-result-irrelevant').removeClass('push active');
//    }
//}
//
//var result_irrelevant_click = function(id){
//    // Push button state
//    if( $('#search-result-irrelevant').hasClass('push') ) {
//        $('#search-result-irrelevant').removeClass('push active');
//    }
//    else {
//        $('#search-result-irrelevant').addClass('push active');
//        $('#search-result-relevant').removeClass('push active');
//    }
//
//};

// --------------------------------------------------------------------
//////////////////
// Snapboard
//////////////////
function snapboard_sort(key_sort_by) {
    if (!project_list.current_project.project.enable_snapboard) {
        alert("Snapboard function is Disable");
        return;
    }
    $.load_search_result(null, function () {
        $.search_panel('xx');
        $.relevant_panel('hide');
        $.keyword_panel('hide');
        $.activity_panel('time_hide');
        $.seen_panel('hide');
        $.snapboard_panel('show');
        //$.snapboard_button('show');
        $.snapboard_result_list();
        $('.result-component').hide();
        var getImage = $.ajax({
            type: "POST",
            url: '/snapshot/get',
            cache: false,
            data: 'project_id=' + $.PKEY
        });
        getImage.done(function (msg) {
            $("#snapboard-result").html("");
            if (key_sort_by != "default") {
                msg = _.sortBy(msg, key_sort_by);
                $("#snapboard-result").empty()
                $("#snapboard-result")
                    .append(
                    "<div class='snapboard-bar'>" +
                    "<div class=\"dropdown\" style='display: inline-block'>" +
                    "<button class=\"btn-primary\" type=\"button\" " +
                    "id=\"dropdownMenu1\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">" +
                    "Sort By" +
                    "<span class=\"caret\"></span>" +
                    "</button>" +

                    "<ul class=\"dropdown-menu\" aria-labelledby=\"dropdownMenu1\" >" +
                    "<li><a id='sort-by-keyword' href='#keyword'>" + "sort by keyword" + "</a></li>" +
                    "<li><a id='sort-hostname' href='#hostname'>" + "sort by hostname" + "</a></li>" +
                    "<li><a id='sort-datecreate' href='#date'>" + "sort by date" + "</a><li>" +
                    "</ul>" +
                    "</div>" +
                    "</div>" +
                    "<div id='box_con'>");
                var keyword = [];
                //-----------------------search_by_keyword---------------------------
                if (key_sort_by == "search_by_keyword") {
                    $.each(msg, function (key, value) {
                        if (!_.contains(keyword, value.search_by_keyword)) {
                            keyword.push(value.search_by_keyword);
                        }
                    });
                    $.each(keyword, function (keyword_key, keyword_value) {
                        wanted_html = "<h1>" + keyword_value.toUpperCase() + "</h1>" + "<hr>" + "<div class='gridsort_bg' id='box_con'>"

                        $.each(msg, function (key, value) {
                            value.ID = value._id;
                            if (value.search_by_keyword == keyword_value) {
                                console.log(key, value);
                                //$.each(msg, show_snappet);
                                wanted_html += show_snappet_sort(key, value);
                            }
                        })
                        wanted_html += "</div><div class='clear'></div>";
                        $("#snapboard-result")
                            .append(wanted_html);

                    });
                    //-----------------------search_by_keyword---------------------------
                    //-----------------------hostname---------------------------
                }
                else if (key_sort_by == "hostname") {
                    $.each(msg, function (key, value) {
                        if (!_.contains(keyword, value.hostname)) {
                            keyword.push(value.hostname);
                        }
                    });
                    $.each(keyword, function (keyword_key, keyword_value) {
                        wanted_html = "<h1>" + keyword_value.toUpperCase() + "</h1>" + "<hr>" + "<div class='gridsort_bg' id='box_con'>"
                        $.each(msg, function (key, value) {
                            value.ID = value._id;
                            if (value.hostname == keyword_value) {
                                console.log(key, value);
                                wanted_html += show_snappet_sort(key, value);
                            }
                        });
                        wanted_html += "</div><div class='clear'></div>";
                        $("#snapboard-result")
                            .append(wanted_html);
                    });
                }
                //-----------------------hostname---------------------------
                //-----------------------created--------------------------------
                else if (key_sort_by == "created") {
                    $.each(msg, function (key, value) {
                        if (!_.contains(keyword, value.showtime_created)) {
                            keyword.push(value.showtime_created);
                        }
                    });
                    $.each(keyword, function (keyword_key, keyword_value) {
                        wanted_html = "<h1>" + keyword_value.toUpperCase() + "</h1>" + "<hr>" + "<div class='gridsort_bg' id='box_con'>"
                        $.each(msg, function (key, value) {
                            value.ID = value._id;
                            if (value.showtime_created == keyword_value) {
                                console.log(key, value);
                                wanted_html += show_snappet_sort(key, value);
                            }
                        });
                        wanted_html += "</div><div class='clear'></div>";
                        $("#snapboard-result")
                            .append(wanted_html)
                    });
                }
                //-----------------------created---------------------------
            } else {
                $("#snapboard-result")
                    .append(
                    "<div class='snapboard-bar'>" +
                    "<div class=\"dropdown\" style='display: inline-block'>" +
                    "<button class=\"btn-primary\" type=\"button\" " +
                    "id=\"dropdownMenu1\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">" +
                    "Sort By" +
                    "<span class=\"caret\"></span>" +
                    "</button>" +

                    "<ul class=\"dropdown-menu\" aria-labelledby=\"dropdownMenu1\" >" +
                    "<li><a id='sort-by-keyword' href='#keyword'>" + "sort by keyword" + "</a></li>" +
                    "<li><a id='sort-hostname' href='#hostname'>" + "sort by hostname" + "</a></li>" +
                    "<li><a id='sort-datecreate' href='#date'>" + "sort by date" + "</a><li>" +
                    "</ul>" +
                    "</div>" +
                    "</div>" +
                    "<div id='box_con'>");
                /////////show grid//////
                $.each(msg, function (key, value) {
                    value.ID = value._id;
                    wanted_html = show_snappet_sort(key, value);
                    $("#box_con")
                        .append(wanted_html)
                });
                $("#snapboard-result")
                    .append($("#box_con"))
            }
            /////////show grid//////


            $('.snapshot-open').on('click', function () {
                var value = $(this).data('value');
                var urls = $(this).data('url');
                var id = $(this).data('id');
                console.log("OPEN SNAPSHOT", value);
                // Init AJAX
                var search_result = $.ajax({
                    type: "POST",
                    url: "/search/getresult",
                    cache: false,
                    data: {
                        project_id: $.PKEY,
                        url: urls
                    }
                });
                // When done
                search_result.done(function (data) {
                    data.Title = data.title;
                    data.Url = data.url;
                    data.DisplayUrl = data.display_url;
                    data.Description = data.description;

                    if (data) {
                        console.log(data)
                        $.search_result_tab(data, 'open');
                        $('#search').focus().blur();
                    }
                });
            });

            $('.snapshot-delete').unbind().on('click', function () {
                var value = $(this).data('value');
                var urls = $(this).data('url');
                var ids = $(this).data('id');
                console.log("delete", ids);

                var getImage = $.ajax({
                    type: "POST",
                    url: '/snapshot/get_delete',
                    cache: false,
                    data: {
                        project_id: $.PKEY,
                        ids: ids
                    }
                });
                getImage.done(function (data) {
                    snapboard_sort("default")
                });

            });

            // Relevant

            $('.search-result-relevant').unbind().on('click', function () {
                var id = $(this).data('id');
                console.log("result_relevant_click", id)
                if( $('.search-result-relevant').hasClass('push') ) {
                    $('.search-result-relevant').removeClass('push active');
                }
                else {
                    $('.search-result-relevant').addClass('push active');
                    $('.search-result-irrelevant').removeClass('push active');
                }
            });

            // Irrelevant
            $('.search-result-irrelevant').unbind().on('click', function () {
                var id = $(this).data('id');
                console.log("result_irrelevant_click", id)
                if( $('.search-result-irrelevant').hasClass('push') ) {
                    $('.search-result-irrelevant').removeClass('push active');
                }
                else {
                    $('.search-result-irrelevant').addClass('push active');
                    $('.search-result-relevant').removeClass('push active');
                }
            });


            $('.btn_url').unbind().on('click', function () {
                var id = $(this).data('id');
                var url = $(this).data('url');
                noty({
                    text: 'A link to' + url + 'has been copied to ypur clipboard',
                    type: 'success'
                });
                var clipboard = new Clipboard('.btn_url', {
                    text: function () {
                        return url;
                    }
                });
            });

            $('#sort-by-keyword')
                .click(function () {
                    snapboard_sort("search_by_keyword")
                })

            $('#sort-hostname')
                .click(function () {
                    snapboard_sort("hostname")
                })

            $('#sort-datecreate')
                .click(function () {
                    snapboard_sort("created")
                })

            $('.comment_tr').click(function () {
                $(this).toggleClass('disabled');
                $(this).parent().parent().parent().find('form').slideToggle(250, function () {
                    //$('.main_container').masonry();
                });
            });


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            setTimeout(function () {
                $('.gridsort_bg').pinto({
                    itemWidth: 330,
                    gapX: 10,
                    gapY: 10,
                    onItemLayout: function ($item, column, position) {
                    }
                });

                $('#box_con').pinto({
                    itemWidth: 330,
                    gapX: 10,
                    gapY: 10,
                    onItemLayout: function ($item, column, position) {
                    }
                });
            }, 100);
        });
    });
}
//////////////////
$('#snapboard-btn')
    .click(function () {
        snapboard_sort("default");
    })
    .tooltip({
        title: 'Results were snapboard',
        placement: 'bottom'
    });
//////////////////

// --------------------------------------------------------------------
//////////////////
// Activity
//////////////////
$('#user-activity-btn')
    .click(function () {
        $('#sidebar-panel-useractivity').show();
        $('#sidebar-panel-activity').hide();
    });

$('#all-activity-btn')
    .click(function () {
        $('#sidebar-panel-useractivity').hide();
        $('#sidebar-panel-activity').show();
    });

// --------------------------------------------------------------------
//////////////////
// Edit profile
//////////////////
$('#edit-profile-btn').click(function () {
    $('#edit-profile-container').modal('show');

    return false;
});

$('#edit-profile-close-btn, #edit-profile-submit-btn').click(function () {
    $('#edit-profile-container').modal('hide');

    return false;
});

// Relevant button
$('#search-component-result-relevant').click(function (e) {
    var data = result_tab.current_data;
    data.key = $.PKEY;

    // Push button state
    if ($(this).hasClass('push')) {
        $(this).removeClass('push active');
    }
    else {
        $(this).addClass('push active');
        $('#search-component-result-irrelevant').removeClass('push active');
    }

    $.relevant_result(data, true);

    e.stopPropagation();
});

// Irrelevant button
$('#search-component-result-irrelevant').click(function (e) {
    var data = result_tab.current_data;
    data.key = $.PKEY;

    // Push button state
    if ($(this).hasClass('push')) {
        $(this).removeClass('push active');
    }
    else {
        $(this).addClass('push active');
        $('#search-component-result-relevant').removeClass('push active');

    }

    $.relevant_result(data, true);

    e.stopPropagation();
});