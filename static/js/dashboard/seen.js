
// --------------------------------------------------------------------
//////////////////
// Function: show/hide seen panel
//////////////////
$.seen_panel = function(action){
    if( action == 'show' ){
        $('#seen-panel')
            .slideDown('slow');
        $('#seen-btn')
            .addClass('active');
    }else{
        $('#seen-panel')
            .slideUp('slow', function(){
                $('.seen-result-item').empty();
            });
        $('#seen-btn')
            .removeClass('active');
    }
};

// --------------------------------------------------------------------
//////////////////
// Function: seen result list
//////////////////

$.seen_result_list = function(){
    $('.search-result-contain.seen-search-result').remove();
    console.log("seen_re:", search_result_store)

    if ( search_result_store && search_result_store.length > 0 )
    {
        $('#seen-result').slideDown('fast');
        $('#seen-empty').slideUp('fast');

        var filter_result = _.filter(search_result_store, function(store){ return store.seen.length > 0; });

        $.each(filter_result, function(key, value){
            $.insert_seen_search_result(key, value);
        });

    }
    else
    {
        $('#seen-empty').slideDown('fast');
    }
};

// --------------------------------------------------------------------
//////////////////
// Function: Insert seen search result
//////////////////

$.insert_seen_search_result = function(key, value){
    if (value.seen.length > 0 )
    {
        //seen point
        console.log("xxx/xxx/xxxx", value, value.seen.length)
        //seen point
        value.ID = value._id;
        value.DisplayUrl = value.display_url;
        value.Title = value.title;
        value.Description = value.description;
        value.Url = value.url;

        // Insert Result
        var search_contain = $('<div>')
            .attr('id', 'search-result-contain-'+value.ID)
            .addClass('search-result-contain seen-search-result seen-result-item');

        var search_result = $('<div>')
            .attr('id', 'search-result-'+value.ID)
            .attr('class','search-result seen')
            .html(templates.search_result(value));

        var search_component = $('<div>')
            .addClass('hide')
            .attr('id', 'search-result-component-'+value.ID)
            .addClass('search-result-component')
            .html(templates.search_component(value));

        var count_relevant = _.countBy(value.relevant, function(relevant) {
            return relevant.state;
        });

        count_relevant.Relevant = ( count_relevant.Relevant ? count_relevant.Relevant : 0);
        count_relevant.Irrelevant = ( count_relevant.Irrelevant ? count_relevant.Irrelevant : 0);
        count_relevant.Maybe = ( count_relevant.Maybe ? count_relevant.Maybe : 0);

        var not_relevant = value.project.associate.length - (count_relevant.Relevant + count_relevant.Irrelevant + count_relevant.Maybe);

        var percent = {
            relevant: (count_relevant.Relevant/value.project.associate.length*100).toFixed(2),
            irrelevant: (count_relevant.Irrelevant/value.project.associate.length*100).toFixed(2),
            maybe: (count_relevant.Maybe/value.project.associate.length*100).toFixed(2),
            not_relevant: (not_relevant/value.project.associate.length*100).toFixed(2)
        };

        if(percent.not_relevant < 100)
        {
            if (percent.relevant + percent.maybe > percent.irrelevant + percent.maybe)
            {
                search_contain.insertAfter('#relevant-result-relevant > h1');
                search_result.addClass('relevant');
            }
            else if (percent.relevant + percent.maybe < percent.irrelevant + percent.maybe)
            {
                search_contain.insertAfter('#relevant-result-irrelevant > h1');
                search_result.addClass('irrelevant');
            }
            else if (percent.relevant + percent.maybe == percent.irrelevant + percent.maybe)
            {
                search_contain.insertAfter('#relevant-result-notsure > h1');
                search_result.addClass('mayberelevant');
            }
        }

        // Search Contain
        search_contain.appendTo('#seen-result');
        search_contain.append(search_result);
        search_contain.append(search_component);

        console.log(value);
        $.fragment_result(value, search_contain);
        $.check_result(value, search_contain);
    }
};