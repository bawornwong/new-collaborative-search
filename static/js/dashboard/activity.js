// --------------------------------------------------------------------
//////////////////
// Function: show/hide activity panel
//////////////////
$.activity_panel = function(action){
    if( action == 'show' ){
        $('#activity-panel')
            .slideDown('slow');
        $('#sidebar-panel-activity')
            .slideDown('slow');
        $('#activity_bt')
            .addClass('active');
    }else if( action == "shown") {
        $('#activity-timeline-panel')
            .slideDown('slow');
        $('#activity_bt')
            .addClass('active');
    }else if(action == 'time_hide'){
        $('#activity-timeline-panel')
            .slideUp('slow');
        $('#sidebar-panel-activity')
            .slideDown('slow');
        $('#activity_bt')
            .removeClass('active');
    }
    else{
        $('#activity-timeline-panel')
            .slideUp('slow');
        $('#activity_bt')
            .removeClass('active');

    }
};
//////////////////////////////////////////////////////////////////////////
$.load_activity_result = function(data, callback){

    $('#activity-loader')
        .animate(
        {'height': 0,
            'margin-bottom': 0},
        'fast',
        function(){
            $(this).hide();
        }
    );
    if (!data) {
        data = {};
    }

    $.loader('activity', 'show');
    data['key'] = $.PKEY;

    var get_activity = $.ajax({
        type: "Post",
        url: '/activity/get',
        cache: false,
        data: 'project_id=' + $.PKEY
    });

    get_activity.done(function (msg) {
        $('.cd-container.activity-timeline-result').remove();
            console.log("load_search_result: ", msg)
            activity_result_store = msg.results;

            if (callback)
            {
                callback();
            }

            $.loader('activity', 'hide');
    });

    // When fail
    get_activity.fail(function(msg){
        $.loader('activity', 'hide');
    });
};


$.activity_list = function(){


    if ( activity_result_store && activity_result_store.length > 0 )
    {

        $('#activity-timeline-result').slideDown('fast');
        $('#activity-timeline-empty').slideUp('fast');

        var message = ""
        var date = ""
        var created = ""

        var timeline_contain = $('<div>')
            .attr('id', 'cd-timeline')
            .addClass('cd-container activity-timeline-result')

        //timeline_contain.appendTo('#activity-timeline-result');

        $.each(activity_result_store, function(key, value){

            timeline_contain.appendTo('#activity-timeline-result');

            created = new moment(value.created);
            date = new moment(value.created).format("MMM Do YYYY")
            value.ID = value._id;

            //console.log(key, value.module)

            if(value.module == "query"){
                var timeline_icon = $('<div>')
                    .attr('id', value._id + '-activity-timeline-icon')
                    .attr('class', 'cd-timeline-img cd-query')
                    .html('<img src="https://bytebucket.org/petabite/new-collaborative-search/raw/c378442db473e7bceb137fdf7cb20b34ef6f5e5b/static/img/timeline-icon/query-icon.png" alt="Movie">');

                message = 'query: '+'<a id="search-result-open-query-'+value.ID+'" href="#">'+value.message.substring(15)+'</a>';
            }
            else if(value.module == "seen"){
                var timeline_icon = $('<div>')
                    .attr('id', value._id + '-activity-timeline-icon')
                    .attr('class', 'cd-timeline-img cd-open')
                    .html('<img src="https://bytebucket.org/petabite/new-collaborative-search/raw/c378442db473e7bceb137fdf7cb20b34ef6f5e5b/static/img/timeline-icon/open-icon.png" alt="Movie">');

                message = 'open: '+'<a id="search-result-open-seen-'+value.ID+'" href="#">'+value.message.substring(16)+'</a>';
            }
            else if(value.module == "snapshot"){
                var timeline_icon = $('<div>')
                    .attr('id', value._id + '-activity-timeline-icon')
                    .attr('class', 'cd-timeline-img cd-snapshot')
                    .html('<img src="https://bytebucket.org/petabite/new-collaborative-search/raw/c378442db473e7bceb137fdf7cb20b34ef6f5e5b/static/img/timeline-icon/snapshot-icon.png" alt="Movie">');

                message = 'snapshot: '+'<a id="search-result-open-snapshot-'+value.ID+'" href="#">'+value.message.substring(18)+'</a>';
            }
            else if(value.module == "comment"){
                var timeline_icon = $('<div>')
                    .attr('id', value._id + '-activity-timeline-icon')
                    .attr('class', 'cd-timeline-img cd-comment')
                    .html('<img src="https://bytebucket.org/petabite/new-collaborative-search/raw/c378442db473e7bceb137fdf7cb20b34ef6f5e5b/static/img/timeline-icon/comment-icon.png" alt="Movie">');

                message = 'comment: '+'<a id="search-result-open-comment-'+value.ID+'" href="#">'+value.message.substring(19)+'</a>';
            }
            else if(value.module == "relevant"){

                if (value.message.substring(0,23) == "make relevant:123456789"){
                    var timeline_icon = $('<div>')
                        .attr('id', value._id + '-activity-timeline-icon')
                        .attr('class', 'cd-timeline-img cd-relevant')
                        .html('<img src="https://bytebucket.org/petabite/new-collaborative-search/raw/c378442db473e7bceb137fdf7cb20b34ef6f5e5b/static/img/timeline-icon/relevant-icon.png" alt="Movie">');


                    message = ''+value.message.substring(0,14)+' '+'<a id="search-result-open-relevant-'+value.ID+'" href="#">'+value.message.substring(23)+'</a>';
                }
                else{
                    var timeline_icon = $('<div>')
                        .attr('id', value._id + '-activity-timeline-icon')
                        .attr('class', 'cd-timeline-img cd-irrelevant')
                        .html('<img src="https://bytebucket.org/petabite/new-collaborative-search/raw/43dbfd3c40c977025127f53e49141e984cfd62ac/static/img/timeline-icon/inrelevant-icon.png" alt="Movie">');


                    message = ''+value.message.substring(0,15)+' '+'<a id="search-result-open-relevant-'+value.ID+'" href="#">'+value.message.substring(18, value.message.length-1)+'</a>';
                }
            }
            else
            {
                //console.log(value.message.substring(0, 5))
                if(value.message.substring(0, 5) == "enter"){
                    var timeline_icon = $('<div>')
                        .attr('id', value._id + '-activity-timeline-icon')
                        .attr('class', 'cd-timeline-img cd-enter_left')
                        .html('<img src="https://bytebucket.org/petabite/new-collaborative-search/raw/c378442db473e7bceb137fdf7cb20b34ef6f5e5b/static/img/timeline-icon/enter-icon.png" alt="Movie">');
                }else{
                    var timeline_icon = $('<div>')
                        .attr('id', value._id + '-activity-timeline-icon')
                        .attr('class', 'cd-timeline-img cd-enter_left')
                        .html('<img src="https://bytebucket.org/petabite/new-collaborative-search/raw/c378442db473e7bceb137fdf7cb20b34ef6f5e5b/static/img/timeline-icon/left-icon.png" alt="Movie">');
                }
                message = value.message;
            }


            var search_contain = $('<div>')
                .attr('id', 'search-result-contain-'+value.ID)
                .addClass('cd-timeline-block activity-timeline-result');


            /////// unknow

            //if(value.module==undefined) {
            //    if( value.message.substring(0, 16)=="opened:123456789"){
            //        var timeline_result = $('<div>')
            //            .attr('id', value._id + '-activity-item')
            //            .attr('class', 'cd-timeline-content bounce-in')
            //            .html('<span class="search-result-link"><strong>' + value.member.firstname + ' ' + value.member.lastname + '</strong> ' + 'opened: '+'<a id="search-result-open-seen-' + value.ID + '" href="#">' + message.substring(16) + '</a>' + '.</span>' +
            //            '<span class="activity-info"><span class="activity-info-date">' + created.format('LT') + '</span></span>');
            //
            //    }
            //    else if( value.message.substring(0,23)== "make relevant:123456789"){
            //        var timeline_result = $('<div>')
            //            .attr('id', value._id + '-activity-item')
            //            .attr('class', 'cd-timeline-content bounce-in')
            //            .html('<span class="activity-info-message"><strong>' + value.member.firstname + ' ' + value.member.lastname + '</strong> ' + 'make relevant: '+'<a id="search-result-open-relevant-' + value.ID + '" href="#">' + message.substring(23).replace('"',"") + '</a>' + '.</span>' +
            //            '<span class="activity-info"><span class="activity-info-date">' + created.format('LT') + '</span></span>');
            //    }
            //    else if( value.message.substring(0,18)== "snapshot:123456789"){
            //        var timeline_result = $('<div>')
            //            .attr('id', value._id + '-activity-item')
            //            .attr('class', 'cd-timeline-content bounce-in')
            //            .html('<span class="activity-info-message"><strong>' + value.member.firstname + ' ' + value.member.lastname + '</strong> ' + 'snapshot: '+'<a id="search-result-open-snapshot-' + value.ID + '" href="#">' + message.substring(18) + '</a>' + '.</span>' +
            //            '<span class="activity-info"><span class="activity-info-date">' + created.format('LT') + '</span></span>');
            //        console.log(message.substring(18));
            //    }
            //    else if(value.message.substring(0,15)=="query:123456789"){
            //        var timeline_result = $('<div>')
            //            .attr('id', value._id + '-activity-item')
            //            .attr('class', 'cd-timeline-content bounce-in')
            //            .html('<span class="activity-info-message"><strong>' + value.member.firstname + ' ' + value.member.lastname + '</strong> ' + 'query: '+'<a id="search-result-open-query-' + value.ID + '" href="#">' + message.substring(15) + '</a>' + '.</span>' +
            //            '<span class="activity-info"><span class="activity-info-date">' + created.format('LT') + '</span></span>');
            //    }
            //    else if( value.message.substring(0,19)== "commented:123456789"){
            //        var timeline_result = $('<div>')
            //            .attr('id', value._id + '-activity-item')
            //            .attr('class', 'cd-timeline-content bounce-in')
            //            .html('<span class="activity-info-message"><strong>' + value.member.firstname + ' ' + value.member.lastname + '</strong> ' + 'comment: '+'<a id="search-result-open-comment-' + value.ID + '" href="#">' + message.substring(19) + '</a>' + '.</span>' +
            //            '<span class="activity-info"><span class="activity-info-date">' + created.format('LT') + '</span></span>');
            //        console.log(message.substring(18));
            //    }
            //    else{
            //        var timeline_result = $('<div>')
            //            .attr('id', value._id + '-activity-item')
            //            .attr('class', 'cd-timeline-content bounce-in')
            //            .html('<span class="activity-info-message"><strong>' + value.member.firstname + ' ' + value.member.lastname + '</strong> ' + message + '.</span>' +
            //            '<span class="activity-info"><span class="activity-info-date">' + created.format('LT') + '</span></span>');
            //    }
            //}
            //else{

                var timeline_result = $('<div>')
                    .attr('id', value._id + '-activity-item')
                    .attr('class', 'cd-timeline-content bounce-in')
                    .html('<span class="activity-info-message"><strong>' + value.member.firstname + ' ' + value.member.lastname + '</strong> ' + message + '.</span>' +
                    '<span class="activity-info"><span class="activity-info-date">' + created.format('LT') + '</span></span>');
            //}

            $("#activity-timeline-result").find('#search-result-open-comment-'+value.ID).live('click',(function (){
                $.load_search_result(null,function(){
                    $.each( search_result_store, function( key, data ) {
                        if(value.message.substring(19)==data.url){
                            data.Title=data.title;
                            $.search_result_tab(data, 'open');
                            $.activity_panel('time_hide')
                            $.search_panel('show')
                            return
                        }
                    });
                });
            }));

            $("#activity-timeline-result").find('#search-result-open-snapshot-'+value.ID).live('click',(function (){
                $.load_search_result(null,function(){
                    $.each( search_result_store, function( key, data ) {
                        if(value.message.substring(18)==data.url){
                            data.Title=data.title;
                            $.search_result_tab(data, 'open');
                            $.activity_panel('time_hide')
                            $.search_panel('show')
                            return
                        }
                    });
                });
            }));

            $("#activity-timeline-result").find('#search-result-open-relevant-'+value.ID).live('click',(function (){
                $.load_search_result(null,function(){
                    $.each( search_result_store, function( key, data ) {
                        if(value.message.substring(23)== data.url || value.message.substring(18, value.message.length-1) == data.url){
                            data.Title = data.title;
                            $.search_result_tab(data, 'open');
                            $.activity_panel('time_hide')
                            $.search_panel('show')
                            return
                        }
                    });
                });
            }));

            $("#activity-timeline-result").find('#search-result-open-seen-'+value.ID).live('click',(function (){
                $.load_search_result(null,function(){
                    $.each( search_result_store, function( key, data ) {
                        if(value.message.substring(16)==data.url){
                            data.Title=data.title;
                            console.log("seen");
                            console.log(data);
                            $.search_result_tab(data, 'open');
                            $.activity_panel('time_hide')
                            $.search_panel('show')
                            return
                        }
                    });
                });
            }));
            $('#search-result-open-query-'+value.ID)
                .live("click",function(){
                    $('#search').val(value.message.substring(15)).submit().blur();
                });


            //timeline_contain.appendTo(search_contain)

            $(timeline_contain).append(search_contain)
            $(search_contain).append(timeline_icon, timeline_result);

            $('#activity-result-'+value.ID+'').click(function(){
                if( $('#activity-result-'+value.ID).hasClass('show-component') )
                {
                    $('#activity-result-'+value.ID).removeClass('show-component');
                }
                else
                {
                    $.check_result(value);
                    $('#activity-result-'+value.ID).addClass('show-component');
                }
            });

        });
    }
    else
    {
        $('activity-empty').slideDown('fast');
    }
};





// --------------------------------------------------------------------
//////////////////
// Function: Update activity
//////////////////
$.update_activity = function(activity_updated){
    // Show chat loader
    $('#activity-loader')
        .show()
        .animate(
        {'height': 20,
            'margin-bottom': 10}
    );

    // Update message
    var data = 'pkey='+$.PKEY;
    var new_update = false;
    var nonscroll = false;

    // Check chat updatd
    if ( activity_updated == null )
    {
        data = 'pkey='+$.PKEY;
        new_update = true;
    }
    else
    {
        data = 'update='+activity_updated+'&pkey='+$.PKEY;
    }

    var update_message = $.ajax({
        type:"POST",
        url:"/activity/get",
        cache: false,
        data: data
    });

    update_message.done(function(msg){
        var receive = msg;

        // Hide chat loader
        $('#activity-loader')
            .animate(
            {'height': 0,
                'margin-bottom': 0},
            'fast',
            function(){
                $(this).hide();
            }
        );

        // Chat scroll event
        if(receive.next)
        {
            $('#activity-seemore')
                .show()
                .unbind('click')
                .click(function(){
                    $(this)
                        .unbind('click')
                        .hide();
                    $.update_activity(receive.last.created);
                });
        }

        $.each(receive.results, function(key, value){

            $.insert_activity(value, true);

            $('#activity-empty').slideUp('fast');
        });
    });
    update_message.fail(function(msg){

    });

};

// --------------------------------------------------------------------
//////////////////
// Function: Insert activity
//////////////////
$.insert_activity = function(value, initial){
    $('#activity-empty:visible').slideUp();
        // insert message
    var created = new moment(value.created);
    var message = ""
    value.ID = value._id;

    if(value.module == "query"){
        message = 'query: '+'<a id="search-result-open-query-'+value.ID+'" href="#">'+value.message.substring(15)+'</a>';
    }
    else if(value.module == "seen"){
        message = 'open: '+'<a id="search-result-open-seen-'+value.ID+'" href="#">'+value.message.substring(16)+'</a>';
    }
    else if(value.module == "snapshot"){
        message = 'snapshot: '+'<a id="search-result-open-snapshot-'+value.ID+'" href="#">'+value.message.substring(18)+'</a>';
    }
    else if(value.module == "comment"){
        message = 'comment: '+'<a id="search-result-open-comment-'+value.ID+'" href="#">'+value.message.substring(19)+'</a>';
    }
    else if(value.module == "relevant"){

        if (value.message.substring(0,23) == "make relevant:123456789"){

            message = ''+value.message.substring(0,14)+'<a id="search-result-open-relevant-'+value.ID+'" href="#">'+value.message.substring(23)+'</a>';
        }
        else{

            message = value.message;
        }
    }
    else
    {
        message = value.message;
    }

    if(value.module==undefined) {


            if( value.message.substring(0, 16)=="opened:123456789"){
                var insert = $('<div>')
                    .attr('id', value._id + '-activity-item')
                    .attr('class', 'activity-item hide')
                    .html('<span class="activity-info-message"><strong>' + value.member.firstname + ' ' + value.member.lastname + '</strong> ' + 'opened: '+'<a id="search-result-open-seen-' + value.ID + '" href="#">' + message.substring(16) + '</a>' + '.</span>' +
                        '<span class="activity-info"><span class="activity-info-date">' + created.fromNow() + '</span></span>');
            }
            else if( value.message.substring(0,23)== "make relevant:123456789"){
                var insert = $('<div>')
                    .attr('id', value._id + '-activity-item')
                    .attr('class', 'activity-item hide')
                    .html('<span class="activity-info-message"><strong>' + value.member.firstname + ' ' + value.member.lastname + '</strong> ' + 'make relevant: '+'<a id="search-result-open-relevant-' + value.ID + '" href="#">' + message.substring(23).replace('"',"") + '</a>' + '.</span>' +
                        '<span class="activity-info"><span class="activity-info-date">' + created.fromNow() + '</span></span>');
            }
            else if( value.message.substring(0,18)== "snapshot:123456789"){
                var insert = $('<div>')
                    .attr('id', value._id + '-activity-item')
                    .attr('class', 'activity-item hide')
                    .html('<span class="activity-info-message"><strong>' + value.member.firstname + ' ' + value.member.lastname + '</strong> ' + 'snapshot: '+'<a id="search-result-open-snapshot-' + value.ID + '" href="#">' + message.substring(18) + '</a>' + '.</span>' +
                        '<span class="activity-info"><span class="activity-info-date">' + created.fromNow() + '</span></span>');
                console.log(message.substring(18));
            }
            else if(value.message.substring(0,15)=="query:123456789"){
                var insert = $('<div>')
                    .attr('id', value._id + '-activity-item')
                    .attr('class', 'activity-item hide')
                    .html('<span class="activity-info-message"><strong>' + value.member.firstname + ' ' + value.member.lastname + '</strong> ' + 'query: '+'<a id="search-result-open-query-' + value.ID + '" href="#">' + message.substring(15) + '</a>' + '.</span>' +
                        '<span class="activity-info"><span class="activity-info-date">' + created.fromNow() + '</span></span>');
            }
            else if( value.message.substring(0,19)== "commented:123456789"){
                var insert = $('<div>')
                    .attr('id', value._id + '-activity-item')
                    .attr('class', 'activity-item hide')
                    .html('<span class="activity-info-message"><strong>' + value.member.firstname + ' ' + value.member.lastname + '</strong> ' + 'comment: '+'<a id="search-result-open-comment-' + value.ID + '" href="#">' + message.substring(19) + '</a>' + '.</span>' +
                        '<span class="activity-info"><span class="activity-info-date">' + created.fromNow() + '</span></span>');
                console.log(message.substring(18));
            }
            else{
                var insert = $('<div>')
                    .attr('id', value._id + '-activity-item')
                    .attr('class', 'activity-item hide')
                    .html('<span class="activity-info-message"><strong>' + value.member.firstname + ' ' + value.member.lastname + '</strong> ' + message + '.</span>' +
                        '<span class="activity-info"><span class="activity-info-date">' + created.fromNow() + '</span></span>');
            }
    }
    else{
        var insert = $('<div>')
            .attr('id', value._id + '-activity-item')
            .attr('class', 'activity-item hide')
            .html('<span class="activity-info-message"><strong>' + value.member.firstname + ' ' + value.member.lastname + '</strong> ' + message + '.</span>' +
                '<span class="activity-info"><span class="activity-info-date">' + created.fromNow() + '</span></span>');

    }

    $("#sidebar-panel").find('#search-result-open-comment-'+value.ID).live('click',(function (){
        $.load_search_result(null,function(){
            $.each( search_result_store, function( key, data ) {
                if(value.message.substring(19)==data.url){
                    data.Title=data.title;
                    $.search_result_tab(data, 'open');
                    return
                }
            });
        });
    }));

    $("#sidebar-panel").find('#search-result-open-snapshot-'+value.ID).live('click',(function (){
        $.load_search_result(null,function(){
            $.each( search_result_store, function( key, data ) {
                if(value.message.substring(18)==data.url){
                    data.Title=data.title;
                    $.search_result_tab(data, 'open');
                    return
                }
            });
        });
    }));

    $("#sidebar-panel").find('#search-result-open-relevant-'+value.ID).live('click',(function (){
        $.load_search_result(null,function(){
            $.each( search_result_store, function( key, data ) {
                if(value.message.substring(23)==data.url){
                    data.Title=data.title;
                    $.search_result_tab(data, 'open');
                    return
                }
            });
        });
    }));

    $("#sidebar-panel").find('#search-result-open-seen-'+value.ID).live('click',(function (){
        $.load_search_result(null,function(){
            $.each( search_result_store, function( key, data ) {
                if(value.message.substring(16)==data.url){
                    data.Title=data.title;
                    console.log("seen");
                    console.log(data);
                    $.search_result_tab(data, 'open');
                    return
                }
            });

        });
    }));

    // Set open result action
    $('#search-result-open-query-'+value.ID)
        .live("click",function(){
            $('#search').val(value.message.substring(15)).submit().blur();
    });

    if (initial)
    {
        insert.insertBefore('#activity-loader');
    }
    else
    {
        insert.insertBefore('#sidebar-panel-activity > :first-child');
        $('#sidebar-panel-activity').scrollTop(0);
    }

    if (initial)
    {
        insert.show();
    }
    else
    {
        insert
            .css('opacity',0)
            .css('padding-top',0)
            .css('padding-bottom',0)
            .animate({
                opacity: 1,
                height: 'auto',
                padding: '10px'
            }, 'fast');
    }

    setInterval(function(){
        var created = new moment(value.created);
        $('#'+value._id+'-activity-item .activity-info-date').text(created.fromNow());
    }, 1000);
};