// --------------------------------------------------------------------
//////////////////
// Function: show/hide keyword panel
//////////////////
$.keyword_panel = function(action){
    if( action == 'show' ){
        $('#keyword-panel')
            .slideDown('slow');
        $('#keyword-btn')
            .addClass('active');
    }else{
        $('#keyword-panel')
            .slideUp('slow');
        $('#keyword-btn')
            .removeClass('active');
    }
}

// --------------------------------------------------------------------
//////////////////
// Function: Insert keyword search result
//////////////////
$.insert_keyword_result = function(key, value){
    if (value.seen.length > 0 )
    {
        value.ID = value._id;

        // insert message
        var insert_to = 'search-result';
        var search_contain = $('<div>')
            .attr('id', 'search-result-contain-'+value.ID)
            .addClass('search-result-contain keyword-result');

        var search_result = $('<div>')
            .attr('id', 'search-result-'+value.ID)
            .attr('class','search-result')
            .html( '<span class="search-result-link"><a id="search-result-open-'+value.ID+'" href="#">'+value.keyword+'</a></span>'+
                '<br><div class="search-result-minicomponent">'+
                '<span class="label"><i class="icon-eye-open"></i> <span id="search-result-minicomponent-seen-'+value.ID+'">0</span></span>'+
                '</div>');
        var search_component = $('<div>')
            .addClass('hide')
            .attr('id', 'search-result-component-'+value.ID)
            .addClass('search-result-component')
            .html('<div class="tab-pane active search-result-component-seen" id="search-result-component-keyword-'+value.ID+'">'+
                '<div class="wrap"><p>No used.</p></div>'+
                '</div>');

        search_contain.appendTo('#keyword-result');
        // Search Contain
        $(search_contain).append(search_result);
        $(search_contain).append(search_component);

        $('#search-result-minicomponent-seen-'+value.ID).text(value.seen.length);

        // Open and Close Description
        $('#search-result-'+value.ID+'').click(function(){
            if( $('#search-result-'+value.ID).hasClass('show-component') )
            {
                $('#search-result-'+value.ID).removeClass('show-component');
                $('#search-result-component-'+value.ID).hide();
            }
            else
            {
                $.check_result(value);
                $('#search-result-'+value.ID).addClass('show-component');
                $('#search-result-component-'+value.ID).show();
            }
        });

        // Set open result action
        $('#search-result-open-'+value.ID)
            .click(function(){
                $('#search').val(value.keyword).submit().blur();
                $('#keyword-btn').removeClass('active');
                return false;
            });

        // Seen result
        if( value.seen.length > 0 )
        {
            $('#search-result-component-keyword-'+value.ID+' .wrap').html('<ul></ul>');

            $.each(value.seen, function(key, seen){

                var comment = $('<li>')
                    .addClass('keyword-list')
                    .html(seen.member.firstname+' '+seen.member.lastname+'<br>'+
                        '<span class="search-component-seen-info">Last used: '+moment(seen.updated).fromNow()+'</span>')
                    .appendTo('#search-result-component-keyword-'+value.ID+' .wrap ul');
            });
        }
    }

};

//////////////////
// Function: keyword list
//////////////////
$.keyword_list = function(){
    $('.search-result-contain.keyword-result').remove();

    if ( keyword_store && keyword_store.length > 0 )
    {
        $('#keyword-result').slideDown('fast');
        $('#keyword-empty').slideUp('fast');


        $.each(keyword_store, function(key, value){
            $.insert_keyword_result(key, value);
        });
    }
    else
    {
        $('#keyword-empty').slideDown('fast');
    }

};

// --------------------------------------------------------------------
//////////////////
// Function: Get keyword result
//////////////////

$.keyword_seen = function(data){
    $.loader('search', 'show');

    // Init AJAX
    var keyword = $.ajax({
        type:"POST",
        url:"/search/keyword/seen",
        cache: false,
        data: data
    });

    // When done

    keyword.done(function(msg){
        var keyword_store = msg.keyword;
        $('.search-component-keyword-userlist').remove();
        $.loader('search', 'hide');



        console.log(keyword_store);
        if ( keyword_store )
        {
            if( keyword_store.seen.length > 0 )
            {

                $('#search-component-keyword-number').text(' '+keyword_store.seen.length+' person');
                $('#search-component-keyword-noti .not-used').hide();

                $.each(keyword_store.seen, function(key, value){
                    comment = $('<li>')
                        .addClass('search-component-keyword-userlist')
                        .html(value.member.firstname+' '+value.member.lastname+'<br>'+
                            '<span class="search-component-keyword-info">Last used: '+moment(value.updated).fromNow()+'</span>')
                        .insertBefore('#search-component-keyword-noti .not-used');
                });
            }
            else
            {

                $('#search-component-keyword-noti .not-used').hide();
            }
        }
        else
        {
            $('#search-component-keyword-number').text(' 0 ');
        }

    });

    // When fail
    keyword.fail(function(msg){
        $.loader('search', 'hide');
    });
};

// --------------------------------------------------------------------
//////////////////
// Function: keyword component
//////////////////
$.keyword_component = function(data){
    $('.search-component-keyword-userlist').remove();



};

// --------------------------------------------------------------------
//////////////////
// Function: Check keyword
//////////////////
$.check_keyword = function(){
    if( $('#search').val() == '' ){
        $('.keyword-component').hide();
        $('#search-result-tab').hide();
        $('#search-empty')
            .slideDown();

        if (!keyword)
        {
            $('#search-result').slideUp('fast');
        }

        $('#search-notfound:visible').slideUp('fast');
    }else{
        $('#search-empty')
            .slideUp();
    }
}

// --------------------------------------------------------------------
//////////////////
// Function: load keyword
//////////////////
$.load_keyword = function(data, callback){
    if (!data) var data = {};

    $.loader('keywordres', 'show');
    data['key'] = $.PKEY;

    // Init AJAX
    var keyword = $.ajax({
        type:"POST",
        url:"/search/keyword/get",
        cache: false,
        data: data
    });

    // When done
    keyword.done(function(msg){
        keyword_store = msg.keyword;

        if (callback)
        {
            callback();
        }

        $.loader('keywordres', 'hide');
    });

    // When fail
    keyword.fail(function(msg){
        $.loader('keywordres', 'hide');
    });
};