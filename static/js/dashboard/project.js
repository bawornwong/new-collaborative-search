
// ============================================================================================================================================
//////////////////
// Create new project
//////////////////

// Open window button event
$('#create-newproject-btn').click($.newproject_window);

// Close window button event
$('#create-newproject-close-btn, #create-newproject-done-btn').click(function(){
    $('#new-project-container').modal('hide');
});

// Submit form button event
$('#create-newproject-submit-btn').click(function(){
    $('#create-project-form').submit();
});

// Submit form event
$('#create-project-form').submit(function(){
    $('#create-newproject-loader').slideDown('fast',function(){
        $('#create-newproject-status').slideToHeight('expand');
    });
    $('#create-newproject-status-text').text('').hide();

    $('#create-newproject-submit-btn').fadeOut('fast');

    // Init ajax
    var new_project_ajax = $.ajax({
        type:"POST",
        url:"/project/new",
        cache: false,
        data: 'project_name='+$('#project-name').val()
    });

    // When done
    new_project_ajax.done(function(msg){
        var receive = msg;
        $('#create-newproject-loader').slideUp('fast',function(){
            if (receive.ack){
                $('#create-newproject-status-text').text('Create success.').show();
                $('#create-newproject-status').slideToHeight('expand');
                $('#create-newproject-done-btn').fadeIn('fast');
                $('#project-name').slideUp('fast');

                $.load_project_list('current');
            }else{

                $('#create-newproject-status-text').text(receive.message).show();
                $('#create-newproject-status').slideToHeight('expand');
                $('#create-newproject-submit-btn').fadeIn('fast');

                $('#project-name')
                    .removeAttr('disabled')
                    .removeAttr('readonly')
                    .val('');
            }
        });

    });

    // When fail
    new_project_ajax.fail(function(msg){
        $('#create-newproject-loader').slideUp('fast').hide();

        $('#create-newproject-status-text').text('Create fail.').show();
        $('#create-newproject-status').slideToHeight('expand');
        $('#create-newproject-submit-btn').fadeIn('fast');

        $('#project-name')
            .removeAttr('disabled')
            .removeAttr('readonly')
            .val('');
    });
    return false;
});

// ============================================================================================================================================
//////////////////
// Complete project
//////////////////

// Open window button event
$('#completed-project-btn').click($.completedproject_window);

//if($.PKEY && ( project_list.current_project.me.type == 'Admin' || project_list.current_project.me.type == 'Master' )){
//    $('#completed-project-btn').show();
//    $('#completed-project-btn').click($.completedproject_window);
//}else{
//    $('#completed-project-btn').hide();
//}

// Close window button event
$('#completed-project-close-btn').click(function(){
    $('#completed-project-container').modal('hide');
});

// Submit form event
$('#completed-project-yes-btn').click(function(){
    if ($.PKEY)
    {

        $.page_loader('show');

        // Init ajax
        var completed_project_ajax = $.ajax({
            type:"POST",
            url:"/project/status/completed",
            cache: false,
            data: 'key='+$.PKEY
        });

        // When done
        completed_project_ajax.done(function(msg){
            $.page_loader('hide');
            console.log(msg);
            if(msg.permit){
                $('#sign-out-project-btn').click();
                $.load_project_list('completed');
                $('#completed-project-container').modal('hide');
            }else{
                $('#completed-project-container').modal('hide');
            }
        });

        // When fail
        completed_project_ajax.fail(function(msg){
            $.page_loader('hide');
            $('#completed-project-container').modal('hide');
            console.log(msg);
        });
        return false;
    }

});