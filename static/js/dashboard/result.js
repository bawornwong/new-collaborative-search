// --------------------------------------------------------------------
//////////////////
// Function: Insert search result
//////////////////

$.comment_result = function(data, callback){
    $.loader('search', 'show');

    // Extend Data
    data['key'] = $.PKEY;

    if (data.from && data.from == 'intab')
    {
        data['Url'] = result_tab.current;
    }

    // Init AJAX
    var comment = $.ajax({
        type:"POST",
        url:"/search/comment",
        cache: false,
        data: data
    });

    // When done
    comment.done(function(msg){

        var this_result = _.find(search_result_store, function(search_result){ return search_result.url == data.Url; });
        var index = _.indexOf(search_result_store, this_result);

        // Send to activity socket receive

        socket.emit("activity_send" , {pkey: $.PKEY, message:'commented:123456789'+msg.search_result.url+'', module:'comment'});


        if ( index != -1 )
        {
            search_result_store[index] = msg.search_result;
        }
        else
        {
            search_result_store.push(msg.search_result);
        }

        $.check_result(data);

        if (callback)
        {
            callback();
        }
    });

    // When other
    comment.always(function(){
        $.loader('search', 'hide');
    });
};


// --------------------------------------------------------------------
//////////////////
// Function: Insert search result
//////////////////

$.relevant_result = function(data, resulttab){
    $.loader('search', 'show');

    // Extend Data
    data['key'] = $.PKEY;

    // Check relevant button state
    if ( (!resulttab && $('#search-result-relevant-'+data.ID).hasClass('push')) || (resulttab && $('#search-component-result-relevant').hasClass('push')) )
    {
        data['relevant'] = 'true';
    }
    else
    {
        data['relevant'] = 'false';
    }

    // Check irrelevant button state
    if ( (!resulttab && $('#search-result-irrelevant-'+data.ID).hasClass('push')) || (resulttab && $('#search-component-result-irrelevant').hasClass('push'))  )
    {
        data['irrelevant'] = 'true';
    }
    else
    {
        data['irrelevant'] = 'false';
    }

    // Init AJAX
    var relevant = $.ajax({
        type:"POST",
        url:"/search/relevant",
        cache: false,
        data: data
    });

    // When done
    relevant.done(function(msg){
        var this_result = _.find(search_result_store, function(search_result){ return search_result.url == data.Url; });
        var index = _.indexOf(search_result_store, this_result);

        // Send to activity socket receive
        if (msg.state == 'Relevant')
        {
            socket.emit("activity_send", {pkey:$.PKEY,message:'make relevant:123456789'+msg.search_result.url+'',module:'relevant'});
        }
        else if (msg.state == 'Irrelevant')
        {
            socket.emit("activity_send", {pkey:$.PKEY,message:'make irrelevant: "'+msg.search_result.url+'"',module:'relevant'});
        }
        else if (msg.state == 'Maybe')
        {
            socket.emit("activity_send", {pkey:$.PKEY,message:'is not sure that "'+msg.search_result.url+'" is relevant or not',module:'relevant'});
        }

        // Set to store
        if ( index != -1 )
        {
            search_result_store[index] = msg.search_result;
        }
        else
        {
            search_result_store.push(msg.search_result);
        }

        $.check_result(data);
        if ( resulttab )
        {
            $.result_component();
        }

    });

    // When other
    relevant.always(function(msg){
        $.loader('search', 'hide');
    });
};



// --------------------------------------------------------------------
//////////////////
// Function: result seen
//////////////////

$.result_seen = function(data){
    $.loader('search', 'show');

    // Extend Data
    data['key'] = $.PKEY;

    // Init AJAX
    var result = $.ajax({
        type:"POST",
        url:"/search/result/seen",
        cache: false,
        data: data
    });

    // When done
    result.done(function(msg){
        var this_result = _.find(search_result_store, function(search_result){ return search_result.url == data.Url; });
        var index = _.indexOf(search_result_store, this_result);

        if ( index != -1 )
        {
            search_result_store[index] = msg.search_result;
        }
        else
        {
            search_result_store.push(msg.search_result);
        }

        $.check_result(data);
        $.result_component();
    });

    // When other
    result.always(function(msg){
        $.loader('search', 'hide');
    });
};


// --------------------------------------------------------------------
//////////////////
// Function: result component
//////////////////
$.result_component = function(){

    $('.search-component-comment-message, .search-component-seen-userlist, .search-component-snapboard-snapshot').remove();
    var search_component_comment_noti = $('#search-component-comment-noti');
    var search_component_seen_noti = $('#search-component-seen-noti');

    var search_component_snapboard_noti = $('#search-component-snapboard-noti');
    search_component_comment_noti.find('.no-comment').show();
    search_component_snapboard_noti.find('.no-snapshot').show();

    // Count to mini component
    var search_component_comment_number = $('#search-component-comment-number');
    var search_component_relevant_number = $('#search-component-relevant-number');
    var search_component_seen_number = $('#search-component-seen-number');
    var search_component_snapboard_number = $('#search-component-snapboard-number');

    search_component_comment_number.text(' 0 ');
    search_component_relevant_number.text(' 0 ');
    search_component_seen_number.text(' 0 ');
    search_component_snapboard_number.text(' 0 ');

    // Check user relevant result
    var this_result = _.find(search_result_store, function(search_result){ return ( search_result.url == result_tab.current ); });


    // If found result
    if ( this_result )
    {
        console.log("!!!!!!", this_result)
        if(this_result.snapshots.length >0){

            search_component_snapboard_noti.find('.no-snapshot').hide();

            $('#search-component-snapboard-number').text(' '+this_result.snapshots.length+' ');
            var image_group_id = this_result.ID;

            $.each(this_result.snapshots, function(key, value){

                if (value.snapshot.image) {
                    console.log("xxxxxxx", value)

                    if(value.snapshot.img_type != "suggest"){
                        var img_url = '/static/snapshot/crop-'+value.snapshot.image;
                        $('<li>')
                            .addClass('search-component-snapboard-snapshot')
                            .html('<a class="snapshot_box_web" data-lightbox="'+image_group_id+'" href="'+img_url+'">'+'<img src="'+img_url+'">'+'</a>')
                            .insertBefore('#snapshotAvaiable');
                    }else{
                        var img_url = value.snapshot.image;
                        $('<li>')
                            .addClass('search-component-snapboard-snapshot')
                            .html('<a class="snapshot_box_web" data-lightbox="'+image_group_id+'" href="'+img_url+'">'+'<img src="'+img_url+'">'+'</a>')
                            .insertBefore('#snapshotAvaiable');
                    }
                }
            });

        }else{

            search_component_snapboard_noti.find('.no-snapshot').show();
            $('#search-component-snapboard-number').text(' 0 ');

        }

        if( this_result.seen.length > 0 )
        {
            search_component_seen_noti.find('.no-seen').hide();
            $('#search-component-seen-number').text(' '+this_result.seen.length+' ');

            $.each(this_result.seen, function(key, value){
                $('<li>')
                    .addClass('search-component-seen-userlist')
                    .html(value.member.firstname+' '+value.member.lastname+'<br>'+
                        '<span class="search-component-seen-info">Last seen: '+moment(value.updated).fromNow()+'</span>')
                    .insertBefore('#search-component-seen-noti .no-seen');
            });
        }
        else
        {
            search_component_seen_noti.find('.no-seen').show();
            $('#search-component-seen-number').text(' 0 ');
        }


        // Show relevent
        var count_relevant = _.countBy(this_result.relevant, function(relevant) {
            return relevant.state;
        });

        count_relevant.Relevant = ( count_relevant.Relevant ? count_relevant.Relevant : 0);
        count_relevant.Irrelevant = ( count_relevant.Irrelevant ? count_relevant.Irrelevant : 0);
        //count_relevant.Maybe = ( count_relevant.Maybe ? count_relevant.Maybe : 0);
        // var not_relevant = this_result.project.associate.length - (count_relevant.Relevant + count_relevant.Irrelevant + count_relevant.Maybe);
        var not_relevant = this_result.project.associate.length - (count_relevant.Relevant + count_relevant.Irrelevant);

        var percent = {
            relevant: (count_relevant.Relevant/this_result.project.associate.length*100).toFixed(2),
            irrelevant: (count_relevant.Irrelevant/this_result.project.associate.length*100).toFixed(2),
            //maybe: (count_relevant.Maybe/this_result.project.associate.length*100).toFixed(2),
            not_relevant: (not_relevant/this_result.project.associate.length*100).toFixed(2)
        };

        $('#search-component-result-relevant').removeClass('push active');
        $('#search-component-result-irrelevant').removeClass('push active');

        if ( this_result.relevant.length > 0 )
        {
            $('#search-component-relevant-number').text(' '+this_result.relevant.length+' ');
            $('#search-component-relevant-do').text(this_result.relevant.length);
            $('#search-component-relevant-total').text(this_result.project.associate.length);

            $('#search-component-relevant-bar .bar-success').css('width',percent.relevant+'%');
            //$('#search-component-relevant-bar .bar-warning').css('width',percent.maybe+'%');
            $('#search-component-relevant-bar .bar-info').css('width',percent.not_relevant+'%');
            $('#search-component-relevant-bar .bar-danger').css('width',percent.irrelevant+'%');

            $('#search-component-relevant-noti .label-success').text('Relevant: '+percent.relevant+'%');
            $('#search-component-relevant-noti .label-important').text('Irrelevant: '+percent.irrelevant+'%');
            //$('#search-component-relevant-noti .label-warning').text('Not sure: '+percent.maybe+'%');
            $('#search-component-relevant-noti .label-info').text('Not vote: '+percent.not_relevant+'%');
            var user_result;
            // Add relevent
            var this_user_result =_.find(this_result.relevant, function(relevant){

                if (this_result.relevant == "true"){
                    user_result = "Relevant";
                }
                else if (this_result.relevant == "false"){
                    user_result = "Irrelevant";
                }
                else{
                    return relevant.member._id == user_id;
                }
            });

            if ( this_user_result )
            {
                if ( this_user_result.state == 'Relevant' )
                {
                    $('#search-component-result-relevant').addClass('push active');
                }
                else if( this_user_result.state == 'Irrelevant' )
                {
                    $('#search-component-result-irrelevant').addClass('push active');
                }
            }

            if ( user_result == "Relevant" )
            {
                $('#search-component-result-relevant').addClass('push active');
            }
            else if( user_result == "Irrelevant" )
            {
                $('#search-component-result-irrelevant').addClass('push active');
            }
        }
        else
        {
            $('#search-component-relevant-do').text(this_result.relevant.length);
            $('#search-component-relevant-total').text(this_result.project.associate.length);

            $('#search-component-relevant-bar .bar-success').css('width','0%');
            $('#search-component-relevant-bar .bar-warning').css('width','0%');
            $('#search-component-relevant-bar .bar-info').css('width','0%');
            $('#search-component-relevant-bar .bar-danger').css('width','0%');

            $('#search-component-relevant-noti .label-success').text('Relevant: 0%');
            $('#search-component-relevant-noti .label-important').text('Irrelevant: 0%');
            $('#search-component-relevant-noti .label-warning').text('Not sure: 0%');
            $('#search-component-relevant-noti .label-info').text('Not vote: '+percent.not_relevant+'%');
        }

        // Show comment
        if ( this_result.comment.length > 0 )
        {
            $('#search-component-comment-number').text(' '+this_result.comment.length+' ');
            $('#search-component-comment-noti .no-comment').hide();
            $.each(this_result.comment, function(key, value){


                var lines = value.message.split("<br>");
                for (var i = 0; i < lines.length; i++) {
                    if (lines[i].length <= 45) continue;
                    var j = 0; space = 45;
                    while (j++ <= 45) {
                        if (lines[i].charAt(j) === " ") space = j;
                    }
                    lines[i + 1] = lines[i].substring(space + 1) + (lines[i + 1] || "");
                    lines[i] = lines[i].substring(0, space);
                }
                value.message= lines.slice(0).join("<br>");

                var comment = $('<li>')
                    .addClass('search-component-comment-message')
                    .html('<span class="comment-message" id="splitLines">'+value.message+'</span>'+
                        '<span class="comment-info">'+
                        '<span class="comment-info-username">'+value.member.firstname+' '+value.member.lastname+'</span>'+
                        '<span class="comment-info-date">'+moment(value.created).fromNow()+'</span>'+
                        '</span>')
                    .insertAfter('#search-component-comment-noti .comment-form');
            });

        }
        else
        {
            $('#search-component-comment-noti .no-comment').show();
            $('#search-component-comment-number').text(' 0 ');
        }
    }
};






// --------------------------------------------------------------------
//////////////////
// Function: Fragment result
//////////////////

$.fragment_result = function(data, search_contain) {

    // Count to mini component
    search_contain.find('#search-result-minicomponent-comment-' + data.ID).text(data.comment.length);
    search_contain.find('#search-result-minicomponent-relevant-' + data.ID).text(data.relevant.length);
    search_contain.find('#search-result-minicomponent-seen-' + data.ID).text(data.seen.length);
    search_contain.find('#search-result-minicomponent-snapshot-' + data.ID).text(data.snapshots.length);

    // Comment
    search_contain.find('#search-result-component-comment-form-' + data.ID).submit(function () {
        var search_result_component_comment_input = search_contain.find('#search-result-component-comment-input-' + data.ID);
        data['message'] = search_result_component_comment_input.val();
        search_result_component_comment_input.val('');

        $.comment_result(data);

        return false;
    });

    // Open and Close Description
    search_contain.find('#search-result-' + data.ID + '').click(function () {
        if (search_contain.find('#search-result-' + data.ID).hasClass('show-component')) {
            search_contain.find('#search-result-' + data.ID).removeClass('show-component');
            search_contain.find('#search-result-component-' + data.ID).hide();
        }
        else {
            $.check_result(data, search_contain);
            search_contain.find('#search-result-' + data.ID).addClass('show-component');
            search_contain.find('#search-result-component-' + data.ID).show();
        }
    });

    // Set open result action
    //console.log(data);
    search_contain.find('#search-result-open-' + data.ID)
        .click(function () {
            $.search_result_tab(data, 'open');
            $('#search').focus().blur();
            return false;
        });
};

// --------------------------------------------------------------------
//////////////////
// Function: Check result
//////////////////

$.check_result = function(data, search_contain){
    // Check user relevant result
    var this_result = _.find(search_result_store, function(search_result){ return ( search_result.url == data.Url ); });

    if (! search_contain)
    {
        search_contain = $('search-result-contain-'+data.ID);
    }

    // If found result
    if ( this_result )
    {
        // Count to mini component

        $('#search-result-minicomponent-comment-'+data.ID).text(this_result.comment.length);
        $('#search-result-minicomponent-relevant-'+data.ID).text(this_result.relevant.length);
        $('#search-result-minicomponent-seen-'+data.ID).text(this_result.seen.length);
        $('#search-result-minicomponent-snapshot-'+data.ID).text(this_result.snapshots.length);

        // Show relevent
        if ( this_result.relevant.length > 0 )
        {
            var count_relevant = _.countBy(this_result.relevant, function(relevant) {
                return relevant.state;
            });
            count_relevant.Relevant = ( count_relevant.Relevant ? count_relevant.Relevant : 0);
            count_relevant.Irrelevant = ( count_relevant.Irrelevant ? count_relevant.Irrelevant : 0);
            //count_relevant.Maybe = ( count_relevant.Maybe ? count_relevant.Maybe : 0);
            $('#search-result-minicomponent-count-relevant-'+data.ID).text(count_relevant.Relevant);
            $('#search-result-minicomponent-count-irrelevant-'+data.ID).text(count_relevant.Irrelevant);



            //var not_relevant = this_result.project.associate.length - (count_relevant.Relevant + count_relevant.Irrelevant + count_relevant.Maybe);
            var not_relevant = this_result.project.associate.length - (count_relevant.Relevant + count_relevant.Irrelevant);

            var percent = {
                relevant: (count_relevant.Relevant/this_result.project.associate.length*100).toFixed(2),
                irrelevant: (count_relevant.Irrelevant/this_result.project.associate.length*100).toFixed(2),

                //maybe: (count_relevant.Maybe/this_result.project.associate.length*100).toFixed(2),
                not_relevant: (not_relevant/this_result.project.associate.length*100).toFixed(2)
            };

            $('#search-result-component-relevant-'+data.ID)
                .html('<div class="wrap">'+
                    'Relevant <strong>'+this_result.relevant.length+'</strong> form <strong>'+this_result.project.associate.length+'</strong> project user(s).'+
                    '<div id="search-result-minicomponent-relevant-bar-'+data.ID+'" class="progress">'+
                    '<div class="bar bar-success" style="width: '+(percent.relevant)+'%;"></div>'+
                    '<div class="bar bar-warning" style="width: '+(percent.maybe)+'%;"></div>'+
                    '<div class="bar bar-danger" style="width: '+(percent.irrelevant)+'%;"></div>'+
                    '<div class="bar bar-info" style="width: '+(percent.not_relevant)+'%;"></div>'+
                    '</div>'+
                    '<span class="label label-success">Relevant: '+percent.relevant+'%</span>'+
                    '<span class="label label-important">Irrelevant: '+percent.irrelevant+'%</span>'+
                    //'<span class="label label-warning">Not sure: '+percent.maybe+'%</span>'+
                    '<span class="label label-info">Not vote: '+percent.not_relevant+'%</span>'+
                    '</div>');
        }

        // Show comment
        if ( this_result.comment.length > 0 )
        {
            $('#search-result-contain-'+data.ID).find('.no-comment, .comment-msg').remove();

            $.each(this_result.comment, function(key, value){

                var lines = value.message.split("<br>");
                for (var i = 0; i < lines.length; i++) {
                    if (lines[i].length <= 200) continue;
                    var j = 0; space = 200;
                    while (j++ <= 200) {
                        if (lines[i].charAt(j) === " ") space = j;
                    }
                    lines[i + 1] = lines[i].substring(space + 1) + (lines[i + 1] || "");
                    lines[i] = lines[i].substring(0, space);
                }
                value.message= lines.slice(0).join("<br>");



                $('<li>')
                    .addClass('comment-msg')
                    .html('<span class="comment-message">'+value.message+'</span>'+
                        '<span class="comment-info">'+
                        '<span class="comment-info-username">'+value.member.firstname+' '+value.member.lastname+'</span>'+
                        '<span class="comment-info-date">'+moment(value.created).fromNow()+'</span>'+
                        '</span>')
                    .insertAfter('#search-result-component-comment-'+data.ID+' .comment-form');
            });
        }

        // Seen result
        if( this_result.seen.length > 0 )
        {
            $('#search-result-'+data.ID).addClass('seen');
            $('#search-result-component-seen-'+data.ID+' .wrap').html('<ul></ul>');

            $.each(this_result.seen, function(key, value){
                $('<li>')
                    .addClass('search-result-component-seen-list')
                    .html(value.member.firstname+' '+value.member.lastname+'<br>'+
                        '<span class="search-component-seen-info">Last seen: '+moment(value.updated).fromNow()+'</span>')
                    .appendTo('#search-result-component-seen-'+data.ID+' .wrap ul');
            });

        }

        // Snapshot
        if ( this_result.snapshots.length > 0 )
        {
            var snapshot_container = $('#search-result-component-snapshot-'+data.ID+' div');

            $('#search-result-contain-'+data.ID).find('.no-snapshot').remove();
            snapshot_container.empty();

            var image_group_id = this_result.ID;
            $.each(this_result.snapshots, function(key, value){

                if (value.snapshot.image)
                {
                    if(value.snapshot.img_type != "suggest"){
                        var img_url = '/static/snapshot/crop-'+value.snapshot.image;
                        var img_box = $('<a class="snapshot_box" data-lightbox="'+image_group_id+'" href="'+img_url+'" data-title="">');
                        img_box.html('<img src="'+img_url+'">');
                        img_box.appendTo(snapshot_container);
                    }else{
                        var img_url = value.snapshot.image;
                        var img_box = $('<a class="snapshot_box" data-lightbox="'+image_group_id+'" href="'+img_url+'" data-title="">');
                        img_box.html('<img src="'+img_url+'">');
                        img_box.appendTo(snapshot_container);
                    }
                }
            });


        }

    }
};

// --------------------------------------------------------------------
//////////////////
// Function: relevant result list
//////////////////

$.relevant_result_list = function(){
    $('.search-result-contain.relevant-search-result').remove();

    if ( search_result_store && search_result_store.length > 0 )
    {
        $('#relevant-result').slideDown('fast');
        $('#relevant-empty').slideUp('fast');
        var filter_result = _.filter(search_result_store, function(store){ return store.relevant.length > 0; });

        $.each(filter_result, function(key, value){

            $.insert_relevant_search_result(key, value);
        });
    }
    else
    {
        $('#relevant-empty').slideDown('fast');
    }

};

// --------------------------------------------------------------------
//////////////////
// Function: snapboard result list
//////////////////

$.snapboard_result_list = function(){

    $('.search-result-contain.snapboard_result_list').remove();

    if ( search_result_store && search_result_store.length > 0 )
    {
        $('#snapboard-result').slideDown('fast');
        $('#snapboard-empty').slideUp('fast');
        /*
         var filter_result = _.filter(search_result_store, function(store){ return store.snapshot.length > 0; });

         $.each(filter_result, function(key, value){
         $.insert_snapboard_search_result(key, value);
         });
         */
    }
    else
    {
        $('#snapboard-empty').slideDown('fast');
    }


};