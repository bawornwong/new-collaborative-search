// ============================================================================================================================================
//////////////////
// Function: user management
/////////////////
$('#user-management-container')
    .modal({
        keyboard: false,
        backdrop: 'static',
        show: false
    })
    .on('shown', function () {
        $('#search-user').focus();
        jsp.project_userlist.reinitialise();
        jsp.invite_userlist.reinitialise();
    });



$.user_management = function(project_id){

    // Set project id
    userlist.project_id = project_id;

    // Set default
    userlist.project_info = null;

    // Show page loader
    $.page_loader('show');

    // Reset to default
    userlist.project = [];
    userlist.invite = [];
    $('.invite-userlist-item').remove();
    $('.project-userlist-item').remove();
    $('#search-user').val('');

    // Init ajax
    var load_project_info_ajax = $.ajax({
        type:"POST",
        url:"/project/getinfo",
        cache: false,
        data: "key="+project_id
    });

    // When done
    load_project_info_ajax.done(function(msg){
        userlist.project_info = msg;
        userlist.project = msg.project.associate;
        $('#user-management-container').modal('show');

        $.each(msg.project.associate, function(key, value){
            $.insert_project_user(value);
        });

        $.each(msg.invite, function(key, value){
            var data = {id: value.invite_to._id,username: value.invite_to.username, firstname: value.invite_to.firstname, lastname: value.invite_to.lastname, invite_id: value._id};
            $.insert_invite_user(data, true);
        });

        jsp.invite_userlist.reinitialise();

        $.page_loader('hide');
    });

    // When fail
    load_project_info_ajax.fail(function(msg){
        $.page_loader('hide');
    });
};

// ============================================================================================================================================
//////////////////
// User Management
//////////////////

// Close window button event
$('#user-management-close-btn, #user-management-done-btn').click(function(){
    $('#user-management-container').modal('hide');
});

// Set typeahead user search
$('#search-user').typeahead({
    source: function(q, process){
        console.log('Source');
        return $.post('/member/search', {q: q}, function(response) {
            console.log(response);
            var data = [];
            for(var i in response) {
                var check_invite = _.find(userlist.invite, function(invite){ return invite.id == response[i]['_id']; });
                var check_userproject = _.find(userlist.project, function(project){ return project.member._id == response[i]['_id']; });

                if ( !check_invite && !check_userproject )
                {
                    data.push(response[i]['_id'] +"#"+ response[i]['username'] +"#"+ response[i]['firstname'] +"#"+ response[i]['lastname']);
                }
            }
            return process(data);
        });
    },
    highlighter: function(item) {
        var parts = item.split('#'),
            html = '<div class="typeahead user-management-list">';
        html += '<div class="left">'+parts[1]+'</div>';
        html += '<div class="left">'+parts[2]+' '+parts[3]+'</div>';
        html += '<div class="clear"></div>';
        html += '</div>';
        return html;
    },
    updater: function(item) {
        var parts = item.split('#');
        var senddata = {id:parts[0], username:parts[1], firstname:parts[2], lastname:parts[3], key:userlist.project_id};
        $.insert_invite_user(senddata);
        return '';
    }
});

// --------------------------------------------------------------------
//////////////////
// Function: Insert invite user
//////////////////
$.insert_invite_user = function(data, first){

    var check_invite = _.find(userlist.invite, function(invite){ return invite.id == data.id; });
    var check_userproject = _.find(userlist.project, function(project){ return project.member._id == data.id; });

    if ( !check_invite  && !check_userproject )
    {
        // reinitialise
        jsp.invite_userlist.reinitialise();

        userlist.invite.push(data);
        //console.log(data);
        // insert message
        var insert_to = 'user-management-invite-userlist';
        var insert = $('<div>')
            .attr('id', 'user-invite-'+data.id)
            .attr('class','invite-userlist-item')
            .html( '<div class="invite-userlist-topic">'+
                '<span class="invite-userlist-header pull-left">'+data.username+'</span><br>'+
                '<span class="invite-userlist-info pull-left">'+data.firstname+' '+data.lastname+'</span></div>'+
                '<div class="invite-userlist-action"><div id="'+data.id+'-invite-loader" class="invite-loader"></div>'+
                '<div id="'+data.id+'-invite-remove" class="invite-remove hide"><div class="icon-remove"></div></div></div>');

        $('#'+insert_to+' .jspPane').append(insert);

        // reinitialise
        jsp.invite_userlist.api.reinitialise();
        jsp.invite_userlist.api.addHoverFunc();

        // If not first call
        if ( !first )
        {
            $('#'+data.id+'-invite-loader').tooltip({
                title:'Sending invite...',
                placement:'left'
            });

            // Save invite
            var invite_user = $.ajax({
                type:"POST",
                url:"/project/invite",
                cache: false,
                data: data
            });

            invite_user.done(function(msg){
                $('#'+data.id+'-invite-loader').tooltip('destroy');

                $('#'+data.id+'-invite-loader').hide();
                $('#'+data.id+'-invite-remove').show();

                $('#'+data.id+'-invite-remove')
                    .click(function(){
                        $.remove_invite_user(msg.project_invite._id, data);
                    })
                    .tooltip({
                        title:'Remove invite...',
                        placement:'left'
                    });


            });

            invite_user.fail(function(msg){

            });
        }
        else
        {
            $('#'+data.id+'-invite-loader').hide();
            $('#'+data.id+'-invite-remove')
                .show()
                .click(function(){
                    $.remove_invite_user(data.invite_id, data);
                })
                .tooltip({
                    title:'Remove invite...',
                    placement:'left'
                });
        }

    }

};

// --------------------------------------------------------------------
//////////////////
// Function: Remove invite user
//////////////////
$.remove_invite_user = function(invite_id, data){

    $('#'+data.id+'-invite-remove').hide();

    $('#'+data.id+'-invite-loader')
        .show()
        .tooltip({
            title:'Removing invite...',
            placement:'left'
        });

    // Save invite
    var invite_user = $.ajax({
        type:"POST",
        url:"/project/invite/remove",
        cache: false,
        data: {key:userlist.project_id, invite_id:invite_id}
    });

    invite_user.done(function(msg){

        var this_invite = _.find(userlist.invite, function(invite){ return invite.id == data.id; });
        var index = _.indexOf(userlist.invite, this_invite);

        //console.log(userlist.invite);
        userlist.invite.splice(index, 1);
        //console.log(userlist.invite);

        if ( msg.permit && !msg.error )
        {
            $('#user-invite-'+data.id).remove();
        }
        else
        {
            $('#'+data.id+'-invite-loader').hide().tooltip('destroy');
            $('#'+data.id+'-invite-remove').show();
        }

    });

    invite_user.fail(function(msg){
        $('#'+data.id+'-invite-loader').hide().tooltip('destroy');
        $('#'+data.id+'-invite-remove').show();
    });

};

// --------------------------------------------------------------------
//////////////////
// Function: Reject and Accept invite
//////////////////
$.project_invite_action = function(project_key, action){
    $.page_loader('show');

    if ( action == 'reject' )
    {
        // Reject invite
        var invite_user = $.ajax({
            type:"POST",
            url:"/project/invite/reject",
            cache: false,
            data: {key:project_key}
        });

        invite_user.done(function(msg){
            $.load_project_list('invite');
            $.page_loader('hide');
        });

        invite_user.fail(function(msg){
            $.page_loader('hide');
        });
    }
    else
    {
        // Accept invite
        var invite_user = $.ajax({
            type:"POST",
            url:"/project/invite/accept",
            cache: false,
            data: {key:project_key}
        });

        invite_user.done(function(msg){
            $.load_project_list(msg.project.status);
            $.page_loader('hide');
        });

        invite_user.fail(function(msg){
            $.page_loader('hide');
        });
    }

};

// --------------------------------------------------------------------
//////////////////
// Function: Remove and Admin project user
//////////////////
$.project_user_action = function(data){

    if ( data.action == 'admin' )
    {
        $('#'+data.member_id+'-project-remove').hide();
        $('#'+data.member_id+'-project-makeadmin-icon').hide();
        $('#'+data.member_id+'-project-makeadmin-loader').show();

        // admin action
        var project_user = $.ajax({
            type:"POST",
            url:"/project/user/admin",
            cache: false,
            data: data
        });

        project_user.done(function(msg){
            if ( data.admin_action == 'grant' )
            {
                $('#'+data.member_id+'-project-admin').show();

                $('#'+data.member_id+'-project-makeadmin')
                    .removeClass('project-makeadmin-no')
                    .addClass('project-makeadmin-yes')
                    .click(function(){
                        data.admin_action = 'revoke';
                        $.project_user_action(data);
                    });
            }
            else
            {
                $('#'+data.member_id+'-project-admin').hide();

                $('#'+data.member_id+'-project-makeadmin')
                    .removeClass('project-makeadmin-yes')
                    .addClass('project-makeadmin-no')
                    .click(function(){
                        data.admin_action = 'grant';
                        $.project_user_action(data);
                    });
            }

            $('#'+data.member_id+'-project-makeadmin-icon').show();
            $('#'+data.member_id+'-project-makeadmin-loader').hide();
            $('#'+data.member_id+'-project-remove').show();
        });

        project_user.fail(function(msg){

        });
    }
    else
    {
        $('#'+data.member_id+'-project-makeadmin').hide();
        $('#'+data.member_id+'-project-remove-icon').hide();
        $('#'+data.member_id+'-project-remove-loader').show();

        // admin action
        var project_user = $.ajax({
            type:"POST",
            url:"/project/user/remove",
            cache: false,
            data: data
        });

        project_user.done(function(msg){
            $('#user-project-'+data.member_id).remove();
        });

        project_user.fail(function(msg){

        });
    }

};


// --------------------------------------------------------------------
//////////////////
// Function: Insert project user
//////////////////
$.insert_project_user = function(data){

    // reinitialise
    jsp.project_userlist.reinitialise();

    // Check name
    var name = data.member.firstname+' '+data.member.lastname;
    if ( userlist.project_info.me.member._id == data.member._id)
    {
        name += ' (You)';
    }

    // insert message
    var insert_to = 'user-management-project-userlist';
    var insert = $('<div>')
        .attr('id', 'user-project-'+data.member._id)
        .attr('class','project-userlist-item')
        .html( '<div class="project-userlist-topic">'+
            '<span class="project-userlist-header pull-left">'+
            '<span id="'+data.member._id+'-project-master" class="badge badge-info pull-left hide">Owner</span>'+
            '<span id="'+data.member._id+'-project-admin" class="badge badge-info pull-left hide">Admin</span>'+
            data.member.username+'</span><br>'+
            '<span class="project-userlist-info pull-left">'+name+'</span></div>'+
            '<div class="project-userlist-action">'+
            '<div id="'+data.member._id+'-project-remove" class="project-remove hide"><div id="'+data.member._id+'-project-remove-icon" class="icon-remove"></div><div id="'+data.member._id+'-project-remove-loader" class="project-loader hide"></div></div>'+
            '<div id="'+data.member._id+'-project-makeadmin" class="project-makeadmin-no hide"><div id="'+data.member._id+'-project-makeadmin-icon" class="icon-user-md"></div><div id="'+data.member._id+'-project-makeadmin-loader" class="project-loader hide"></div></div>'+
            '</div>');

    // Define function
    var admin_action = function(action){
        $('#'+data.member._id+'-project-remove').hide();
        $('#'+data.member._id+'-project-makeadmin').unbind('click');

        $.project_user_action({
            action:'admin',
            admin_action:action,
            key: userlist.project_id,
            associate_id: data._id,
            member_id: data.member._id
        });
    };

    var remove_action = function(){
        $('#'+data.member._id+'-project-remove').unbind('click');
        $('#'+data.member._id+'-project-makeadmin').hide();

        $.project_user_action({
            action:'remove',
            key: userlist.project_id,
            associate_id: data._id,
            member_id: data.member._id
        });
    };

    // JSP Scrollpane
    $('#'+insert_to+' .jspPane').append(insert);

    // Check type
    if ( data.type == 'Master' )
    {
        $('#'+data.member._id+'-project-master').show();
    }
    else if ( data.type == 'Admin' )
    {

        if ( userlist.project_info.me.member._id != data.member._id)
        {
            $('#'+data.member._id+'-project-admin')
                .show();

            $('#'+data.member._id+'-project-remove')
                .show()
                .tooltip({
                    title:'Remove from project...',
                    placement:'left'
                })
                .click(function(){
                    remove_action();
                });

            $('#'+data.member._id+'-project-makeadmin')
                .removeClass('project-makeadmin-no')
                .addClass('project-makeadmin-yes')
                .show()
                .tooltip({
                    title:'Revoke admin...',
                    placement:'left'
                })
                .click(function(){
                    admin_action('revoke');
                });
        }

    }
    else
    {
        $('#'+data.member._id+'-project-remove')
            .show()
            .tooltip({
                title:'Remove from project...',
                placement:'left'
            })
            .click(function(){
                remove_action();
            });

        $('#'+data.member._id+'-project-makeadmin')
            .removeClass('project-makeadmin-yes')
            .addClass('project-makeadmin-no')
            .show()
            .tooltip({
                title:'Make admin...',
                placement:'left'
            })
            .click(function(){
                admin_action('grant');
            });
    }

    // reinitialise
    jsp.project_userlist.api.reinitialise();
    jsp.project_userlist.api.addHoverFunc();

};