
// --------------------------------------------------------------------
//////////////////
// Function: show/hide bookmark panel
//////////////////

$.relevant_panel = function(action){
    if( action == 'show' ){
        $('#relevant-result-panel')
            .slideDown('slow');
        $('#relevant-result-btn')
            .addClass('active');
    }else{
        $('#relevant-result-panel')
            .slideUp('slow', function(){
                $('.relevant-result-item').empty();
            });
        $('#relevant-result-btn')
            .removeClass('active');
    }
};

// --------------------------------------------------------------------
//////////////////
// Function: Insert relevant search result
//////////////////



var export_pdf_html = ""
var relevant_text = "<h1><u>Relevant</u></h1><br>"
var Irrelevant_test = "<h1><u>Irrelevant</u></h1><br>"


var search_result_pdf = $('#relevant-result-exporttopdf')
//
search_result_pdf.click(function(){
    //console.log("")
    var export_pdf = $.ajax({
        type: 'POST',
        url: '/export_pdf',
        cache: false
    }) ;

    export_pdf.done(function(msg){
        console.log(msg)
    });

});

$.insert_relevant_search_result = function(key, value){


    value.ID = value._id;
    value.DisplayUrl = value.display_url;
    value.Title = value.title;
    value.Description = value.description;
    value.Url = value.url;
    value.host = value.hostname;

    // Insert Result
    var search_contain = $('<div>')
        .attr('id', 'search-result-contain-'+value.ID)
        .addClass('search-result-contain relevant-search-result relevant-result-item');

    //var search_result_title = $('.search-result-link > a').attr('id', 'search-result-open-'+value.ID)


    var search_result = $('<div>')
        .attr('id', 'search-result-'+value.ID)
        .attr('class','search-result')
        .html(templates.search_result($.extend({}, value, {Thumbs_control: true})))
        //.html(templates.search_result(value))
        .tooltip({
            title:'Mark relevant',
            placement:'bottom',
            selector:'#search-result-relevant-'+value.ID
        })
        .tooltip({
            title:'Mark irrelevant',
            placement:'bottom',
            selector:'#search-result-irrelevant-'+value.ID
        });

    var search_component = $('<div>')
        .addClass('hide')
        .attr('id', 'search-result-component-'+value.ID)
        .addClass('search-result-component')
        .html(templates.search_component(value));

    var count_relevant = _.countBy(value.relevant, function(relevant) {
        return relevant.state;
    });

    count_relevant.Relevant = ( count_relevant.Relevant ? count_relevant.Relevant : 0);
    count_relevant.Irrelevant = ( count_relevant.Irrelevant ? count_relevant.Irrelevant : 0);
    count_relevant.Maybe = ( count_relevant.Maybe ? count_relevant.Maybe : 0);

    var not_relevant = value.project.associate.length - (count_relevant.Relevant + count_relevant.Irrelevant + count_relevant.Maybe);

    var percent = {
        relevant: (count_relevant.Relevant/value.project.associate.length*100).toFixed(2),
        irrelevant: (count_relevant.Irrelevant/value.project.associate.length*100).toFixed(2),
        maybe: (count_relevant.Maybe/value.project.associate.length*100).toFixed(2),
        not_relevant: (not_relevant/value.project.associate.length*100).toFixed(2)
    };

    var search_export_pdf = $('<div>')
        .attr('id', 'search_export_pdf-'+value.ID)
        .addClass('search_export_pdf')
        //.html(templates.search_);
        .html(templates.search_result_export_to_pdf($.extend({}, value, {Thumbs_control: true})));


    export_pdf_html +=  search_export_pdf.html()
    //console.log(export_pdf_html)


    if(percent.not_relevant < 100)
    {
        if (percent.relevant + percent.maybe > percent.irrelevant + percent.maybe)
        {
            console.log(value)

            search_contain.insertAfter('#relevant-result-relevant > h1');
            var point = Math.floor((100-percent.relevant)+1)
            if(percent.relevant >= 80){
                //search_result_title.css('color', 'rgba(255, 255, 255, 1)');
                search_result.css('background-color', 'rgba(20,'+(140 + point)+', 75, 0.75)')
            }else if(percent.relevant >= 60){
                //search_result_title.css('color', 'rgba(255, 255, 255, 1)');
                search_result.css('background-color', 'rgba(20,'+(140 + point)+', 65, 0.5)')
            }else if(percent.relevant >= 40){
                //search_result_title.css('color', 'rgba(255, 255, 255, 1)');
                search_result.css('background-color', 'rgba(20,'+(140 + point)+', 65, 0.25)')
            }
            else{
                //search_result_title.css('color', 'rgba(0, 0, 0, 1)');
                search_result.css('background-color', 'rgba(20,'+(140 + point)+', 45, 0.075)')
                }
            search_result.addClass('relevant');

        }
        else if (percent.relevant + percent.maybe < percent.irrelevant + percent.maybe)
        {
            var point = Math.floor((100-percent.irrelevant)+1)
            search_contain.insertAfter('#relevant-result-irrelevant > h1');
            if(percent.irrelevant >= 80){
                //search_result_title.css('color', 'rgba(255, 255, 255, 1)');
                search_result.css('background-color', 'rgba(255,'+(120 + point)+','+(120 + point)+', 1)')
            }else if(percent.irrelevant >= 60){
                //search_result_title.css('color', 'rgba(255, 255, 255, 1)');
                search_result.css('background-color', 'rgba(255,'+(120 + point)+','+(120 + point)+', 0.75)')
            }else if(percent.irrelevant >= 40){
                //search_result_title.css('color', 'rgba(255, 255, 255, 1)');
                search_result.css('background-color', 'rgba(255,'+(120 + point)+','+(120 + point)+', 0.5)')
            }
            else{
                //search_result_title.css('color', 'rgba(0, 0, 0, 1)');
                search_result.css('background-color', 'rgba(255,'+(120 + point)+','+(120 + point)+', 0.25)')
            }
            search_result.addClass('irrelevant');
        }
        else if (percent.relevant + percent.maybe == percent.irrelevant + percent.maybe)
        {
            search_contain.insertAfter('#relevant-result-notsure > h1');
            search_result.addClass('mayberelevant');
        }
    }


    // Search Contain
    search_contain.append(search_result);
    search_contain.append(search_component);
    //search_contain.append(search_export_pdf);

    $.fragment_result(value, search_contain);
    $.check_result(value, search_contain);

    // Check button state
    var check_button_state = function(data){
        // Check button state
        if ( $('#search-result-relevant-'+data.ID).hasClass('push') && $('#search-result-irrelevant-'+data.ID).hasClass('push') )
        {
            $('#search-result-'+data.ID)
                .removeClass('irrelevant relevant')
                .addClass('mayberelevant');

            return 'maybe';
        }
        else if (  $('#search-result-relevant-'+data.ID).hasClass('push') && !$('#search-result-irrelevant-'+data.ID).hasClass('push') )
        {
            $('#search-result-'+data.ID)
                .removeClass('irrelevant mayberelevant')
                .addClass('relevant');

            return 'relevant';
        }
        else if (  !$('#search-result-relevant-'+data.ID).hasClass('push') && $('#search-result-irrelevant-'+data.ID).hasClass('push') )
        {
            $('#search-result-'+data.ID)
                .removeClass('relevant mayberelevant')
                .addClass('irrelevant');

            return 'irrelevant';
        }
        else
        {
            $('#search-result-'+data.ID)
                .removeClass('relevant mayberelevant irrelevant');

            return false;
        }
    };

    var result_relevant_click = function(relevant_action){

        // Push button state
        if( $('#search-result-relevant-'+value.ID).hasClass('push') )
        {
            $('#search-result-relevant-'+value.ID).removeClass('push active');
        }
        else
        {
            $('#search-result-relevant-'+value.ID).addClass('push active');
            $('#search-result-irrelevant-'+value.ID).removeClass('push active');
        }

        check_button_state(value);

        if ( !relevant_action )
        {
            $.relevant_result(value);
        }

        $('#search-result-'+value.ID).addClass('show-component');
        $('#search-result-component-'+value.ID).show();
    };

    // Pre event irrelevant click
    var result_irrelevant_click = function(relevant_action){

        // Push button state
        if( $('#search-result-irrelevant-'+value.ID).hasClass('push') )
        {
            $('#search-result-irrelevant-'+value.ID).removeClass('push active');
        }
        else
        {
            $('#search-result-irrelevant-'+value.ID).addClass('push active');
            $('#search-result-relevant-'+value.ID).removeClass('push active');
        }

        check_button_state(value);

        if ( !relevant_action )
        {
            $.relevant_result(value);
        }

        $('#search-result-'+value.ID).addClass('show-component');
        $('#search-result-component-'+value.ID).show();
    };

    // Relevant
    $('#search-result-relevant-'+value.ID).click(function(){ result_relevant_click(); });

    // Irrelevant
    $('#search-result-irrelevant-'+value.ID).click(function(){ result_irrelevant_click(); });


    // Check user relevant result
    var this_result = _.find(search_result_store, function(search_result){ return search_result.url == value.Url; });
    if ( this_result )
    {
        $.check_result(value, search_contain);

        // Add relevent
        var this_user_result =_.find(this_result.relevant, function(relevant){
            return relevant.member._id == user_id;
        });

        if ( this_user_result )
        {
            if ( this_user_result.state == 'Relevant' )
            {
                result_relevant_click(true);
            }
            else if( this_user_result.state == 'Irrelevant' )
            {
                result_irrelevant_click(true);
            }
            else if( this_user_result.state == 'Maybe' )
            {
                result_relevant_click(true);
                result_irrelevant_click(true);
            }
        }
        $('#search-result-'+value.ID).removeClass('show-component');
        $('#search-result-component-'+value.ID).hide();
    }

    return export_pdf_html
};
