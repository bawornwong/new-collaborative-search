// --------------------------------------------------------------------
//////////////////
// Function: show/hide search panel
//////////////////
$.search_panel = function(action){
    if( action == 'show' ){
        $('#search-panel')
            .slideDown('slow');

        // Animate Main Section
        $('#main')
            .animate({
                top: 100
            }, 'fast');

        // Animate Search Bar
        $('#search-bar')
            .show()
            .animate({
                top: 50,
                opacity: 1
            }, 'fast');

    }else if( action == 'hide'){
        $('#search-panel')
            .slideUp('slow');

        // Animate Search Bar
        $('#main')
            .animate({
                top: 50
            }, 'fast');

        // Animate Main Section
        $('#search-bar')
            .animate({
                top: 0,
                opacity: 0
            }, 'fast', function() {
                $('#search-bar').show();
            });
    }else{
        $('#search-bar')
            .animate({
                top: 0,
                opacity: 0
            }, 'fast', function() {
                $('#search-bar').hide();
            });

        $('#main')
            .animate({
                top: 50
            }, 'fast');
        $('#search-panel')
            .slideUp('slow');
    }
};

// --------------------------------------------------------------------
//////////////////
// Function: Insert search result
//////////////////
$.insert_search_result = function(key, value){
    // insert message
    var insert_to = 'search-result';
    var search_contain = $('<div>')
        .attr('id', 'search-result-contain-'+value.ID)
        .addClass('search-result-contain');

    //ปุ่มกดไลค์
    var search_result = $('<div>')
        .attr('id', 'search-result-'+value.ID)
        .attr('class','search-result')
        .html(templates.search_result($.extend({}, value, {Thumbs_control: true})))
        //like
        .tooltip({
            title:'Mark relevant',
            placement:'bottom',
            selector:'#search-result-relevant-'+value.ID
        })
        //dislike
        .tooltip({
            title:'Mark irrelevant',
            placement:'bottom',
            selector:'#search-result-irrelevant-'+value.ID
        });

    var search_component = $('<div>')
        .addClass('hide')
        .attr('id', 'search-result-component-'+value.ID)
        .addClass('search-result-component')
        .html(templates.search_component(value));

    if( $('#'+insert_to+' > :Last-child').length == 0 )
    {
        $('#'+insert_to).append(search_contain);
    }
    else
    {
        search_contain.insertAfter('#'+insert_to+' > :Last-child');
    }

    // Search Contain
    search_contain.append(search_result);
    search_contain.append(search_component);

    // Check button state
    var check_button_state = function(data){
        // Check button state
        if ( $('#search-result-relevant-'+data.ID).hasClass('push') && $('#search-result-irrelevant-'+data.ID).hasClass('push') )
        {
            $('#search-result-'+data.ID)
                .removeClass('irrelevant relevant')
                .addClass('mayberelevant');

            return 'maybe';
        }
        else if (  $('#search-result-relevant-'+data.ID).hasClass('push') && !$('#search-result-irrelevant-'+data.ID).hasClass('push') )
        {
            $('#search-result-'+data.ID)
                .removeClass('irrelevant mayberelevant')
                .addClass('relevant');

            return 'relevant';
        }
        else if (  !$('#search-result-relevant-'+data.ID).hasClass('push') && $('#search-result-irrelevant-'+data.ID).hasClass('push') )
        {
            $('#search-result-'+data.ID)
                .removeClass('relevant mayberelevant')
                .addClass('irrelevant');

            return 'irrelevant';
        }
        else
        {
            $('#search-result-'+data.ID)
                .removeClass('relevant mayberelevant irrelevant');

            return false;
        }
    };

    // Pre event relevant click
    var result_relevant_click = function(relevant_action){

        // Push button state
        if( $('#search-result-relevant-'+value.ID).hasClass('push') )
        {
            $('#search-result-relevant-'+value.ID).removeClass('push active');
        }
        else
        {
            $('#search-result-relevant-'+value.ID).addClass('push active');
            $('#search-result-irrelevant-'+value.ID).removeClass('push active');
        }

        check_button_state(value);

        if ( !relevant_action )
        {
            $.relevant_result(value);
        }

        $('#search-result-'+value.ID).addClass('show-component');
        $('#search-result-component-'+value.ID).show();
    };

    // Pre event irrelevant click
    var result_irrelevant_click = function(relevant_action){

        // Push button state
        if( $('#search-result-irrelevant-'+value.ID).hasClass('push') )
        {
            $('#search-result-irrelevant-'+value.ID).removeClass('push active');
        }
        else
        {
            $('#search-result-irrelevant-'+value.ID).addClass('push active');
            $('#search-result-relevant-'+value.ID).removeClass('push active');
        }

        check_button_state(value);

        if ( !relevant_action )
        {
            $.relevant_result(value);
        }

        $('#search-result-'+value.ID).addClass('show-component');
        $('#search-result-component-'+value.ID).show();
    };

    // Relevant
    $('#search-result-relevant-'+value.ID).click(function(){ result_relevant_click(); });

    // Irrelevant
    $('#search-result-irrelevant-'+value.ID).click(function(){ result_irrelevant_click(); });

    // Comment
    $('#search-result-component-comment-form-'+value.ID).submit(function(){

        value['message'] = $('#search-result-component-comment-input-'+value.ID).val();
        $('#search-result-component-comment-input-'+value.ID).val('');

        $.comment_result(value);

        return false;
    });



    // Open and Close Description

    $('#search-result-'+value.ID+'').click(function(){
        if( $('#search-result-'+value.ID).hasClass('show-component') )
        {
            $('#search-result-'+value.ID).removeClass('show-component');
            $('#search-result-component-'+value.ID).hide();
        }
        else
        {
            $.check_result(value);
            $('#search-result-'+value.ID).addClass('show-component');
            $('#search-result-component-'+value.ID).show();
        }
    });

    // Set open result action

    $('#search-result-open-'+value.ID).live('mousedown', function(e) {
        if( (e.which == 1) ) {
            $.search_result_tab(value, 'open');
        }if( (e.which == 3) ) {
            alert("disable right click");
        }else if( (e.which == 2) ) {
            alert("disable middle click");
        }
        e.preventDefault();
    }).live('contextmenu', function(e){
        e.preventDefault();
    });


    /*
        .click(function(){

                $.search_result_tab(value, 'open');

        });
    */
    // Check user relevant result
    var this_result = _.find(search_result_store, function(search_result){ return search_result.url == value.Url; });
    if ( this_result )
    {
        $.check_result(value, search_contain);

        // Add relevent
        var this_user_result =_.find(this_result.relevant, function(relevant){
            return relevant.member._id == user_id;
        });

        if ( this_user_result )
        {
            if ( this_user_result.state == 'Relevant' )
            {
                result_relevant_click(true);
            }
            else if( this_user_result.state == 'Irrelevant' )
            {
                result_irrelevant_click(true);
            }
            else if( this_user_result.state == 'Maybe' )
            {
                result_relevant_click(true);
                result_irrelevant_click(true);
            }
        }

        $('#search-result-'+value.ID).removeClass('show-component');
        $('#search-result-component-'+value.ID).hide();
    }

};
//
//var query_suggestion_engine = new Bloodhound({
//    datumTokenizer: function(d) {
//        return Bloodhound.tokenizers.whitespace(d.val);
//    },
//    queryTokenizer: Bloodhound.tokenizers.whitespace,
//    remote: {
//        url: '/search/query_suggestion',
//        filter: function (data) {
//            // Map the remote source JSON array to a JavaScript array
//            return $.map(data.results, function (d) {
//                return {
//                    value: d
//                };
//            });
//        }
//    }
//});
//query_suggestion_engine.initialize();

$('#search').autocomplete({
    source: "/search/query_suggestion",
    minLength: 2
});

//
//$.insert_search_suggest = function(suggests){
//    // insert message
//    var insert_to = 'search-result';
//    $('#search-result-suggestion').remove();
//    var search_contain = $('<div>')
//        .attr('id', 'search-result-suggestion')
//        .addClass('search-result-suggestion');
//
//    if( $('#'+insert_to+' > :Last-child').length == 0 )
//    {
//        $('#'+insert_to).append(search_contain);
//    }
//    else
//    {
//        search_contain.insertBefore('#'+insert_to+' > :First-child');
//    }
//
//    // Search Contain
//    suggests.forEach(function(suggest){
//        var suggest_button = $('<a>').attr('id', suggest.id).attr('class', 'btn').attr('href', '#').text(suggest.title).click(function(){
//            $('#search').val(suggest.title);
//            $('#search-submit').click();
//        });
//        search_contain.append(suggest_button);
//    });
//
//};


// --------------------------------------------------------------------
//////////////////
// Function: load search result
//////////////////

$.load_search_result = function(data, callback){

    if (!data) {
        data = {};
    }

    $.loader('searchres', 'show');
    data['key'] = $.PKEY;

    // Init AJAX
    var search_result = $.ajax({
        type:"POST",
        url:"/search/result/get",
        cache: false,
        data: data
    });

    // When done
    search_result.done(function(msg){
        console.log("load_search_result: ", msg)
        search_result_store = msg.search_result;

        if (callback)
        {
            callback();
        }

        $.loader('searchres', 'hide');
    });

    // When fail
    search_result.fail(function(msg){
        $.loader('searchres', 'hide');
    });
};

// --------------------------------------------------------------------
//////////////////
// Function: Search result tab
//////////////////

$.search_result_tab = function(data, action){
    $.keyword_component();

    $('#search-result-tab').show();

    $('#search-result').show();
    $('.search-result-tabopen-contain').hide();
    $('#search-result-tab-title').text('All result');

    $('.search-result-tablist').removeClass('active');
    $('#search-result-tabmenu-allresult').parent().addClass('active');


    if ( data == 'CloseAll')
    {
        $.keyword_component();
        $('.keyword-component').show();
        $('.result-component').hide();

        result_tab.current = 'all';
        result_tab.tab = [];

        $('.search-result-tablist, .search-result-tabopen-contain').remove();
        $('#search-result-tab-title').text('All result');
        $('#no-open-tab').show();
    }
    else if ( data.ID == 0 )
    {

        result_tab.current = 'all';

        $.keyword_component();

        $('#search-result-tab').show();
        $('.result-component').hide();
        $('#search-result').show();
        $('.search-result-tabopen-contain').hide();
        $('#search-result-tab-title').text('All result');


        $("li[class^='search-result-tablist']").removeClass('active');
        $('#search-result-tabmenu-allresult').blur().parent().addClass('active');
        $('.keyword-component').show();
    }
    else
    {
        // If show result
        if ( action == 'open' )
        {

            if(!('search_by_keyword' in data)){

                data.search_by_keyword = $.CURRENT_KEYWORD;
            }

            data.ID = (data.ID) ? data.ID : data._id;

            // Set current tab
            console.log("data=");
            console.log(data);

            result_tab.current = (data.Url) ? data.Url : data.url;

            $("#snapshot-image").attr("data-url", result_tab.current);

            result_tab.current_data = data;

            // Generate noti
            $.result_component();
            $('.keyword-component').hide();
            $('.result-component').show();

            // Hide all result tab
            $('#search-result').hide();
            $('.search-result-tabopen-contain').hide();
            // Set insert element
            var insert_to = 'search-result-tabmenu';
            var insert = $('<li>')
                .addClass('search-result-tablist-'+data.ID)
                .html(templates.search_result_tab(data));

            // Set title text
            $('#search-result-tab-title').text(data.Title);

            if ( $('#search-result-tabmenu-'+data.ID).length < 1 )
            {
                if(!$("#search-result-tabmenu-allresult").parent().hasClass("active") && !$("#search-result-tabmenu-allresult").is(':visible')){
                    $("#search-result-tabmenu-allresult").hide();
                    $('#search-result-tab').show();
                    $('#search-result-tabmenu-allresult').parent().addClass('active');
                }

                // ถ้าเปิดจาก seen ครั้งแรก ไม่ search ก่อน
                // Send to activity socket receive


                socket.emit("activity_send", {pkey:$.PKEY, message: 'opened:123456789'+result_tab.current+'', module:'seen'});
                //socket.emit("activity_send", {pkey:$.PKEY, message: data ,module:'seen'});
                insert.insertBefore('#'+insert_to+' > #no-open-tab');

                // Set close tab action
                $('#search-result-tabmenu-'+data.ID+'-remove')
                    .click(function(){
                        return $.search_result_tab(data, 'close');
                    });

                // Set open action
                $('#search-result-tabmenu-'+data.ID)
                    .click(function() {
                        $.search_result_tab(data, 'open');
                    });
                // Push to tab open array

                result_tab.tab.push(data);
                $.result_seen(data);
            }

            // Hide no open menu
            $('#no-open-tab').hide();

            // New tab when not opened
            if ( $('#search-result-tab-'+data.ID).length < 1 || !$('#search-result-tab-'+data.ID).length )
            {
                // Set insert element

                if(data.Url==undefined){
                    var tab_insert = $('<div>')
                        .attr('id', 'search-result-tab-' + data.ID)
                        .addClass('hide')
                        .addClass('search-result-tabopen-contain')
                        .html('<iframe id="search-result-tab-' + data.ID + '-iframe" name="' + data.ID + '" src="' + data.url + '" class="search-result-tabopen" ></iframe>');
                }else{
                    var tab_insert = $('<div>')
                        .attr('id', 'search-result-tab-' + data.ID)
                        .addClass('hide')
                        .addClass('search-result-tabopen-contain')
                        .html('<iframe id="search-result-tab-' + data.ID + '-iframe" name="' + data.ID + '" src="' + data.Url + '" class="search-result-tabopen" ></iframe>');
                }

                tab_insert.insertAfter('#search-result')
            }

            $('#search-result-tab-'+data.ID).show();
            $('#search-result-tabmenu-allresult').blur().parent().removeClass('active');
            $("li[class^='search-result-tablist']").removeClass('active');
            $('.search-result-tablist-'+data.ID).addClass('active');
        }
        // If hide result
        else
        {
            // Remove from array
            var this_tab = _.find(result_tab.tab, function(tab){ return tab.ID == data.ID; });
            var index = _.indexOf(result_tab.tab, this_tab);

            result_tab.tab.splice(index, 1);

            // Show no open tab when no result open
            if ( result_tab.tab.length == 0 )
            {
                $('#no-open-tab').show();
                if (keyword)
                {
                    $('#search-result').show();
                    $('.keyword-component').show();
                }

                $('.result-component').hide();
                $('#search-result-tab-title').text('All result');
            }

            $('.search-result-tablist-'+data.ID).remove();
            $('#search-result-tab-'+data.ID).remove();
            $('#search-result-tabmenu-'+data.ID).remove();
            $('#search-result-tabmenu-allresult').parent().addClass('active');
            $('#search-result').show();
            $('.keyword-component').show();
            $('.result-component').hide();


            // if close current tab
            if ( result_tab.current == data.ID )
            {
                if (keyword)
                {
                    $('#search-result').show();
                }

                $('#search-result-tab-title').text('All result');
                return true;
            }
            else if ( result_tab.tab.length == 0 )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    var check_invite = _.find(userlist.invite, function(invite){ return invite.id == data.id; });

};

// --------------------------------------------------------------------
//////////////////
// Search form
//////////////////
$('#search_form').submit(function(){

    // Select Element
    var search_input = $('#search');

    // Animate Search Bar
    $.search_panel('show');

    search_input.blur();

    if( search_input.val() != '' && search_input.val() != keyword ){

        // Reset to default
        keyword_store = null;
        search_result_store = null;
        $.search_result_tab({ID:0});
        $('#search-result').hide();
        $('.search-result-contain').remove();

        var keyword = search_input.val();
        $.CURRENT_KEYWORD = keyword;

        $.loader('search', 'show');

        // Init AJAX
        var search_query = $.ajax({
            type:"POST",
            url:"/search/get",
            cache: false,
            data: 'query='+search_input.val()+'&pkey='+$.PKEY
        });

        // When done
        search_query.done(function(msg){
            var receive = msg;
            num_result = 0;

            // Send to activity socket receive
            socket.emit("activity_send", {pkey:$.PKEY,message:'query:123456789'+keyword+'',module:'query'});

            // Show keyword component
            $.keyword_seen({key:$.PKEY, keyword:keyword});
            $.keyword_component();
            $("#search-result-tabmenu-allresult").show();
            $('.keyword-component').show();
            $('#search-result-tab').show();
            $('#search-result-tabmenu-allresult').parent().addClass('active');

            $('.result-component').hide();

            // Store Search Result
            if (receive.search_result)
            {
                search_result_store = receive.search_result;
            }

            // Store User Id
            if (receive.user_id)
            {
                user_id = receive.user_id;
            }

//            $.insert_search_suggest(receive.suggests);

            // Loop display search result
            $.each(receive.d.results, function(key, value){
                $.insert_search_result(key, value);
                num_result += 1;
            });

            // Check for show noti
            if ( num_result == 0 )
            {
                $('#search-notfound')
                    .html('Your search <strong>"'+ keyword +'"</strong><br>did not match any results.')
                    .slideDown('fast');

                found_result = false;
            }
            else
            {
                $('#search-notfound').slideUp('fast');
                $('#search-result').slideDown('fast');

                found_result = true;
            }

            $.check_keyword();
            $.loader('search', 'hide');
        });

        // When fail
        search_query.fail(function(msg){
            //alert('error');
            $('#search-result').slideDown('fast');
            $.loader('search', 'hide');

            found_result = false;
        });
    }else{
        if( $('#search').val() != keyword )
        {
            $('#search-result').slideUp('slow');
        }

        $.check_keyword();
    }

    return false;
});

$('#search-submit').click(function(){
    $('#search_form').submit();
});

$('#search')
    .focus(function(){
        $.search_panel('show');
        $.relevant_panel('hide');
        $.keyword_panel('hide');
        $.seen_panel('hide');
        $.activity_panel('hide');
        $.snapboard_panel('hide');

        $('#search-action-group').addClass('search-action-group-focus');
    })
    .focusout(function(){
        $('#search-action-group').removeClass('search-action-group-focus');
    });

$('#search-component-comment-form')
    .submit(function(){
        var value = result_tab.current_data;
        var search_component_comment_input = $('#search-component-comment-input');
        value['from'] = 'intab';
        value['message'] = search_component_comment_input.val().replace(/\r?\n/g,"<br>");
        search_component_comment_input.val('');

        $.comment_result(value, function(){ $.result_component() });

        return false;
    })
    .find('textarea')
        .click(function(e) { // Fix input element click problem
            e.stopPropagation();
        });