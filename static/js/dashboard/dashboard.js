// ============================================================================================================================================
// Dashboard
// ============================================================================================================================================

// ============================================================================================================================================
//////////////////
// Function: Project list loader
//////////////////
$.projectlist_loader = function(action){
    if( action == 'show' ){

        // Show load
        if (loadqueue.projectlist.length == 0){
            $('#project-list-loader').show();
        }

        // Push queue
        loadqueue.projectlist.push($.random_string(10));

    }else{
        // Pop queue
        loadqueue.projectlist.pop();

        // hide loader
        if (loadqueue.projectlist.length == 0){
            $('#project-list-loader').hide();
        }
    }
};

// ============================================================================================================================================
//////////////////
// Function: load dashboard
//////////////////
$.load_dashboard = function(){
    // Set global pkey
    $.PKEY = null;
    $.CURRENT_KEYWORD = null;

    // Set location hash
    $.address.value('dashboard');

    // Show menu
    $.show_menu('dashboard');

    // Init project list
    if ( project_list.current_tab == null )
    {
        $.load_project_list('current');
    }

};

// ============================================================================================================================================
//////////////////
// Function: load project
//////////////////
$.load_project = function(project_key){
    // Show page loader
    $.page_loader('show');

    // Set global pkey
    $.PKEY = project_key;

    // Set location hash
    $.address.value('project/'+project_key);

    // Get project info if not loaded
    if ( !project_list.current_project || project_list.current_project['project']['_id']!=project_key )
    {
        // Reset to default
        keyword_store = null;
        found_result = false;
        keyword = '';
        $('#sidebar-panel-useractivity, #search-result, #activity-seemore').hide();
        $('#search').val('');
        $('#chat-empty, #search-empty, #activity-empty').show();
        $.search_result_tab('CloseAll');
        $('.search-result-contain, .chat-message-dategroup, .activity-item').remove();
        $('#search-result-tabmenu-allresult')
            .unbind('click')
            .bind('click',function(){
                $.search_result_tab({ID:0});
            });

        // Init ajax
        var load_project_info_ajax = $.ajax({
            type:"POST",
            url:"/project/getinfo",
            cache: false,
            data: "key="+project_key
        });

        // When done
        load_project_info_ajax.done(function(msg){
            // Set global user id
            user_id = msg.user_id;

            // get info
            project_list.current_project = msg;
            $('#project-title').text(project_list.current_project.project.name);

            // Count time
            $.count_time('reset');

            // Show project search menu
            $.show_menu('project', project_key, function(){
                // Run
                $('#search_form').submit();
                $.activity_panel('show');
                $.update_chat();
                $.update_activity();

                // Hide loader
                $.page_loader('hide');
            });

            // Connect to Socket if not found connection
            if (socket == null)
            {
                socket = io.connect('/socketserver');
                //console.log("Create Connection!");

                // Callback from server
                socket.on('connect', function () {
                    //console.log("Connected!");
                });


                // Receive log from server
                socket.on('log',function(data){
                });

                // Chat receive
                socket.on('chat_receive',function(data){
                    //console.log(data);
                    $.insert_chat(data);
                });

                // Activity receive
                socket.on('activity_receive',function(data){
                    //console.log(data);
                    $.insert_activity(data);
                });

                socket.on('disconnect',function(data){
                    // Send to activity socket receive
                    socket.emit("activity_send", {pkey:$.PKEY,message:' left the project',module:'project'});
                });
            }

            // Send to server : connect to project room
            socket.emit("connect_project", {pkey:$.PKEY});

            // Send to activity socket receive
            socket.emit("activity_send", {pkey:$.PKEY,message:'entered the project',module:'project'});

            // User manager button
            if ( project_list.current_project.me.type == 'Admin' || project_list.current_project.me.type == 'Master' )
            {
                $('#user-manager-btn')
                    .unbind('click')
                    .bind('click',function(){
                        $.user_management($.PKEY);
                    });

                $('.user-manager-group')
                    .show();

                $('#completed-project-btn')
                    .show()
                    .unbind('click')
                    .bind('click', $.completedproject_window);
            }
            else
            {
                $('#user-manager-btn')
                    .unbind('click');

                $('.user-manager-group')
                    .hide();

                $('#completed-project-btn')
                    .hide()
                    .unbind('click');
            }

        });

        // When fail
        load_project_info_ajax.fail(function(msg){
            $.page_loader('hide');
        });

    }
    // If project info loader
    else
    {
        // Set loader hide
        $.page_loader('hide');

        // Show project search menu
        $.show_menu('project', project_key);

        // Send to server : connect to project room
        if (socket != null)
        {
            socket.emit("connect_project", {pkey:$.PKEY});

            // Count time
            $.count_time('start');
        }

        // User manager button
        if ( project_list.current_project.me.type == 'Admin' || project_list.current_project.me.type == 'Master' )
        {
            $('#user-manager-btn')
                .unbind('click')
                .bind('click',function(){
                    $.user_management($.PKEY);
                });

            $('.user-manager-group')
                .show();

            $('#completed-project-btn')
                .show()
                .unbind('click')
                .bind('click', $.completedproject_window);
        }
        else
        {
            $('#user-manager-btn')
                .unbind('click');

            $('.user-manager-group')
                .hide();

            $('#completed-project-btn')
                .hide()
                .unbind('click');
        }

    }

    // Sign out project button
    $('#sign-out-project-btn')
        .unbind('click')
        .bind('click',function(){
            // Send to activity socket receive
            socket.emit("activity_send", {pkey:$.PKEY,message:'left the project',module:'project'});

            // Send to server : connect to project room
            socket.emit("disconnect_project", {pkey:$.PKEY});

            // Count time
            $.count_time('pause');

            // Load dashboard
            $.load_dashboard();

            //return false;
        });

    // Sign out project button
    $('#logo1')
        .unbind('click')
        .bind('click',function(){
            // Send to activity socket receive
            socket.emit("activity_send", {pkey:$.PKEY,message:'left the project',module:'project'});

            // Send to server : connect to project room
            socket.emit("disconnect_project", {pkey:$.PKEY});

            // Count time
            $.count_time('pause');

            // Load dashboard
            $.load_dashboard();

            //return false;
        });
};

// ============================================================================================================================================
//////////////////
// Function: Count time
//////////////////
$.count_time = function(mode){

    var count_time_func = function(reset){

        if (!reset)
        {
            count_time_store.time_in += 1000;
        }

        var cal = moment(count_time_store.start) + moment(count_time_store.time_in);
        var duration = moment.duration(cal);
        var time_hour = ( (duration.hours()<10) ? '0'+duration.hours():duration.hours() );
        var time_min = ( (duration.minutes()<10) ? '0'+duration.minutes():duration.minutes() );
        var time_sec = ( (duration.seconds()<10) ? '0'+duration.seconds():duration.seconds() );
        var time_text = ' ( '+time_hour+':'+time_min+':'+time_sec+' \)';

        $('#sidebar-showtime').text(duration.humanize() + time_text);
    }

    if (mode == 'reset')
    {

        count_time_store.time_in = 0;
        count_time_store.start = (project_list.current_project) ? project_list.current_project.me.time_spent : 0;
        count_time_func(true);
        count_time_store.current = setInterval(count_time_func, 1000);

    }
    else if(mode == 'start')
    {
        count_time_store.current = setInterval(count_time_func, 1000);
    }
    else if(mode == 'pause')
    {
        clearInterval(count_time_store.current);
    }
};

// ============================================================================================================================================
//////////////////
// Function: load project list
//////////////////
$.load_project_list = function(status, see_more){

    // Init status
    var status_data = '';
    var see_more = ( see_more != null ) ? see_more : false;
    $('#project-list-item-empty').hide();

    // reinitialise
    jsp.project_list.reinitialise();

    // Check status
    if ( $.inArray( status, project_status) > -1 ){

        // Check see more
        if ( !see_more ){
            project_list[status]['page'] = 0;
        }

        // Check status
        status_data = 'status='+status+'&page='+project_list[status]['page'];
        project_list.current_tab = status;

        // Un-active button
        $('#project-btn-group .active').removeClass('active');

        // Active status button
        $('#'+status+'-btn').addClass('active');

        $('#project-list-title').text( project_list[status]['title'] );
    }

    // Show loader
    $.projectlist_loader('show');

    // Init ajax
    var project_list_ajax = $.ajax({
        type:"POST",
        url:"/project/get",
        cache: false,
        data: status_data
    });

    // When done
    project_list_ajax.done(function(msg){

        var receive = msg;

        // If is status
        if ( $.inArray( status, project_status ) > -1 ){

            if ( !see_more ){
                $('.project-list-item').remove();
                project_list[status]['loaded'] = 0;
                project_list[status]['result'] = {};
                $.review_project('default');
            }

            project_list[status]['page'] = project_list[status]['page'] + 1;
            project_list[status]['total'] = receive[status].total;
            project_list[status]['loaded'] = project_list[status]['loaded'] + receive[status].this_total;
            project_list[status]['this_total'] = receive[status].this_total;
            project_list[status]['remain'] = project_list[status]['total'] - project_list[status]['loaded'];

            if ( receive[status].total != 0 )
            {
                $.each(receive[status].results, function(key, value){
                    var project_info = ( status == 'invite') ? value.project : value;

                    project_list[status]['result'][project_info._id] = value;
                    project_list[status]['result'][project_info._id]['me'] = receive[status].me[key];

                    var insert = $('<div>')
                        .attr('id', 'project-list-'+value._id)
                        .attr('class','project-list-item')
                        .html( '<div class="project-list-item-title">'+project_info.name+
                            '</div><span class="icon icon-angle-right"></span>')
                        .click(function(){
                            $.review_project(status, project_info._id);

                            // Un-active button
                            $('#project-list-content .active').removeClass('active');

                            // Active status button
                            $(this).addClass('active');
                        })
                        .insertBefore('#project-list-seemore');

                    $('#project-list-item-empty').hide();

                });

                if ( project_list[status]['remain'] != 0 ){

                    $('#project-list-seemore')
                        .unbind('click')
                        .bind('click',function(){
                            $.load_project_list(status, true);
                            $(this).hide();
                        })
                        .show();
                }else{
                    $('#project-list-seemore')
                        .unbind('click')
                        .hide();
                }

            }else{
                $('#project-list-item-empty-title').text( project_list[status]['not_found'] );
                $('#project-list-item-empty').show();
            }

            if ( receive.invite.total > 0 && receive.invite.total < 100 )
            {
                $('#invite-noti')
                    .text(receive.invite.total)
                    .show();
            }
            else if (receive.invite.total >= 100)
            {
                $('#invite-noti')
                    .text('99+')
                    .show();
            }
            else
            {
                $('#invite-noti')
                    .hide();
            }

            //console.log(project_list);
        }

        // Hide loader
        $.projectlist_loader('hide');

        // reinitialise
        jsp.project_list.api.reinitialise();
        jsp.project_list.api.addHoverFunc();
    });

    // When fail
    project_list_ajax.fail(function(msg){
        // Hide loader
        $.projectlist_loader('hide');
    });
}

// ============================================================================================================================================
//////////////////
// Function: project menu
/////////////////
$.show_menu = function(action, project_key, callback){
    // int action value
    var action = ( action != null ) ? action : 'dashboard';

    // if action is show
    if ( action == 'dashboard' )
    {
        // Show Container
        $('#user-menu-container').fadeIn('fast');

        // Hide project info
        $('#user-menu-title, #project-separator').fadeOut('fast');

        // When project container is hide , user menu is show
        $('#project-container').fadeOut('fast',function(){
            $('#user-btn').animate({width: 230}, "fast", function(){
                $('#project-title').fadeOut('fast');
                $('#project-btn').animate({width: 0}, "fast", function(){
                    $('#user-title').fadeIn('fast');
                });
            });
        });

        // Hide Search show Project
        $('#main-search-container').hide();
        $('#header-search-container').fadeOut('fast',function(){
            $('#header-dashboard-container').fadeIn('fast');
            $('#main-dashboard-container').show();

            // reinitialise
            jsp.project_list.reinitialise();

            // Callback
            if (typeof callback === 'function')
            {
                callback();
            }
        });
    }
    else if ( action == 'project' )
    {
        $('#user-menu-container:not(visible)').fadeIn();
        $('#user-menu-title').fadeIn('fast');
        $('#user-title').fadeOut('fast',function(){
            $('#user-btn').animate({width: 5}, "fast", function(){
                $('#project-btn').animate({width: 171}, "fast", function(){
                    $('#project-separator, #project-container, #project-title').fadeIn('fast');
                });
            });
        });

        // Hide Project show Search
        $('#main-dashboard-container').hide();
        $('#header-dashboard-container').fadeOut('fast',function(){
            $('#header-search-container').fadeIn('fast',function(){
                // Callback
                if (typeof callback === 'function')
                {
                    callback();
                }
            });

            $('#main-search-container').show();
        });

    }
}


// ============================================================================================================================================
//////////////////
// Function: review project
//////////////////
$.review_project = function(status, project_key){

    // If not set to default
    if (status != 'default')
    {
        // Get value
        var get_project = project_list[status]['result'][project_key];

        // Set title
        $('#project-review-title').text(get_project.name);

        // If this is Invite
        if (status == 'invite')
        {
            // Accept button
            $('#project-confirmation-accept')
                .tooltip('destroy')
                .tooltip({
                    title:'Accept',
                    placement:'bottom'
                })
                .unbind('click')
                .bind('click',function(){
                    $.project_invite_action(project_key, 'accept');
                });

            // Reject button
            $('#project-confirmation-reject')
                .tooltip('destroy')
                .tooltip({
                    title:'Reject',
                    placement:'bottom'
                })
                .unbind('click')
                .bind('click',function(){
                    $.project_invite_action(project_key, 'reject');
                });

            // Hide group of action button
            $('.project-action').hide();

            // Show confirm group action button
            $('#project-confirmation-action').show();

        }
        // If is a completed project
        else if(status == 'completed')
        {
            // Hide group of action button
            $('.project-action').hide();

            // Show confirm group action button
            $('#project-confirmation-action').hide();

        }
        // If not Invite
        else
        {
            // Work on button
            $('#project-review-work-on')
                .tooltip('destroy')
                .tooltip({
                    title:'Sign in project',
                    placement:'bottom'
                })
                .unbind('click')
                .bind('click',function(){
                    $.load_project(project_key);
                });

            // user management button
            if ( get_project.me.type == 'Master' || get_project.me.type == 'Admin' )
            {
                $('#project-review-user-management')
                    .tooltip('destroy')
                    .tooltip({
                        title:'User management',
                        placement:'bottom'
                    })
                    .unbind('click')
                    .bind('click',function(){
                        $.user_management(project_key);
                    })
                    .show();
            }
            else
            {
                $('#project-review-user-management')
                    .hide()
                    .unbind('click');
            }


            $('.project-action').hide();
            $('#project-review-action').show();
        }

        // Review
        $('#pjr-user').text(project_list[status]['result'][project_key].associate.length);
        $('#pjr-time').text(moment(project_list[status]['result'][project_key].created).fromNow());
        $('#project-review-select').hide();
        $('#project-review-content').show();
    }
    // If set to default
    else
    {
        // Set title
        $('#project-review-title').text('Review');

        // Hide group of action button
        $('.project-action').hide();

        $('#project-review-select').show();
    }

}

// ============================================================================================================================================
//////////////////
// Function: newproject window
/////////////////
$('#new-project-container')
    .modal({
        keyboard: false,
        backdrop: 'static',
        show: false
    })
    .on('shown', function () {
        $('#project-name').focus();
    })
    .on('hidden', function () {
        $('#project-name')
            .removeAttr('disabled')
            .removeAttr('readonly')
            .val('');
        $('#create-newproject-submit-btn').show();
        $('#create-newproject-done-btn').hide();
        $('#create-newproject-status').slideToHeight('zero');
        $('#create-newproject-loader').slideDown('fast').show();
        $('#project-name').slideDown('fast');
    });

$.newproject_window = function(){
    $('#new-project-container').modal('show');
    return false;
};

// ============================================================================================================================================
//////////////////
// Function: completedproject window
/////////////////
$('#completed-project-container')
    .modal({
        keyboard: false,
        backdrop: 'static',
        show: false
    });

$.completedproject_window = function(){
    if($.PKEY && ( project_list.current_project.me.type == 'Admin' || project_list.current_project.me.type == 'Master' ))
    {
        $('#completed-project-container').modal('show');
    }

    return false;
};

// Close window button event
$('.completed-project-cancel-btn').click(function(){
    $('#completed-project-container').modal('hide');
});
